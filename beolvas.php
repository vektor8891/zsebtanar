﻿<?php
/**
 * Read data from datafile
 *
 * PHP Version 5
 *
 * @category PHP
 * @package  Zsebtanár
 * @author   Viktor Szabó <zsebtanar@gmail.com>
 * @license  http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
 * @link     http://zsebtanar.hu/
 */

session_start();

require 'functions_main.php';

connectDatabase('../database_old.txt'); 

if (!isset($_SESSION['conn'])) {
    $conn = $_SESSION['conn'];
}

if (!isset($_REQUEST['p'])) {

    // Total update 
    dropDatabase();
    createDatabase();
    createTables();

} else {

    // Partial update
    $id = $_REQUEST['p'];
    deleteFromTables($id);

}

// Read in file
$json = file_get_contents('DATA.json');
$data = json_decode($json, TRUE);


// Read classes
foreach ($data["classes"] as $class) {


    insertClass($class);

    // Read topics
    if (isset($class["topics"])) {
        foreach ($class["topics"] as $topic) {

            insertTopic($topic);

            // Read subtopics
            if (isset($topic["subtopics"])) {
                foreach ($topic["subtopics"] as $subtopic) {

                    insertSubtopic($subtopic);
                        
                    if (checkSubtopic()) {

                        // Read questions
                        if (isset($subtopic["questions"])) {
                            insertQuestions($subtopic["questions"]);
                        }

                        // Read quiz
                        if (isset($subtopic["quiz"])) {
                            insertQuiz($subtopic["quiz"]);
                        }

                        // Read exercises
                        if (isset($subtopic["exercise"])) {
                            insertExercises($subtopic["exercise"]);
                        }
                    }
                }
            }
        }
    }
}

redirectMain();





/*****************************  MISC - set up etc.  ****************************/


/**
 * Drops existing database
 *
 * @return NULL
 */
function dropDatabase()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_torol = 'DROP DATABASE IF EXISTS '.$dbname;
    $conn->query($sql_torol) or die ('Failed to drop database. '.$conn->error);
    return;
}

/**
 * Create new database
 *
 * @return NULL
 */
function createDatabase()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_letrehoz = 'CREATE DATABASE '.$dbname;
    $conn->query($sql_letrehoz) or die ('Failed to create database. '.$conn->error);
    return;
}

/**
 * Redirect to main page
 *
 * @return NULL
 */
function redirectMain()
{
    $p = '';
    $f = '';
    if (isset($_REQUEST["p"])) {
        $p = '&p='.$_REQUEST["p"];
        if (isset($_REQUEST["f"])) {
            $f = '&f='.$_REQUEST["f"];
        }
    }

    header('Location: index.php?write=true&logged_in=true'.$p.$f);
    return;
}

/**
 * Checks whether to insert subtopic
 *
 * @return bool TRUE if subtopic is to be inserted.
 */
function checkSubtopic()
{
    $subtopicID = $_SESSION['subtopicID'];

    if (!isset($_REQUEST['p'])) {
        return TRUE;
    } else {
        $current = $_REQUEST['p'];
        if ($current == $subtopicID) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}




















/******************************   CREATE - create tables    ************************/

/**
 * Create tables
 *
 * @return NULL
 */
function createTables()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_db = 'USE '.$dbname;
    $conn->query($sql_db) or die ('Failed to use database. '.$conn->error);
    createClasses();
    createTopics();
    createSubtopics();
    createQuestions();
    createQuiz();
    createExercises();
    createLinks();
    return;
}

/**
 * Create table for classes
 *
 * @return NULL
 */
function createClasses()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE classes (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                alt VARCHAR(60) NOT NULL,
                name VARCHAR(60) NOT NULL,
                PRIMARY KEY (id)
            );';
    $conn->query($sql) or die ('Failed to create table Classes. '.$conn->error);
    return;
}

/**
 * Create table for exercises
 *
 * @return NULL
 */
function createExercises()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE exercises (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                subtopicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                topicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                classID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                ex_no VARCHAR(10) NOT NULL,
                label VARCHAR(120) NOT NULL,
                guide TEXT NOT NULL,
                guide1 TEXT NOT NULL,
                guide2 TEXT NOT NULL,
                guide3 TEXT NOT NULL,
                youtube VARCHAR(60) NOT NULL,
                ex_question TEXT NOT NULL,
                ex_answer TEXT NOT NULL,
                source VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Exercises. '.$conn->error);
    return;
}

/**
 * Create table for links
 *
 * @return NULL
 */
function createLinks()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE links (
                id SMALLINT NOT NULL REFERENCES exercises(id),
                link_class VARCHAR(60) NOT NULL,
                link_topic VARCHAR(60) NOT NULL,
                link_subtopic VARCHAR(60) NOT NULL,
                link_label VARCHAR(60) NOT NULL,
                link_type VARCHAR(60) NOT NULL,
                PRIMARY KEY (id)
            );';
    $conn->query($sql) or die ('Failed to create table Classes. '.$conn->error);
    return;
}

/**
 * Create table for questions
 *
 * @return NULL
 */
function createQuestions()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE questions (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                classID SMALLINT UNSIGNED NOT NULL REFERENCES classes(id),
                topicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                subtopicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                label VARCHAR(60) NOT NULL,
                question TEXT NOT NULL,
                answer TEXT NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Questions. '.$conn->error);
    return;
}

/**
 * Create table for quiz
 *
 * @return NULL
 */
function createQuiz()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE quiz (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                classID SMALLINT UNSIGNED NOT NULL REFERENCES classes(id),
                topicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                subtopicID SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                question TEXT NOT NULL,
                correct VARCHAR(120) NOT NULL,
                wrong1 VARCHAR(120) NOT NULL,
                wrong2 VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Quiz. '.$conn->error);
    return;
}

/**
 * Create table for subtopics
 *
 * @return NULL
 */
function createSubtopics()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE subtopics (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                classID SMALLINT UNSIGNED NOT NULL REFERENCES classes(id),
                topicID VARCHAR(60) NOT NULL REFERENCES topics(id),
                alt VARCHAR(60) NOT NULL,
                name VARCHAR(60) NOT NULL,
                type VARCHAR(60) NOT NULL,
                source VARCHAR(120) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Subtopics. '.$conn->error);
    return;
}

/**
 * Create table for topics
 *
 * @return NULL
 */
function createTopics()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE topics (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                classID SMALLINT UNSIGNED NOT NULL REFERENCES classes(alt),
                alt VARCHAR(60) NOT NULL,
                name VARCHAR(60) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Topics. '.$conn->error);
    return;
}



















/**************************   DELETE - delete from tables   ***********************/


/**
 * Delete from tables
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function deleteFromTables($id)
{
    deleteFromQuestions($id);
    deleteFromQuiz($id);
    deleteFromExercises($id);
    deleteFromLinks($id);
    return;
}

/**
 * Delete from Questions
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function deleteFromQuestions($id)
{
    $conn = $_SESSION['conn'];
    $sql = 'DELETE FROM questions WHERE subtopicID = '.$id;
    $conn->query($sql) or die ('Failed to delete data from Questions. '.$conn->error);
    return;
}

/**
 * Delete from Links
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function deleteFromLinks($id)
{
    $conn = $_SESSION['conn'];

    $exercises = getExercises('subtopicID = "'.$id.'"');

    if ($exercises) {
        foreach ($exercises as $exercise) {

            print_r($exercise["id"]);

            $sql = 'DELETE FROM links WHERE id = '.$exercise["id"];
            $conn->query($sql) or die ('Failed to delete data from Links. '.$conn->error);

        }
    }

    return;
}

/**
 * Delete from Quiz
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function deleteFromQuiz($id)
{
    $conn = $_SESSION['conn'];
    $sql = 'DELETE FROM quiz WHERE subtopicID = '.$id;
    $conn->query($sql) or die ('Failed to delete data from Quiz. '.$conn->error);
    return;
}

/**
 * Delete from Exercises
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function deleteFromExercises($id)
{
    $conn = $_SESSION['conn'];
    $sql = 'DELETE FROM exercises WHERE subtopicID = '.$id;
    $conn->query($sql) or die ('Failed to delete data from Exercises. '.$conn->error);
    return;
}



















/*******************   GET  - get data (e.g. from database)   **********************/

/**
 * Create path to folder
 *
 * Possible values for 'folder': 'questions' or 'feladatok'.
 *
 * @param array $folder Name of folder.
 *
 * @return string $dir Path.
 */
function getCurrentDir($folder)
{
    $classID = $_SESSION['classID'];
    $topicID = $_SESSION['topicID'];
    $subtopicID = $_SESSION['subtopicID'];

    $class = getClass('id = "'.$classID.'"');
    $topic = getTopic('id = "'.$topicID.'"');
    $subtopic = getSubtopics('id = "'.$subtopicID.'"');
    
    $dir = 'tananyag/'.$class["alt"]
                    .'/'.$topic["alt"]
                    .'/'.$subtopic[0]["alt"]
                    .'/'.$folder;
    return $dir;
}

/**
 * Get exercises
 *
 * @param string $cond Condition.
 *
 * @return array $exercises Exercises data.
 */
function getExercises($cond=NULL)
{
    $conn = $_SESSION['conn'];

    if ($cond) {
        $sql = "SELECT * FROM exercises WHERE ".$cond;
    } else {
        $sql = "SELECT * FROM exercises";
    }

    $res = $conn->query($sql) or die ('Failed to select exercises.');

    $i = 0;
    if ($res->num_rows > 0) {
        while($row = $res->fetch_assoc()) {
            $exercises[$i] = $row;
            $i++;
        }
    } else {
        return NULL;
    }

    return $exercises;
}


/**
 * Calculates width of image
 *
 * @param array $imgtext Image data.
 *
 * @return int $width Width of image.
 */
function getImgWidth($imgtext)
{
    if (isset($imgtext[0]["width"])) {
        $width = $imgtext[0]["width"];
    } else {
        $width = 100;
    }

    return $width;
}

/**
 * Get subtopics
 *
 * @param string $cond Condition.
 *
 * @return array $subtopics Subtopic data.
 */
function getSubtopics($cond=NULL)
{
    $conn = $_SESSION['conn'];
    if ($cond) {
        $sql = 'SELECT * FROM subtopics WHERE '.$cond;
    } else {
        $sql = 'SELECT * FROM subtopics';
    }

    $res = $conn->query($sql) or die ('Failed to select subtopics.');

    $i = 0;
    if ($res->num_rows > 0) {
        while($row = $res->fetch_assoc()) {
            $subtopics[$i] = $row;
            $i++;
        }
    } else {
        return NULL;
    }

    return $subtopics;
}

/**
 * Get topic data from database
 *
 * @param string $cond Condition.
 *
  * @return array $topic Topic data.
 */
function getTopic($cond=NULL)
{
    $conn = $_SESSION['conn'];
    if ($cond) {
        $sql = 'SELECT * FROM topics WHERE '.$cond;
    } else {
        $sql = 'SELECT * FROM topics';
    }

    $res = $conn->query($sql) or die ('Failed to select topics.');
    $topic = $res->fetch_assoc();

    return $topic;
}

/**
 * Get class data from database
 *
 * @param string $cond Condition.
 *
 * @return array $class Class data.
 */
function getClass($cond=NULL)
{
    $conn = $_SESSION['conn'];
    if ($cond) {
        $sql = 'SELECT * FROM classes WHERE '.$cond;
    } else {
        $sql = 'SELECT * FROM classes';
    }

    $res = $conn->query($sql) or die ('Failed to select classes.');
    $class = $res->fetch_assoc();

    return $class;
}

























/*************************   INCLUDE - include data to string   *******************/

/**
 * Include text to answer
 *
 * @param string $html Answer.
 * @param string $text Text.
 *
 * @return string $html Answer including text.
 */
function includeText($html, $text)
{
    $html .= '<p>'.$text.'</p>';
    return $html;
}

/**
 * Include image to answer
 *
 * @param string $html   Answer.
 * @param string $dir   Path to image.
 * @param array  $imgtext  Image data.
 * @param array  $question Questions data.
 * @param int   $id Picture number.
 *
 * @return string $html Answer including image text.
 */
function includeImgText($html, $dir, $imgtext, $question, $id)
{
    $pic = $dir.'/'.$imgtext[0]["file"];
    $width = getImgWidth($imgtext);
    if (isset($imgtext[0]["change"]) && $imgtext[0]["change"]) {
        $change = ' onclick="kepCsere(\\\'kep-'
            .toAscii($question["label"]).$id.'\\\')" ';
        $id = ' id="kep-'.toAscii($question["label"]).$id.'" ';
    } else {
        $change = ' ';
        $id = ' ';
    }
    if (isset($imgtext[0]["alt"])) {
        $alt = ' alt="'.$imgtext[0]["alt"].'" ';
    } else {
        $alt = ' ';
    }
    $html .= '<img'.$id.'class="img-question center-block" '
        .'src="'.$pic.'" width="'.$width.'%" '.$change.$alt.'>';
    if (isset($imgtext[0]["source"]) || isset($imgtext[0]["title"])) {
        $html = insertImgCaption($html, $imgtext, $width);

    }
    return $html;
}

/**
 * Include alert to answer
 *
 * @param string $html   Answer.
 * @param string $dir   Path to image.
 * @param array  $alert Alert data.
 * @param array  $question Questions data.
 * @param int   $pic     Picture number.
 *
 * @return string $html Answer including alert.
 */
function includeAlert($html, $dir, $alert, $question, $pic)
{
    $alerthtml = '';
    foreach ($alert[0] as $key => $value) {
        if (strpos($key, 'text') !== FALSE) {
            $alerthtml = includeText($alerthtml, $value);
        } else if (strpos($key, 'img') !== FALSE) {
            $alerthtml = includeImgText($alerthtml, $dir, $value, $question, $pic);
            $pic++;

        }
    }

    $html .= '<div class="alert alert-'.$alert[0]["class"].'">'
            .'<b>'.$alert[0]["title"].'</b> '
            .$alerthtml.'</div>';
    return array($html, $pic);
}

/**
 * Include carousel to answer
 *
 * @param string $html  Answer.
 * @param array  $imgtext Image data.
 * @param string $dir   Path to image.
 *
 * @return string $html Answer including alert.
 */
function includeCarousel($html, $imgtext, $dir)
{
    $html .= '<div id="myCarousel" class="carousel slide" data-ride="carousel">'
            .'<ol class="carousel-indicators">';
    for ($i=0; $i < $imgtext[0]["darab"]; $i++) {
        $html = includeCarouselIndicator($html, $i);
    }
    $html .= '</ol><div class="carousel-inner" role="listbox">';

    $pic = $dir.'/'.$imgtext[0]["nev"].'/'.$imgtext[0]["nev"];
    
    $width = getImgWidth($imgtext);

    for ($i=1; $i <= $imgtext[0]["darab"]; $i++) {
        $html = includeCarouselInner($html, $pic, $width, $i);
    }
    $html .= '<a class="left carousel-control" href="#myCarousel" '
            .'role="button" data-slide="prev">'
            .'<span class="glyphicon glyphicon-chevron-left" '
            .'aria-hidden="true"></span><span class="sr-only">Previous</span>'
            .'</a><a class="right carousel-control" href="#myCarousel" '
            .'role="button" data-slide="next">'
            .'<span class="glyphicon glyphicon-chevron-right" '
            .'aria-hidden="true"></span><span class="sr-only">Next</span></a>'
            .'</div></div>';

    return $html;
}

/**
 * Include carousel indicator to answer
 *
 * @param string $html Answer.
 * @param int   $i Indicator number.
 *
 * @return string $html Answer including carousel indicator.
 */
function includeCarouselIndicator($html, $i)
{
    if ($i == 1) {
        $class = ' class="active" ';
    } else {
        $class = ' ';
    }
    $html .= '<li data-target="#myCarousel" '
            .'data-slide-to="'.$i.'"'.$class.'></li>';

    return $html;
}

/**
 * Include carousel inner link to answer
 *
 * @param string $html  Answer.
 * @param string $pic   Image path.
 * @param int   $width Width of image.
 * @param int   $i  Indicator number.
 *
 * @return string $html Answer including carousel inner link.
 */
function includeCarouselInner($html, $pic, $width, $i)
{
    if ($i == 1) {
        $class = 'item active';
    } else {
        $class = 'item';
    }
    $html .= '<div class="'.$class.'">'
                .'<img src="'.$pic.$i.'.jpg" width="'.$width.'%">'
            .'</div>';

    return $html;
}

/**
 * Include html
 *
 * @param array  $guidedata Guide data.
 * @param string $dir    Current directory.
 * @param array  $folder    Exercise data folder.
 *
 * @return string $html Guide text.
 */
function includeHtml($guidedata, $dir, $folder)
{
    $html = '';
    foreach ($guidedata as $key => $value) {
        if (strpos($key, "text") !== FALSE) {
            $html = includeHtmlText($html, $value);
        } elseif (strpos($key, "img") !== FALSE) {
            $html = includeHtmlImg($html, $value, $dir, $folder);
        } elseif (strpos($key, "title") !== FALSE) {
            $html = includeHtmlTitle($html, $value);
        } elseif (strpos($key, "youtube") !== FALSE) {
            $html = includeHtmlYoutube($html, $value);
        } else {
            trigger_error('Ismeretlen állománytípus: '.$value, E_USER_ERROR);
        }
    }

    return $html;
}

/**
 * Include title to html
 *
 * @param string $html  Guide.
 * @param array  $title Text.
 *
 * @return string $html Text containing title.
 */
function includeHtmlTitle($html, $title)
{
    $html .= '<h2>'.$value.'</h2>';

    return $html;
}

/**
 * Include text to html
 *
 * @param string $html Guide.
 * @param array  $text Text.
 *
 * @return string $html Text containing text.
 */
function includeHtmlText($html, $text)
{
    if (is_array($text)) {
        if (isset($text[0]["class"])) {
            $html .= '<p class="'.$text[0]["class"].'">';
        } else {
            $html .= '<p>';
        }
        if (isset($text[0]["text"])) {
            $html .= $text[0]["text"];
        }
    } else {
        $html .= '<p>'.$text.'</p>';
    }

    return $html;
}

/**
 * Include image text to html
 *
 * @param string $html   Guide.
 * @param array  $imgtext  Image text.
 * @param string $dir   Current directory.
 * @param array  $folder   Exercise folder.
 *
 * @return string $html Text containing image text.
 */
function includeHtmlImg($html, $imgtext, $dir, $folder)
{
    $width = getImgWidth($imgtext);
    if (is_array($imgtext)) {
        if (!isset($imgtext[0]["alt"])) {
            $imgtext[0]["alt"] = '';
        }
        if (isset($imgtext[0]["file"])) {
            $html .= '<div class="text-center">'
                    .'<img class="img-question" alt="'.$imgtext[0]["alt"]
                    .'" src="'.$dir.'/'.$folder.$imgtext[0]["file"]
                    .'" width='.$width.'%></div>';  
        }
        if (isset($imgtext[0]["source"]) || isset($imgtext[0]["title"])) {
            $html = insertImgCaption($html, $imgtext, $width);
            print_r($html);
        }
    } else {
        $html .= '<img src='.$dir.'/'.$folder.$imgtext
                .' width='.$width.'%>'; 
    }

    return $html;
}

/**
 * Include youtube to html
 *
 * @param string $html  Guide.
 * @param string $youtube Youtube-link.
 *
 * @return string $html Text containing title.
 */
function includeHtmlYoutube($html, $youtube)
{
    $html .= '<div class="text-center"><a class="btn btn-warning" href="http://www.youtube.com/watch?v='.$youtube.'"><img src="eszkozok/images/logo_small.png" width="25%" alt="megoldas"> Megoldás</a></div>';

    return $html;
}



















/************************   INSERT - insert data to database    ********************/

/**
 * Insert class into table
 *
 * @param array $class Class data.
 *
 * @return NULL
 */
function insertClass($class)
{
    $conn = $_SESSION['conn'];

    if (!isset($_REQUEST['p'])) {
        $sql = 'INSERT INTO classes (alt,name) VALUES (\''
                    .$class["alt"].'\',\''
                    .$class["name"].'\')';
        $conn->query($sql) or die ('Failed to insert data into Classes.');
    }

    $sql = 'SELECT id FROM classes WHERE '
                .'alt = "'.$class["alt"].'" AND '
                .'name = "'.$class["name"].'"';
    $res = $conn->query($sql) or die ('Failed to select id from Classes.');

    $current = $res->fetch_assoc();

    $_SESSION['classID'] = $current["id"];

    return;
}

/**
 * Insert link into table
 *
 * @param int $exerciseID Exercise id.
 * @param array $link   Link data.
 *
 * @return NULL
 */
function insertLink($exerciseID, $link)
{

    $conn = $_SESSION['conn'];

    // insert link in both direction
    $sql = 'INSERT INTO links (id,link_class,link_topic,link_subtopic,link_label,link_type) VALUES '
            ."('"
                .$exerciseID."','"
                .$link["class"]."','"
                .$link["topic"]."','"
                .$link["subtopic"]."','"
                .$link["label"]."','"
                .$link["type"]
            ."')";

    $conn->query($sql) or die ('Failed to insert data into Links.'.$sql);

    return;
}

/**
 * Insert questions into table
 *
 * @param array $questions Question data.
 *
 * @return NULL
 */
function insertQuestions($questions)
{
    $id = $_SESSION['subtopicID'];
    $conn = $_SESSION['conn'];

    foreach ($questions as $question) {
        $html = '';
        $answer = $question["answer"];
        if (is_array($answer)) {
            $pic = 0;
            foreach ($answer[0] as $key => $value) {
                $dir = getCurrentDir('elmelet');
                
                if (strpos($key, 'text') !== FALSE) {
                    $html = includeText($html, $value);
                } else if (strpos($key, 'img') !== FALSE) {
                    $html = includeImgText($html, $dir, $value, $question, $pic);
                    $pic++;
                } else if (strpos($key, 'alert') !== FALSE) {
                    list($html, $pic) = includeAlert($html, $dir, $value, $question, $pic);
                } else if ($key === "carousel") {
                    $html = includeCarousel($html, $value, $dir);
                } else {
                    trigger_error("Ismeretlen állománytípus", E_USER_ERROR);
                }
            }
        } else {
            $html .= '<p>'.$answer.'</p>';
        }

        insertQandA($question["label"], $question["question"], $html);
    }
    return;
}

/**
 * Insert subtopic into table
 *
 * @param array  $subtopic Subtopic data.
 * @param array  $topicID  Topic id.
 * @param array  $classID  Class id.
 *
 * @return NULL
 */
function insertSubtopic($subtopic)
{
    $conn = $_SESSION['conn'];
    $classID = $_SESSION['classID'];
    $topicID = $_SESSION['topicID'];

    if (!isset($_REQUEST['p'])) {
        if (!isset($subtopic["type"])) {
            $subtopic["type"] = '';
        }
        if (!isset($subtopic["source"])) {
            $subtopic["source"] = '';
        }
        $sql = 'INSERT INTO subtopics (classID,topicID,alt,name,type,source)
                    VALUES (\''
                    .$classID.'\',\''
                    .$topicID.'\',\''
                    .$subtopic["alt"].'\',\''
                    .$subtopic["name"].'\',\''
                    .$subtopic["type"].'\',\''
                    .$subtopic["source"].'\')';
        $conn->query($sql) or die ('Failed to insert data into Subtopics.');
    }

    $sql = 'SELECT id FROM subtopics WHERE '
            .'classID = "'.$classID.'" AND '
            .'topicID = "'.$topicID.'" AND '
            .'alt = "'.$subtopic["alt"].'"';
    $res = $conn->query($sql) or die ('Failed to select id from Subtopics.');
    $current = $res->fetch_assoc();

    $_SESSION['subtopicID'] = $current["id"];

    return;
}

/**
 * Insert topic into table
 *
 * @param array $topic   Topic data.
 *
 * @return NULL
 */
function insertTopic($topic)
{
    $classID = $_SESSION['classID'];
    $conn = $_SESSION['conn'];

    if (!isset($_REQUEST['p'])) {
        $sql = 'INSERT INTO topics (classID,alt,name) VALUES (\''
                    .$classID.'\',\''
                    .$topic["alt"].'\',\''
                    .$topic["name"].'\')';
        $conn->query($sql) or die ('Failed to insert data into Topics.');
    }

    $sql = 'SELECT id FROM topics WHERE '
            .'classID = "'.$classID.'" AND '
            .'alt = "'.$topic["alt"].'"';
    $res = $conn->query($sql) or die ('Failed to select id from Topics.');
    $current = $res->fetch_assoc();

    $_SESSION['topicID'] = $current["id"];

    return;
}

/**
 * Include caption to image text
 *
 * @param string $html  Image text.
 * @param array  $imgtext Image data.
 * @param int   $width   Image width.
 *
 * @return string $html Image text including source.
 */
function insertImgCaption($html, $imgtext, $width)
{
    if ($width == 100) {
        $align = 'text-right';
    } else {
        $align = 'text-center';
    }
    $html .= '<p class="'.$align.' small image-description">';
    if (isset($imgtext[0]["title"])) {
        $html .= $imgtext[0]["title"];
    }
    if (isset($imgtext[0]["download"])) {
        $html .= ' (<a href="download.php?f='
            .$imgtext[0]["download"].'">Letöltés</a>)';
    }
    if (isset($imgtext[0]["source"])) {
        $html .= ' <a target="_blank" href="'.$imgtext[0]["source"].'">Forrás</a>';
    }
    echo '</p>';

    return $html;
}

/**
 * Insert question and answer into table
 *
 * @param string $label Label.
 * @param string $question Question.
 * @param string $answer   Answer.
 *
 * @return NULL
 */
function insertQandA($label, $question, $answer)
{
    $conn = $_SESSION['conn'];

    $subtopicID = $_SESSION['subtopicID'];
    $topicID = $_SESSION['topicID'];
    $classID = $_SESSION['classID'];

    $sql = 'INSERT INTO questions (classID,topicID,subtopicID,label,question,answer)
            VALUES (\''
            .$classID.'\',\''
            .$topicID.'\',\''
            .$subtopicID.'\',\''
            .$label.'\',\''
            .$question.'\',\''
            .$answer.'\')';
    
    $conn->query($sql) or die ('Failed to insert data into Questions.');
    return;
}

/**
 * Insert quiz into table
 *
 * @param array $quizzes Quiz data.
 *
 * @return NULL
 */
function insertQuiz($quizzes)
{
    $conn = $_SESSION['conn'];

    $subtopicID = $_SESSION['subtopicID'];
    $topicID = $_SESSION['topicID'];
    $classID = $_SESSION['classID'];

    foreach ($quizzes as $quiz) {

        $sql = 'INSERT INTO quiz (classID,topicID,subtopicID,question,correct,wrong1,wrong2)
                VALUES (\''
                .$classID.'\',\''
                .$topicID.'\',\''
                .$subtopicID.'\',\''
                .$quiz["question"].'\',\''
                .$quiz["correct"].'\',\''
                .$quiz["wrong1"].'\',\''
                .$quiz["wrong2"].'\')';
        $conn->query($sql) or die ('Failed to insert data into Quiz.');
    }
    return;
}

/**
 * Insert quiz into table
 *
 * @param array $exercises Exercises data.
 *
 * @return NULL
 */
function insertExercises($exercises)
{

    // $temp = $_SESSION['temp'];
    $conn = $_SESSION['conn'];
    $subtopicID = $_SESSION['subtopicID'];
    $topicID = $_SESSION['topicID'];
    $classID = $_SESSION['classID'];

    $basedir = getCurrentDir('feladatok');
    $ex_no = 1;

    foreach ($exercises as $exercise) {

        $folder = toAscii($exercise["label"]);
        $dir = $basedir.'/'.$folder;

        // ------------ ÚTMUTATÓ -------------- //
        $guide = '';
        $guide1 = '';
        $guide2 = '';
        $guide3 = '';
        $ex_question = '';
        $ex_answer = '';
        $youtube = '';
        $order = $ex_no;
        $source = '';

        if (isset($exercise["guide"])) {
            $data = $exercise["guide"];
            if (is_array($data)) {
                foreach ($data[0] as $kulcs => $ertek) {
                    if ($kulcs === "level1") {
                        $guide1 = includeHtml($ertek[0], $dir, $folder);
                    } elseif ($kulcs === "level2") {
                        $guide2 = includeHtml($ertek[0], $dir, $folder);
                    } elseif ($kulcs === "level3") {
                        $guide3 = includeHtml($ertek[0], $dir, $folder);
                    }
                }
            } else {
                $guide = includeHtml($data[0], $dir, $folder);
            }
        }
        if (isset($exercise["youtube"])) {
            $youtube = $exercise["youtube"];
        }
        if (isset($exercise["order"])) {
            $order = $exercise["order"];
        }
        if (isset($exercise["question"])) {
            $ex_question = includeHtml($exercise["question"][0], $dir, $folder);
        }
        if (isset($exercise["answer"])) {
            $ex_answer = includeHtml($exercise["answer"][0], $dir, $folder);
        }
        if (isset($exercise["source"])) {
            $source = $exercise["source"];
        }

        $sql = 'INSERT INTO exercises (subtopicID, topicID, classID, ex_no, label, guide, guide1,
                                    guide2, guide3, youtube, ex_question, ex_answer, source)
                VALUES (\''
                .$subtopicID.'\',\''
                .$topicID.'\',\''
                .$classID.'\',\''
                .$order.'\',\''
                .$exercise["label"].'\',\''
                .$guide.'\',\''
                .$guide1.'\',\''
                .$guide2.'\',\''
                .$guide3.'\',\''
                .$youtube.'\',\''
                .$ex_question.'\',\''
                .$ex_answer.'\',\''
                .$source.'\')';

        $conn->query($sql) or die ('Failed to insert data into Exercises.');

        if (isset($exercise["link"])) {
            $ex_Data = getExercises('subtopicID = "'.$subtopicID.'" AND label = "'.$exercise["label"].'"');
            foreach ($exercise["link"] as $link) {
                insertLink($ex_Data[0]["id"], $link);   
            }
        }

        $ex_no++;

    }
    return;
}

















?>