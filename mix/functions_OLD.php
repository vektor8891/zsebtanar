<?php
/**
 * Zsebtanár main API
 *
 * PHP Version 5
 *
 * @category PHP
 * @package  Zsebtanár
 * @author   Viktor Szabó <zsebtanar@gmail.com>
 * @license  http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
 * @link     http://zsebtanar.hu/
 */











/*****************************    MISC - set up etc.    ****************************/



/**
 * Get configuration data
 *
 * @param string $file Configuration file.
 *
 * @return array $configdata Configuration data.
 */
function getConfigData($file)
{
    $configdata = parse_ini_file($file);

    return $configdata;
}

/**
 * Connects to database
 *
 * @param string $file Configuration file.
 *
 * @return NULL
 */
function connectDatabase($file)
{
    $configdata = getConfigData($file);

    $servername = $configdata["servername"];
    $username = $configdata["username"];
    $password = $configdata["password"];
    $dbname = $configdata["dbname"];

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        $conn = new mysqli($servername, $username, $password)
            or die('Connection failed: ' . $conn->connect_error);
    }

    $_SESSION['conn'] = $conn;
    $_SESSION['dbname'] = $dbname;

    return;
}

/**
 * Drops existing database
 *
 * @return string $error Error message (if any).
 */
function dropDatabase()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_torol = 'DROP DATABASE IF EXISTS '.$dbname;
    $conn->query($sql_torol);
    return $conn->error;
}

/**
 * Creates new database
 *
 * @return string $error Error message (if any).
 */
function createDatabase()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_letrehoz = 'CREATE DATABASE '.$dbname;
    $conn->query($sql_letrehoz);
    return $conn->error;
}

/**
 * Creates tables
 *
 * @return string $error Error message (if any).
 */
function createTables()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql_db = 'USE '.$dbname;
    $conn->query($sql_db);
    echo $conn->error;
    createClasses($dbname, $conn);
    createTopics($dbname, $conn);
    createSubtopics($dbname, $conn);
    createQuestions($dbname, $conn);
    createQuiz($dbname, $conn);
    createExercises($dbname, $conn);
}

/**
 * Redirect to main page
 *
 * @return NULL
 */
function redirectMain()
{
    $p = '';
    $f = '';
    if (isset($_REQUEST["p"])) {
        $p = '?p='.$_REQUEST["p"];
        if (isset($_REQUEST["f"])) {
            $f = '&f='.$_REQUEST["f"];
        }
    }

    header("Location: index.php".$p.$f);
    return;
}

/**
 * Creates image with watermark
 *
 * @param string $pic   Image path.
 * @param int    $width Width of image.
 *
 * @return bool Whether image is created.
 */
function createImgWatermark($pic, $width)
{
    $image = imagecreatefromjpeg($pic.'.jpg');
    $w = imagesx($image);
    $h = imagesy($image);
    $watermark = imagecreatefrompng('eszkozok/images/logo_medium.png');
    $ww = imagesx($watermark);
    $wh = imagesy($watermark);
    $w2 = $w / 2000 * $ww * 100 / $width;
    $h2 = $w / 2000 * $wh * 100 / $width;
    imagecopyresampled($image, $watermark, 5, 5, 0, 0, $w2, $h2, $ww, $wh);
    if (file_exists($pic.'_logo.jpg') != 1) {
        imagejpeg($image, $pic.'_logo.jpg', 95);
        return true;
    } else {
        return false;
    }
}

/**
 * Convert string to Ascii
 *
 * @param string $str Original string.
 *
 * @return string $clean Modified string.
 */
function toAscii($str)
{
    $accents = array('Á'=>'A', 'É'=>'E', 'Í'=>'I', 'Ó'=>'O', 'Ö'=>'O',
                    'Ő'=>'O', 'Ú'=>'U', 'Ü'=>'U', 'Ű'=>'U',
                    'á'=>'a', 'é'=>'e', 'í'=>'i', 'ó'=>'o', 'ö'=>'o',
                    'ő'=>'o', 'ú'=>'u', 'ü'=>'u', 'ű'=>'u');
    $clean = strtr($str, $accents);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

    return $clean;
}









/******************************   CREATE - create tables    ************************/



/**
 * Creates table for classes
 *
 * @return string $error Error message (if any).
 */
function createClasses()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE classes (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                alt CHAR(60) NOT NULL,
                name CHAR(60) NOT NULL,
                PRIMARY KEY (id)
            );';
    $conn->query($sql) or die ('Failed to create table Classes.');
    return $conn->error;
}

/**
 * Creates table for exercises
 *
 * @return string $error Error message (if any).
 */
function createExercises()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE feladat (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                tema SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                sorrend SMALLINT UNSIGNED NOT NULL,
                fajl CHAR(120) NOT NULL,
                label CHAR(120) NOT NULL,
                utmutato TEXT NOT NULL,
                utmutato1 TEXT NOT NULL,
                utmutato2 TEXT NOT NULL,
                utmutato3 TEXT NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Exercises.');
    return $conn->error;
}

/**
 * Creates table for questions
 *
 * @return string $error Error message (if any).
 */
function createQuestions()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE questions (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                osztaly SMALLINT UNSIGNED NOT NULL REFERENCES classes(alt),
                tema SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                label CHAR(60) NOT NULL,
                question TEXT NOT NULL,
                valasz TEXT NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Questions.');
    return $conn->error;
}

/**
 * Creates table for quiz
 *
 * @return string $error Error message (if any).
 */
function createQuiz()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE kviz (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                tema SMALLINT UNSIGNED NOT NULL REFERENCES subtopics(id),
                question TEXT NOT NULL,
                jo CHAR(120) NOT NULL,
                rossz1 CHAR(120) NOT NULL,
                rossz2 CHAR(120) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Quiz.');
    return $conn->error;
}

/**
 * Creates table for subtopics
 *
 * @return string $error Error message (if any).
 */
function createSubtopics()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE subtopics (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                osztaly SMALLINT UNSIGNED NOT NULL REFERENCES classes(alt),
                temakor CHAR(60) NOT NULL REFERENCES topics(alt),
                alt CHAR(60) NOT NULL,
                name CHAR(60) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Subtopics.');
    return $conn->error;
}

/**
 * Creates table for topics
 *
 * @return string $error Error message (if any).
 */
function createTopics()
{
    $conn = $_SESSION['conn'];
    $dbname = $_SESSION['dbname'];
    $sql = 'CREATE TABLE topics (
                id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
                osztaly SMALLINT UNSIGNED NOT NULL REFERENCES classes(alt),
                alt CHAR(60) NOT NULL,
                name CHAR(60) NOT NULL,
                PRIMARY KEY (id)
            )';
    $conn->query($sql) or die ('Failed to create table Topics.');
    return $conn->error;
}

















/*******************   GET  - get data (e.g. from database)   **********************/

/**
 * Get classes from database
 *
 * @return object $res Result of query.
 */
function getClasses()
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT id,alt,name FROM classes';
    $res = $conn->query($sql) or die ('Failed to select classes.');
    if ($res->num_rows < 1) {
        die ('No classes were selected');
    }
    return $res;
}



/**
 * Get classes from database
 *
 * @param $alt Class alt name.
 *
 * @return object $res Result of query.
 */
function getClassData($alt)
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT name FROM classes WHERE alt='.$alt;
    $res = $conn->query($sql) or die ('Failed to select classes.');
    if ($res->num_rows < 1) {
        die ('No classes were selected');
    }
    return $res;
}

/**
 * Get exercises
 *
 * @param int    $id   Subtopic id.
 *
 * @return object $res Result of query.
 */
function getExercises($id)
{
    $conn = $_SESSION['conn'];
    $sql = "SELECT sorrend,fajl,label,utmutato,utmutato1,utmutato2,utmutato3
            FROM feladat WHERE tema = ".$id;
    $res = $conn->query($sql) or die ('Failed to select exercises.');
    return $res;
}

/**
 * Get topics of a class
 *
  * @param string $class Class name.
 *
 * @return object $res Result of query.
 */
function getTopics($class)
{
    $conn = $_SESSION['conn'];
    $sql ='SELECT alt,name FROM topics WHERE osztaly ='.$class;
    $res = $conn->query($sql) or die ('Failed to select topics.');
    return $res;
}

/**
 * Get subtopics of a topic
 *
 * @param string $topic Topic name.
 *
 * @return object $res Result of query.
 */
function getSubTopics($topic)
{
    $conn = $_SESSION['conn'];
    $sql ='SELECT id,name FROM subtopics WHERE temakor ="'.$topic.'"';
    $res = $conn->query($sql) or die ('Failed to select subtopics.');
    return $res;
}

/**
 * Get id of subtopic from database
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return string $id ID of current subtopic.
 */
function getSubtopicID($subtopic)
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT id FROM subtopics
                WHERE alt = "'.$subtopic["alt"].'"';
    $res = $conn->query($sql) or die ('Failed to select id from Subtopics.');
    $current = $res->fetch_assoc();
    return $current["id"];
}

/**
 * Get id of last subtopic from database
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return string $id ID of last subtopic.
 */
function getSubtopicIDLast($subtopic)
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT id FROM subtopics ORDER BY id DESC';
    $res = $conn->query($sql) or die ('Failed to select subtopic.');
    if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();
        $id = $row["id"];
    }
    return $id;
}

/**
 * Get data of subtopic from database
 *
 * @param int    $id   Subtopic id.
 *
 * @return array $data Subtopic data.
 */
function getSubtopicData($id)
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT id,temakor,osztaly,alt,name 
            FROM subtopics
            WHERE id = '.$id;
    $data = $conn->query($sql) or die ('Failed to get Subtopic data.');

    return $data;
}

/**
 * Get questions from database
 *
 * @param int    $id   Subtopic id.
 *
 * @return array $data Question data.
 */
function getQuestionData($id)
{
    $conn = $_SESSION['conn'];
    $sql = "SELECT label,question,valasz FROM questions WHERE tema = ".$id;
    $data = $conn->query($sql) or die ('Failed to get Question data.');

    return $data;
}

/**
 * Get class of subtopic from database
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return string $class ID of current subtopic.
 */
function getSubtopicClass($subtopic)
{
    $conn = $_SESSION['conn'];
    $sql = 'SELECT osztaly FROM subtopics
                WHERE alt = "'.$subtopic["alt"].'"';
    $res = $conn->query($sql) or die ('Failed to select class from Subtopics.');
    $current = $res->fetch_assoc();
    return $current["osztaly"];
}

/**
 * Creates path to folder
 *
 * Possible values for 'folder': 'questions' or 'feladatok'.
 *
 * @param array $subtopic Subtopic data.
 * @param array $topic    Topic data.
 * @param array $class    Class data.
 * @param array $folder   Name of folder.
 *
 * @return string $dir Path.
 */
function getCurrentDir($subtopic, $topic, $class, $folder)
{
    $dir = 'tananyag/'.$class[0]["alt"]
                    .'/'.$topic["alt"]
                    .'/'.$subtopic["alt"]
                    .'/'.$folder;
    return $dir;
}

/**
 * Calculates width of image
 *
 * @param array $imgtext Image data.
 *
 * @return int $width Width of image.
 */
function getImgWidth($imgtext)
{
    if (isset($imgtext[0]["width"])) {
        $width = $imgtext[0]["width"];
    } else {
        $width = 100;
    }

    return $width;
}
















/*************************   INCLUDE - include data to string    *******************/

/**
 * Include text to answer
 *
 * @param string $html Answer.
 * @param string $text Text.
 *
 * @return string $html Answer including text.
 */
function includeText($html, $text)
{
    $html .= '<p>'.$text.'</p>';
    return $html;
}

/**
 * Include image to answer
 *
 * @param string $html     Answer.
 * @param string $dir      Path to image.
 * @param array  $imgtext  Image data.
 * @param array  $question Questions data.
 *
 * @return string $html Answer including image text.
 */
function includeImgText($html, $dir, $imgtext, $question)
{
    $pic = $dir.'/'.$imgtext[0]["file"];
    $width = getImgWidth($imgtext);
    if (isset($imgtext[0]["change"]) && $imgtext[0]["change"]) {
        // vízjel
        createImgWatermark($pic, $width);
        $html .= '<img '
            .'id="kep-'.toAscii($question["label"]).'" '
            .'class="img-question center-block" '
            .'src="'.$pic.'_logo.jpg" '
            .'width="'.$width.'%" '
            .'onclick="kepCsere(\\\'kep-'
            .toAscii($question["label"]).'\\\')" '
            .'alt="'.$imgtext[0]["alt"].'">';
    } else {
        $html .= '<img '
            .'id="kep-'.toAscii($question["label"]).'" '
            .'class="img-question center-block" '
            .'src="'.$pic.'.jpg" width="'.$width.'%">';
    }
    if (isset($imgtext[0]["source"])) {
        $html = insertImgSource($html, $imgtext);
    }
    if (isset($imgtext[0]["title"])) {
        $html = insertImgTitle($html, $imgtext);
    }
    return $html;
}

/**
 * Include alert to answer
 *
 * @param string $html    Answer.
 * @param array  $imgtext Image data.
 *
 * @return string $html Answer including alert.
 */
function includeAlert($html, $imgtext)
{
    $html .= '<div class="alert alert-'.$imgtext[0]["class"].'">'
            .'<b>'.$imgtext[0]["title"].'</b> '
            .$imgtext[0]["text"].'</div>';
    return $html;
}

/**
 * Include carousel to answer
 *
 * @param string $html    Answer.
 * @param array  $imgtext Image data.
 * @param string $dir     Path to image.
 *
 * @return string $html Answer including alert.
 */
function includeCarousel($html, $imgtext, $dir)
{
    $html .= '<div id="myCarousel" class="carousel slide" data-ride="carousel">'
            .'<ol class="carousel-indicators">';
    for ($i=0; $i < $imgtext[0]["darab"]; $i++) {
        $html = includeCarouselIndicator($html, $i);
    }
    $html .= '</ol><div class="carousel-inner" role="listbox">';

    $pic = $dir.'/'.$imgtext[0]["nev"].'/'.$imgtext[0]["nev"];
    
    $width = getImgWidth($imgtext);

    for ($i=1; $i <= $imgtext[0]["darab"]; $i++) {
        $html = includeCarouselInner($html, $pic, $width, $i);
    }
    $html .= '<a class="left carousel-control" href="#myCarousel" '
            .'role="button" data-slide="prev">'
            .'<span class="glyphicon glyphicon-chevron-left" '
            .'aria-hidden="true"></span><span class="sr-only">Previous</span>'
            .'</a><a class="right carousel-control" href="#myCarousel" '
            .'role="button" data-slide="next">'
            .'<span class="glyphicon glyphicon-chevron-right" '
            .'aria-hidden="true"></span><span class="sr-only">Next</span></a>'
            .'</div></div>';

    return $html;
}

/**
 * Include carousel indicator to answer
 *
 * @param string $html Answer.
 * @param int    $i    Indicator number.
 *
 * @return string $html Answer including carousel indicator.
 */
function includeCarouselIndicator($html, $i)
{
    if ($i == 1) {
        $class = ' class="active" ';
    } else {
        $class = ' ';
    }
    $html .= '<li data-target="#myCarousel" '
            .'data-slide-to="'.$i.'"'.$class.'></li>';

    return $html;
}

/**
 * Include carousel inner link to answer
 *
 * @param string $html  Answer.
 * @param string $pic   Image path.
 * @param int    $width Width of image.
 * @param int    $i     Indicator number.
 *
 * @return string $html Answer including carousel inner link.
 */
function includeCarouselInner($html, $pic, $width, $i)
{
    if ($i == 1) {
        $class = 'item active';
    } else {
        $class = 'item';
    }
    $html .= '<div class="'.$class.'">'
                .'<img src="'.$pic.$i.'.jpg" width="'.$width.'%">'
            .'</div>';

    return $html;
}

/**
 * Include guide text to exercise
 *
 * @param array  $guidedata Guide data.
 * @param string $dir       Current directory.
 * @param array  $exercise  Exercise data.
 *
 * @return string $html Guide text.
 */
function includeGuide($guidedata, $dir, $exercise)
{
    $html = '';
    foreach ($guidedata as $key => $value) {
        if (strpos($key, "text") !== false) {
            $html = includeGuideText($html, $value);
        } elseif (strpos($key, "img") !== false) {
            $html = includeGuideImg($html, $value, $dir, $exercise);
        } elseif (strpos($key, "title") !== false) {
            $html = includeGuideTitle($html, $value);
        } else {
            trigger_error("Ismeretlen állománytípus", E_USER_ERROR);
        }
    }

    return $html;
}

/**
 * Include title to guide
 *
 * @param string $html  Guide.
 * @param array  $title Text.
 *
 * @return string $html Guide containing title.
 */
function includeGuideTitle($html, $title)
{
    $html .= '<h2>'.$value.'</h2>';

    return $html;
}

/**
 * Include text to guide
 *
 * @param string $html Guide.
 * @param array  $text Text.
 *
 * @return string $html Guide containing text.
 */
function includeGuideText($html, $text)
{
    if (is_array($text)) {
        if (isset($text[0]["class"])) {
            $html .= '<p class="'.$text[0]["class"].'">';
        } else {
            $html .= '<p>';
        }
        if (isset($text[0]["text"])) {
            $html .= $text[0]["text"];
        }
    } else {
        $html .= '<p>'.$text.'</p>';
    }

    return $html;
}

/**
 * Include image text guide.
 *
 * @param string $html     Guide.
 * @param array  $imgtext  Image text.
 * @param string $dir      Current directory.
 * @param array  $exercise Exercise data.
 *
 * @return string $html Guide containing image text.
 */
function includeGuideImg($html, $imgtext, $dir, $exercise)
{
    $width = getImgWidth($imgtext);
    if (is_array($imgtext)) {
        if (isset($imgtext[0]["fajl"])) {
            $html .= '<div class="text-center">'
                    .'<img src="'.$dir.'/'.$exercise["fajl"].$imgtext[0]["fajl"]
                    .'" width='.$width.'%></div>';    
        }
    } else {
        $html .= '<img src='.$dir.'/'.$exercise["fajl"].$imgtext
                .' width='.$width.'%>';    
    }

    return $html;
}















/************************   INSERT - insert data to database    ********************/

/**
 * Inserts class into table
 *
 * @param array  $class Class data.
 *
 * @return NULL
 */
function insertClass($class)
{
    $conn = $_SESSION['conn'];
    $sql = 'INSERT INTO classes (alt,name) VALUES (\''
                .$class[0]["alt"].'\',\''
                .$class[0]["name"].'\')';
    $conn->query($sql) or die ('Failed to insert data into Classes.');
    return;
}

/**
 * Inserts questions into table
 *
 * @param array  $subtopic Subtopic data.
 * @param array  $topic    Topic data.
 * @param array  $class    Class data.
 *
 * @return NULL
 */
function insertQuestions($subtopic, $topic, $class)
{
    $conn = $_SESSION['conn'];
    foreach ($subtopic["questions"] as $question) {
        $html = '';
        $answer = $question["valasz"];
        if (is_array($answer)) {
            foreach ($answer[0] as $key => $value) {
                $dir = getCurrentDir($subtopic, $topic, $class, 'elmelet');
                
                if (strpos($key, 'text') !== false) {
                    $html = includeText($html, $value);
                } else if (strpos($key, 'img') !== false) {
                    $html = includeImgText($html, $dir, $value, $question);
                } else if ($key === "alert") {
                    $html = includeAlert($html, $value);
                } else if ($key === "carousel") {
                    $html = includeCarousel($html, $value, $dir);
                } else {
                    trigger_error("Ismeretlen állománytípus", E_USER_ERROR);
                }
            }
        } else {
            $html .= $answer;
        }

        insertQandA($subtopic, $question, $html);
    }
    return;
}

/**
 * Inserts subtopic into table
 *
 * @param array  $subtopic Subtopic data.
 * @param array  $topic    Topic data.
 * @param array  $class    Class data.
 *
 * @return NULL
 */
function insertSubtopic($subtopic, $topic, $class)
{
    $conn = $_SESSION['conn'];
    $sql = 'INSERT INTO subtopics (osztaly,temakor,alt,name)
                VALUES (\''
                .$class[0]["alt"].'\',\''
                .$topic["alt"].'\',\''
                .$subtopic["alt"].'\',\''
                .$subtopic["name"].'\')';
    $conn->query($sql) or die ('Failed to insert data into Subtopics.');
    return;
}

/**
 * Inserts topic into table
 *
 * @param array  $topic Topic data.
 * @param array  $class Class data.
 *
 * @return NULL
 */
function insertTopic($topic, $class)
{
    $conn = $_SESSION['conn'];
    $sql = 'INSERT INTO topics (osztaly,alt,name) VALUES (\''
                .$class[0]["alt"].'\',\''
                .$topic["alt"].'\',\''
                .$topic["name"].'\')';
    $conn->query($sql) or die ('Failed to insert data into Topics.');
    return;
}

/**
 * Include source to image text
 *
 * @param string $html    Image text.
 * @param array  $imgtext Image data.
 *
 * @return string $html Image text including source.
 */
function insertImgSource($html, $imgtext)
{
    $html .= '<p '
        .'class="text-right small image-description">'
        .'<a target="_blank" '
        .'href="'.$imgtext[0]["source"].'">'
        .'Forrás</a></p>';

    return $html;
}

/**
 * Include title to image text
 *
 * @param string $html    Image text.
 * @param array  $imgtext Image data.
 *
 * @return string $html Image text including title.
 */
function insertImgTitle($html, $imgtext)
{
    $html .= '<p '
        .'class="text-center small image-description">'
        .$imgtext[0]["title"];
    if (isset($imgtext[0]["download"])) {
        $html .= ' (<a href="download.php?f='
            .$imgtext[0]["download"].'">Letöltés</a>)';
    }
    $html .= '</p>';

    return $html;
}

/**
 * Inserts question and answer into table
 *
 * @param array  $subtopic Subtopic data.
 * @param array  $QandA    Questions & answers data.
 * @param html   $answer   Answer text.
 *
 * @return NULL
 */
function insertQandA($subtopic, $QandA, $answer)
{
    $conn = $_SESSION['conn'];
    $id = getSubtopicID($subtopic);
    $class = getSubtopicClass($subtopic);

    $sql = 'INSERT INTO questions (osztaly,tema,label,question,valasz)
            VALUES (\''
            .$class.'\',\''
            .$id.'\',\''
            .$QandA["label"].'\',\''
            .$QandA["question"].'\',\''
            .$answer.'\')';
    $conn->query($sql) or die ('Failed to insert data into Questions.');
    return;
}

/**
 * Inserts quiz into table
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return string $error Error message (if any).
 */
function insertQuiz($subtopic)
{
    $conn = $_SESSION['conn'];
    $id = getSubtopicID($subtopic);

    foreach ($subtopic["kviz"] as $quiz) {
        $sql = 'INSERT INTO kviz (tema,question,jo,rossz1,rossz2)
                VALUES (\''
                .$id.'\',\''
                .$quiz["question"].'\',\''
                .$quiz["jo"].'\',\''
                .$quiz["rossz1"].'\',\''
                .$quiz["rossz2"].'\')';
        $conn->query($sql) or die ('Failed to insert data into Quiz.');
    }
}

/**
 * Inserts quiz into table
 *
 * @param array  $subtopic     Subtopic data.
 * @param array  $topic        Topic data.
 * @param array  $class        Class data.
 *
 * @return NULL
 */
function insertExercises($subtopic, $topic, $class)
{
    $conn = $_SESSION['conn'];
    $id = getSubtopicID($subtopic);
    $basedir = getCurrentDir($subtopic, $topic, $class, 'feladatok');
    $sorrend = 1;

    foreach ($subtopic["feladat"] as $exercise) {
        
        $dir = $basedir.'/'.$exercise["fajl"];
        $phpfile = $dir.'/'.$exercise["fajl"].".php";

        // ------------ ÚTMUTATÓ -------------- //
        $guide = '';
        $guide1 = '';
        $guide2 = '';
        $guide3 = '';
        if (isset($exercise["utmutato"])) {
            $adat7 = $exercise["utmutato"];
            if (is_array($adat7)) {
                foreach ($adat7[0] as $kulcs => $ertek) {
                    if ($kulcs === "konnyu") {
                        $guide1 = includeGuide($ertek[0], $dir, $exercise);
                    } elseif ($kulcs === "kozepes") {
                        $guide2 = includeGuide($ertek[0], $dir, $exercise);
                    } elseif ($kulcs === "nehez") {
                        $guide3 = includeGuide($ertek[0], $dir, $exercise);
                    }
                }
            } else {
                $guide = includeGuide($ertek, $dir, $exercise);
            }
        }
        $sql = 'INSERT INTO feladat (tema, sorrend, fajl, label, utmutato, utmutato1,
                                    utmutato2, utmutato3)
                VALUES (\''
                .$id.'\',\''
                .$sorrend.'\',\''
                .$phpfile.'\',\''
                .$exercise["label"].'\',\''
                .$guide.'\',\''
                .$guide1.'\',\''
                .$guide2.'\',\''
                .$guide3.'\')';
        $conn->query($sql) or die ('Failed to insert data into Exercises.');
        $sorrend++;
    }
    return;
}


















/**************************    PRINT - create html data    *************************/

/**
 * Print questions and answers
 *
 * Get questions and answers from database and prints into php file.
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return NULL
 */
function printQuestions($subtopic)
{
    $questions = getQuestionData($subtopic["id"]);
    if (($questions -> num_rows) > 0) {
        while ($question = $questions->fetch_assoc()) {
            echo '<div id="'.toAscii($question["label"]).'" class="anchor"></div>
                    <h1>'.$question["question"].'</h1>'
                    .$question["valasz"]
                    .'<div class="question-bottom"></div>';
        }
    }
    return;
}

/**
 * Print navbar menu
 *
 * @return NULL
 */
function printNavBarMenu()
{
    $classes = getClasses();
    while ($class = $classes->fetch_assoc()) {
        echo '<li class="dropdown">'
            .'<a href="#" class="dropdown-toggle" data-toggle="dropdown" '
            .'role="button" aria-expanded="false">';
        echo $class["name"];
        $topics = getTopics($class["alt"]);
        if ($topics->num_rows > 0) {
            echo '<span class="caret"></span></a>';
            echo '<ul class="dropdown-menu" role="menu">';
            while($topic = $topics->fetch_assoc()) {
                $subtopics = getSubTopics($topic["alt"]);
                if ($subtopics->num_rows > 0) {
                    $subtopic = $subtopics->fetch_assoc();
                    echo'<li><a href="index.php?p='.$subtopic["id"].'">'
                        .$topic["name"].'</a></li>';
                }
            }
            echo '</ul>';
        } else {
            echo '</a></li>';
        }
    }
    return;
}

/**
 * Print refresh icon
 *
 * @return NULL
 */
function printRefreshIcon()
{
    $p = '';
    $f = '';
    if (isset($_REQUEST["p"])) {
        $p = '?p='.$_REQUEST["p"];
        if (isset($_REQUEST["f"])) {
            $f = '&f='.$_REQUEST["f"];
        }
    }
    echo '<li><a href="beolvas.php'.$p.$f.'">'
            .'<span class="glyphicon glyphicon-refresh"></span> Frissít</a>'
        .'</li>';
    return;
}

/**
 * Print typeahead data
 *
 * @return NULL
 */
function printTypeaheadData()
{
    $conn = $_SESSION['conn'];
    $sql1 = 'SELECT id,alt FROM classes';
    $res1 = $conn->query($sql1) or die ('Failed to select classes.');
    if ($res1->num_rows > 0) {
        while ($row1 = $res1->fetch_assoc()) {
            $data = 'osztaly'.$row1["alt"].': [';
            $sql2 = 'SELECT question,tema,label FROM questions
                     WHERE osztaly = '.$row1["alt"];
            $res2 = $conn->query($sql2) or die ('Failed to select questions.');
            if ($res2->num_rows > 0) {
                while ($row2 = $res2->fetch_assoc()) {
                    $row2["question"] = str_replace('"', '\\"', $row2["question"]);
                    $data = $data.'"'.$row2["question"].'",';
                }
                $data = rtrim($data, ',');
            }
            $data = $data.'],';
        }
        print_r($data);
    }
    return;
}

/**
 * Print typeahead source
 *
 * @return NULL
 */
function printTypeaheadSource()
{
    $conn = $_SESSION['conn'];
    $sql1 = "SELECT alt,name FROM classes";
    $res1 = $conn->query($sql1) or die ('Failed to select classes.');
    $data = "";
    if ($res1->num_rows > 0) {
        while ($row1 = $res1->fetch_assoc()) {
            $data = '"'.$row1["name"].'"'
                    .': { data: data_typeahead.osztaly'.$row1["alt"].'},';
        }
        $data = rtrim($data, ',');
    }
    print_r($data);
    return;
}

/**
 * Print subtopics
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return NULL
 */
function printSubtopics($subtopic)
{
    $conn = $_SESSION['conn'];
    echo '<div id="sidebar"><div class="list-group panel">';
    $sql2 = 'SELECT id,alt,name
             FROM subtopics
             WHERE temakor = "'.$subtopic["temakor"].'"';
    $res2 = $conn->query($sql2) or die ('Failed to select subtopic.');
    if ($res2->num_rows > 0) {
        while ($row2 = $res2->fetch_assoc()) {
            $id = $row2["id"];
            echo '<a id="menu'.$id.'" href="#almenu'.$id
                .'" class="list-group-item list-group-item-default" '
                .'data-toggle="collapse" data-parent="#siderbar">'
                .$row2["name"].'</a>';
            $sql3 = "SELECT label,question
                     FROM questions
                     WHERE tema = ".$id;
            $res3 = $conn->query($sql3) or die ('Failed to select questions.');
            if ($res3->num_rows > 0) {
                echo '<div class="collapse" id="almenu'.$id.'">';
                while ($row3 = $res3->fetch_assoc()) {
                    echo '<a href="index.php?p='.$id.'#'
                        .$id.'_'.toAscii($row3["label"]).'" '
                        .'data-parent="#almenu'.$id.'" '
                        .'class="list-group-item list-group-sub-item">'
                        .$row3["question"].'</a>';
                }
                $sql3b = "SELECT fajl
                          FROM feladat
                          WHERE tema = ".$id;
                $res3b = $conn->query($sql3b) or die ('Failed to select exercises.');
                if ($res3b->num_rows > 0) {
                    echo '<a href="index.php?p='.$id.'&f=1" '
                        .'data-parent="#almenu'.$id.'" '
                        .'class="list-group-item list-group-sub-item">Feladatok</a>';
                }
                echo '</div>';
            }
        }
    }
    echo '</div></div>';
    return;
}

/**
 * Print sidebar for questions
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return NULL
 */
function printSideBarQuestions($subtopic)
{
    $conn = $_SESSION['conn'];
    echo '<ul class="nav nav-pills nav-stacked" '
            .'data-spy="affix" data-offset-top="205" id="sidebar">';
    $id = $subtopic["id"];
    $sql = "SELECT label,question
             FROM questions
             WHERE tema = ".$id;
    $res = $conn->query($sql) or die ('Failed to select questions.');
    if ($res->num_rows > 0) {
        $active = true;
        while ($row3 = $res->fetch_assoc()) {
            if ($active) {
                $class = ' class="active"';
                $active = false;
            } else {
                $class = '';
            }
            echo '<li'.$class.'>
                    <a href="#'.toAscii($row3["label"]).'">'
                    .$row3["label"].'</a>
                <li>';
        }
        $sql3b = "SELECT fajl
                  FROM feladat
                  WHERE tema = ".$id;
        $res3b = $conn->query($sql3b) or die ('Failed to select exercises.');
        if ($res3b->num_rows > 0) {
            echo '<li><a href="index.php?p='.$id.'&f=1"><b>FELADATOK</b></a><li>';
        }
        echo '<li><a href="#" data-toggle="modal" '
            .'data-target="#kviz"><b>TESZT</b></a></li>';
    }
    echo '</ul>';
    return;
}

/**
 * Print sidebar for exercises
 *
 * @param int    $id   Subtopic id.
 *
 * @return NULL
 */
function printSideBarExercises($id)
{
    echo '<ul class="nav nav-pills nav-stacked" '
            .'data-spy="affix" data-offset-top="205" id="sidebar">';
    $res = getExercises($id);
    if ($res->num_rows > 0) {
        $active = true;
        while ($row = $res->fetch_assoc()) {
            if ($active) {
                $class = ' class="active"';
                $active = false;
            } else {
                $class = '';
            }
            echo '<li'.$class.'>
                    <a href="#'.toAscii($row["label"]).'">'
                    .$row["label"].'</a>
                <li>';
        }
    }
    echo '<li><a href="index.php?p='.$id.'"><b>ELMÉLET</b></a><li>';
    echo '<li><a href="#" data-toggle="modal" '
            .'data-target="#kviz"><b>TESZT</b></a></li>';
    echo '</ul>';
    return;
}

/**
 * Print quiz
 *
 * @param int    $id   Subtopic id.
 *
 * @return NULL
 */
function printQuiz($id)
{
    $conn = $_SESSION['conn'];
    $sql = "SELECT question,jo,rossz1,rossz2 FROM kviz WHERE tema = ".$id;
    $res = $conn->query($sql) or die ('Failed to select quiz.');
    if ($res->num_rows > 0) {
        // printQuizButton($id);
        printQuizHtml();
        printQuizJs($res);
    }
    return;
}

/**
 * Print quiz button
 *
 * @param int    $id   Subtopic id.
 *
 * @return NULL
 */
function printQuizButton($id)
{
    $conn = $_SESSION['conn'];
    echo '<div class="text-center padding-top-extra">'
        .'<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" '
        .'data-target="#kviz">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teszt&nbsp;&nbsp;&nbsp;'
        .'&nbsp;&nbsp;</button>';
    $sql = "SELECT fajl FROM feladat WHERE tema = ".$id;
    $res = $conn->query($sql) or die ('Failed to select exercises.');
    if ($res->num_rows > 0) {
        echo '<a href="index.php?p='.$id.'&f=1" type="button" '
            .'class="btn btn-success btn-lg">Feladatok</a>';
    }
    echo '</div>';
    return;
}

/**
 * Print quiz html
 *
 * @return NULL
 */
function printQuizHtml()
{
    echo '<div id="example"></div>
            <div id="kviz" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><div id="question"></h4>
                  </div>
                  <div class="modal-body">
                    <div id="options"></div>
                    <div id="error" class="text-center"></div>
                    <div id="result"></div>
                  </div>
                  <div class="modal-footer">
                    <div id="button"></div>
                  </div>
                </div>
              </div>
            </div>';
    return;
}

/**
 * Print quiz js
 *
 * @param array $quizdata Quizdata.
 *
 * @return NULL
 */
function printQuizJs($quizdata)
{
    echo '<script type="text/javascript">';
    echo 'var questions = [';
    while ($quiz = $quizdata->fetch_assoc()) {
        echo '{label: \''.$quiz["question"]
            .'\',options: [\''.$quiz["jo"].'\',\''
            .$quiz["rossz1"].'\',\''
            .$quiz["rossz2"].'\'],answer : [\''
            .$quiz["jo"].'\'],forceAnswer : true},';
    }
    echo '];';
    echo 'function ujratolt() {
            location.reload();
        }

        var quizMaker = new DG.QuizMaker({
            questions : questions,
            questions : shuffle(questions),
                el : \'options\',
                el2 : \'question\',
                el3 : \'button\',
            forceCorrectAnswer:false,
            listeners : {
                \'finish\' : showScore,
                \'missinganswer\' : showAnswerAlert,
                \'sendanswer\' : clearErrorBox,
                \'wrongAnswer\' : showWrongAnswer
              }
            });
            quizMaker.start();
            /* kérdésfrissítés */
            // $(\'body\').on(\'hidden.bs.modal\', \'.modal\', '
                .'function (){ document.location.reload();});';
    echo '</script>';
    return;
}

/**
 * Print page navigation
 *
 * @param array $data Subtopic data.
 *
 * @return NULL
 */
function printPageNav($data)
{
    $max = getSubtopicIDLast($data);
    $id = getSubtopicID($data);
    $res1 = getSubtopicData($id-1);
    $res2 = getSubtopicData($id+1);
    $subtopic1 = $res1->fetch_assoc();
    $subtopic2 = $res2->fetch_assoc();
    echo '<div class="row">
            <div class="col-md-5 col-md-5-navbar-left">
                <ul class="pager">';
    if ($id > 1) {
        $prev = $id-1;
        echo    '<li class="previous">
                    <a href="index.php?p='.$prev.'">
                        <span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="pagerhidden" href="index.php?p='.$prev.'">'
                        .$subtopic1["name"].'</a>
                </li>';
    }
    echo '      </ul>
            </div>
            <div class="col-md-2 col-md-2-navbar-center">
                <ul class="pager">
                    <li class="home">
                        <a href="index.php">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5 col-md-5-navbar-right">
                <ul class="pager">';
    if ($id < $max) {
        $next = $id+1;
        echo    '<li class="next">
                    <a href="index.php?p='.$next.'">
                        <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <a class="pagerhidden" href="index.php?p='.$next.'">'
                        .$subtopic2["name"].'</a>
                </li>';
    }
    echo '      </ul>
            </div>
        </div>';
    return;
}

/**
 * Print main page
 *
 * @return NULL
 */
function printMainPage()
{
    echo'<div class="jumbotron">
            <img class="img-responsive center-block" '
            .'src="eszkozok/images/logo.png" alt="logo" width="200" >
            <h1 class="text-center">zsebtanár</h1>      
            <p class="text-center">matek | másként</p>
        </div>';
    printTOC();
    return;
}

/**
 * Print table of contents
 *
 * @return NULL
 */
function printTOC()
{
    $classes = getClasses();
    while ($class = $classes->fetch_assoc()) {
        echo '<div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-2" id="leftCol">
                    <div class="text-center jumbotron2">
                        <h1>'.$class["alt"].'</h1>
                    </div>
                </div>
                <div class="col-md-4">';
        $topics = getTopics($class["alt"]);
        while ($topic = $topics->fetch_assoc()) {
            echo '<h2>'.$topic["name"].'</h2><ul class="list-group">';
            $subtopics = getSubTopics($topic["alt"]);
            while ($subtopic = $subtopics->fetch_assoc()) {
                echo '<li class="list-group-item2">
                        <a href="index.php?p='.$subtopic["id"].'">'
                            .$subtopic["name"]
                        .'</a>
                    </li>';
            }
            echo '</ul>';
        }
        echo '  </div>
                <div class="col-md-3"></div>
            </div>';
    }
    return;
}

/**
 * Print exercises
 *
 * @param int    $id   Subtopic id.
 *
 * @return NULL
 */
function printExercises($id)
{
    $nehezseg = $_REQUEST["f"];
    $result5 = getExercises($id);
    if ($result5->num_rows > 0) {
        echo '<h1>Feladatok</h1>';

        printExerciseButtonsTop($id);

        while ($row5 = $result5->fetch_assoc()) {
            $_SESSION['utvonal'] = preg_replace('/^(.*)\/[^\/]*\.php$/', '\1', $row5["fajl"]);
            include $row5["fajl"];
            $order = $row5["sorrend"];
            echo '<div action="" class="panel panel-default" id="'.toAscii($row5["label"]).'">';

            printExerciseHeader($order, $question);
            printExerciseBody($order, $opciok, $helyes, $megoldas);
            printExerciseHint($order, $row5, $nehezseg);

            echo '</div>';
        }

        printExerciseButtonsBottom($id);
    }
    return;
}

/**
 * Print exercise buttons on bottom
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printExerciseButtonsBottom($id)
{
    echo '<div class="text-center">
            <a href="index.php?p='.$id.'" type="button" '
            .'class="btn btn-success"><<< Vissza az elmélethez</a>
            <a href="hiba.php?p='.$id.'" type="button" '
            .'class="btn btn-danger">HIBÁT TALÁLTAM!</a>
            <a href="feladat.php?p='.$id.'" type="button" '
            .'class="btn btn-primary">Új feladat beküldése >>></a>
        </div>';
    return;
}

/**
 * Print exercise buttons on top
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printExerciseButtonsTop($id)
{
    echo '<br />
        <div class="text-center">
            <div class="btn-group">
                <a href="index.php?p='.$id.'&f=1" '
                    .'type="button" class="btn btn-primary">Könnyű</a>
                <a href="index.php?p='.$id.'&f=2" '
                    .'type="button" class="btn btn-primary">Közepes</a>
                <a href="index.php?p='.$id.'&f=3" '
                    .'type="button" class="btn btn-primary">Nehéz</a>
            </div>
        </div>
        <br />';
    return;
}

/**
 * Print exercise header
 *
 * @param int    $id       Subtopic id.
 * @param string $question Question.
 *
 * @return NULL
 */
function printExerciseHeader($id, $question)
{
    echo '<div class="panel-heading">
            <div class="row">
                <div id="feladatszam" class="col-md-1">'.$id.'</div>
                <div id="feladatszoveg" class="col-md-10">
                    <h4 class="panel-title">'.$question.'</h4>
                </div>
                <div class="col-md-1">
                    <span id="utmutato-gomb'.$id.'" class="glyphicon '
                    .'glyphicon-question-sign" data-toggle="modal" '
                    .'data-target="#utmutato-szoveg'.$id.'"></span>
                </div>
            </div>
        </div>';
    return;
}

/**
 * Print exercise body
 *
 * @param int    $id       Subtopic id.
 * @param array  $options  Options.
 * @param int    $correct  Correct option.
 * @param string $solution Solution.
 *
 * @return NULL
 */
function printExerciseBody($id, $options, $correct, $solution)
{
    echo '<div class="panel-body">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <form name="feladat'.$id.'">';
    if (is_array($options)) {
        if (count($options) > 3) {
            echo '<select name="valasz" class="form-control" id="sel'.$id.'\"'.'>';
            foreach ($options as $key => $value) {
                echo '<option value="'.$key.'">'.$value.'</option>';
            }
            echo '</select>';
        } else {
            foreach ($options as $key => $value) {
                echo '<div class="radio"><label>'
                .'<input type="radio" name="valasz" value="'.$key.'">'.$value
                .'</label></div>';
            }
        }
    } else {
        echo '<input type="text" class="form-control" name="valasz">';
    }
    echo '          </form>
                    <div class="text-center">
                        <p class="hibauzenet" id="error'.$id.'"></p>
                    </div>
                    <div id="button'.$id.'" class="text-center">
                        <button class="btn btn-default" onclick="ellenoriz(\''
                        .$id.'\',\''
                        .$correct.'\',\''
                        .$solution.'\')">Mehet</button>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>';
    return;
}

/**
 * Print exercise hint
 *
 * @param int   $id    Subtopic id.
 * @param array $data  Exercise data.
 * @param int   $level Difficulty level.
 *
 * @return NULL
 */
function printExerciseHint($id, $data, $level)
{
    echo '<div id="utmutato-szoveg'.$id.'" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" '
                    .'data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Feladat</h4>
                </div>';

    if ($level == 1 && $data["utmutato1"] != '') {
        $guide = $data["utmutato1"];
    } elseif ($level == 2 && $data["utmutato2"] != '') {
        $guide = $data["utmutato2"];
    } elseif ($level == 3 && $data["utmutato3"] != '') {
        $guide = $data["utmutato3"];
    } else {
        $guide = $data["utmutato"];
    }
    
    echo '      <div class="modal-body">'.$guide.'</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" '
                    .'data-dismiss="modal">Bezár</button>
                </div>
            </div>
        </div>
    </div>';
    return;
}