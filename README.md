# README #

Content of website (www.zsebtanar.hu) aiming to provide efficient resources for Hungarian kids struggling with Maths for free.

### Features ###

* Defitinitions
* Exercises
* Quizzes

### Installation ###

* Copy content to "public\_html" folder.
* Create file "database.txt" outside "public\_html". See "database_example.txt" for sample.
* Run "beolvas.php" to create database.
* Run "index.php" to run website

### Contact ###

* Viktor Szabó (zsebtanar@gmail.com)

### Licence ###

* http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
