/*!
 * Misc javascript functions
 * Licensed under CC BY-NC-SA 4.0 license.
 *
 * @author Viktor Szabó
 * @link http://www.zsebtanar.hu
 */

// picture swap
function kepCsere(kep) {
  kep.replace('smooth_','');
  var image = document.getElementById(kep);
  if (image) {
    if (image.src.match(".jpg")) {
        image.src = image.src.replace(".jpg",".gif");
    } else {
        image.src = image.src.replace(".gif",".jpg");
    }
  }
}

// picture start motion
function picStartMotion(kep) {
  kep.replace('smooth_','');
  for (var i = 5; i >= 0; i--) {
    var image = document.getElementById(kep + i);
    if (image) {
      if (image.src.match(".jpg")) {
          image.src = image.src.replace(".jpg",".gif");
      }
    }
  }
}

// picture stop motion
function picStopMotion(kep) {
  kep.replace('smooth_','');
  for (var i = 5; i >= 0; i--) {
    var image = document.getElementById(kep + i);
    if (image) {
      if (image.src.match(".gif")) {
          image.src = image.src.replace(".gif",".jpg");
      }
    }
  }
}

// quiz functions
function ujratolt() {
    location.reload();
}
