/*!
 * Misc javascript functions
 * Licensed under CC BY-NC-SA 4.0 license.
 *
 * @author Viktor Szabó
 * @link http://www.zsebtanar.hu
 */

/* activate sidebar */
$('#sidebar').affix({
	offset: {
		top:445
	}
});

/* activate popover */
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});

/* set width of submenu */
$(window).scroll(function () {
    $('#sidebar.affix').width($('#leftCol').width());
});

$(window).load(function () {
    $('#sidebar.affix').width($('#leftCol').width());
});

$(window).resize(function () {
    $('#sidebar.affix').width($('#leftCol').width());
});

/* activate scrollspy menu */
var $body   = $(document.body);
var navHeight = $('.navbar').outerHeight(true) + 10;

$body.scrollspy({
	target: '#leftCol',
	offset: navHeight
});

/* smooth scrolling sections */
$('a[href*=#smooth_]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
});

// activate data-toggle
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

$('a#popover').on('click', function(e) {e.preventDefault(); return true;});

// print messages
// activate data-toggle
$(document).ready(function(){
    var kep = document.querySelector('[id^="kep-"]');
    if (kep != null){
        document.getElementById("header_message").innerHTML = '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span class="glyphicon glyphicon-info-sign"></span> A mozgó képeket kattintással tudod megállítani és újraindítani.</div>';
    }
});





// multilevel navbar
(function($){
    $(document).ready(function(){
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
            event.preventDefault(); 
            event.stopPropagation(); 
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);








// quiz check answer
function ellenoriz(id,helyes,megoldas,tipus) {
    
    if (tipus == 'fraction') {
        var num = document.forms["feladat"+id]["numerator"].value;
        var denom = document.forms["feladat"+id]["denominator"].value;

        if ((num == null || num == "") && (denom == null || denom == "")) {
            document.getElementById("error"+id).innerHTML = "Hiányzik a válasz!";
            return;
        } else if (num == null || num == "") {
            document.getElementById("error"+id).innerHTML = "Hiányzik a számláló!";
            return;
        } else if (denom == null || denom == "") {
            document.getElementById("error"+id).innerHTML = "Hiányzik a nevező!";
            return;
        } else if (denom == 0) {
            document.getElementById("error"+id).innerHTML = "A nevező nem lehet 0!";
            return;
        }

        var x = num/denom;
        x = Math.round(x*1000000)/1000000;
        helyes = Math.round(helyes*1000000)/1000000;
    } else {
        
        var x = document.forms["feladat"+id]["answer"].value;

    }

    if (x == null || x == "") {
        document.getElementById("error"+id).innerHTML = "Hiányzik a válasz!" + x;
        return;
    }         

    if (tipus == 'fraction') {
        document.forms["feladat"+id]["numerator"].disabled = true;
        document.forms["feladat"+id]["denominator"].disabled = true;

    } else {
        var radios = document.forms["feladat"+id]["answer"];

        for (var i=0, iLen=radios.length; i<iLen; i++) {
            radios[i].disabled = true;
        }
        radios.disabled = true;
    }

    if (typeof x == 'string') {
        x = x.replace(/\s/g,''); /* szóközök */
        if (x.charAt(x.length - 1) == '.') {
            x = x.substr(0, x.length-1);
        }
        
        x = x.replace('egyszáz','száz'); /* számok szöveggel */
        x = x.replace('egyezer','ezer');
        x = x.replace('kettőezer','kétezer');
        x = x.replace('kettőmillió','kétmillió');
        x = x.replace('kettőmilliárd','kétmilliárd');
        x = x.toUpperCase();

    }

    if (typeof helyes == 'string') {
        helyes = helyes.toUpperCase();
    }

    if (x === helyes) {
        document.getElementById("error"+id).innerHTML = "";
        document.getElementById("button"+id).innerHTML = "<button class=\"btn btn-success\"><span class=\"glyphicon glyphicon-ok\"></span></button>&nbsp;&nbsp;<button class=\"btn btn-primary\" onclick=\"ujratolt()\"><span class=\"glyphicon glyphicon-refresh\"></span></button>";
    } else {
        document.getElementById("error"+id).innerHTML = "A helyes megoldás: " + megoldas;
        MathJax.Hub.Queue(["Typeset",MathJax.Hub,"error"+id]);
        document.getElementById("button"+id).innerHTML = "<button class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-remove\"></span></button>&nbsp;&nbsp;<button class=\"btn btn-primary\" onclick=\"ujratolt()\"><span class=\"glyphicon glyphicon-refresh\"></span></button>";
    } 
}






// youtube player
// REQUIRED: Include "jQuery Query Parser" plugin here or before this point: 
//       https://github.com/mattsnider/jquery-plugin-query-parser

$(document).ready(function(){
    
// BOOTSTRAP 3.0 - Open YouTube Video Dynamicaly in Modal Window
// Modal Window for dynamically opening videos
$('a[href^="http://www.youtube.com"]').on('click', function(e){
  // Store the query string variables and values
    // Uses "jQuery Query Parser" plugin, to allow for various URL formats (could have extra parameters)
    var queryString = $(this).attr('href').slice( $(this).attr('href').indexOf('?') + 1);
    var queryVars = $.parseQuery( queryString );

    // if GET variable "v" exists. This is the Youtube Video ID
    if ( 'v' in queryVars )
    {
        // Prevent opening of external page
        e.preventDefault();

        // Calculate default iFrame embed size based on current window size
        // (these will only be used if data attributes are not specified)
        if ($(window).height() < $(window).width()) {
            var vidHeight = $(window).height() * 0.7;
            var vidWidth = vidHeight * 1.77777;
        } else {
            var vidWidth = $(window).width() * 0.9;
            var vidHeight = vidWidth / 1.77777;
        }
        
        if ( $(this).attr('data-width') ) { vidWidth = parseInt($(this).attr('data-width')); }
        if ( $(this).attr('data-height') ) { vidHeight =  parseInt($(this).attr('data-height')); }
        var iFrameCode = '<iframe width="' + vidWidth + '" height="'+ vidHeight +'" scrolling="no" allowtransparency="true" allowfullscreen="true" src="http://www.youtube.com/embed/'+  queryVars['v'] +'?rel=0&wmode=transparent&showinfo=0" frameborder="0"></iframe>';

        // Replace Modal HTML with iFrame Embed
        $('#mediaModal .modal-body').html(iFrameCode);
        // Set new width of modal window, based on dynamic video content
        $('#mediaModal').on('show.bs.modal', function () {
            // Add video width to left and right padding, to get new width of modal window
            var modalBody = $(this).find('.modal-body');
            var modalDialog = $(this).find('.modal-dialog');
            var newModalWidth = vidWidth + parseInt(modalBody.css("padding-left")) + parseInt(modalBody.css("padding-right"));
            newModalWidth += parseInt(modalDialog.css("padding-left")) + parseInt(modalDialog.css("padding-right"));
            newModalWidth += 'px';
            // Set width of modal (Bootstrap 3.0)
            $(this).find('.modal-dialog').css('width', newModalWidth);
        });

        // Open Modal
        $('#mediaModal').modal();
    }
});

// Clear modal contents on close. 
// There was mention of videos that kept playing in the background.
$('#mediaModal').on('hidden.bs.modal', function () {
    $('#mediaModal .modal-body').html('');
});

}); 