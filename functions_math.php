<?php
/**
 * Zsebtanár mathematical functions
 *
 * PHP Version 5
 *
 * @category PHP
 * @package  Zsebtanár
 * @author   Viktor Szabó <zsebtanar@gmail.com>
 * @license  http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
 * @link     http://zsebtanar.hu/
 */


if (isset($_REQUEST['generate'])) {
  if ($_REQUEST['generate']) {
    $function = $_REQUEST['generate'];
    $function();
  }
}
















/*******************************    MISC functions    ******************************/

/**
 * Random number generator
 *
 * Generates number of $len digits in $numSys numeral system (e.g. value is 10 for
 * decimal system).
 *
 * @param int $len    No. of digits.
 * @param int $numSys Numeral system.
 *
 * @return int $num Random number.
 */
function randGenerate($len, $numSys)
{
  // first digit non-0
  $num = rand(1, $numSys-1);
  for ($i=0; $i<$len-1; $i++) {
      $digit = rand(0, $numSys-1);
      // for small numbers, last two digit differs
      while ($len < 4 && $i == 0 && $digit == $num) {
          $digit = rand(0, $numSys-1);
      }
      $num .= $digit;
  }
  return $num;
}

/**
 * Associative array shuffle
 *
 * Shuffle for associative arrays, preserves key=>value pairs.
 * (Based on (Vladimir Kornea of typetango.com)'s function) 
 *
 * @param array &$array Array.
 *
 * @return NULL
 */
function shuffleAssoc(&$array)
{
  $keys = array_keys($array);
  shuffle($keys);
  foreach ($keys as $key) {
      $new[$key] = $array[$key];
  }
  $array = $new;
  return;
}

/**
 * Modify same digits
 *
 * Modifies all digits in $num number that is equal to the one on $pos position.
 *
 * @param int $num Number.
 * @param int $pos Position.
 *
 * @return int $new New number.
 */
function modifySameDigits($num, $pos)
{
  $digits = str_split($num);
  $digit = $digits[$pos];
  foreach ($digits as $key => $value) {
      while ($value == $digit && $key != $pos) {
          if ($key == 0) {
              $value = rand(1, 9);   
          } else {
              $value = rand(0, 9);
          }
          $digits[$key] = $value;
      }
  }
  $new = implode("", $digits); 
  return $new;
}

/**
 * Convert to Roman number
 *
 * @param int $num Number.
 *
 * @return string $rom Roman number.
 */
function convertRoman($num)
{
  $values = array(
      1000000 => "M",
       900000 => "CM",
       500000 => "D",
       400000 => "CD",
       100000 => "C",
        90000 => "XC",
        50000 => "L",
        40000 => "XL",
        10000 => "X",
         9000 => "IX",
         5000 => "V",
         4000 => "IV",
         1000 => "M",
          900 => "CM",
          500 => "D",
          400 => "CD",
          100 => "C",
           90 => "XC",
           50 => "L",
           40 => "XL",
           10 => "X",
            9 => "IX",
            5 => "V",
            4 => "IV",
            1 => "I",
    );

  $rom = "";
  while ($num > 0) {
      foreach ($values as $key => $value) {
          if ($num >= $key) {
              if ($key > 1000) {
                  $rom = $rom.'\overline{'.$value.'}';
              } else {
                  $rom = $rom.$value;
              }
              $num -= $key;
              break;
          }
      }
  }
  return $rom;
}

/**
 * Replace digit in number
 *
 * @param int $num   Number.
 * @param int $pos   Position of digit to replace.
 * @param int $digit New digit.
 *
 * @return int $num Modified number.
 */
function replaceDigit($num,$pos,$digit)
{
  $digits = str_split($num);
  $hossz = strlen($num);
  foreach ($digits as $key => $value) {
      if ($hossz-$pos == $key) {
          $digits[$key] = $digit;
      }
  }
  $num = implode("", $digits);
  if ($hossz > 4) {
      $num = preg_replace( '/(.)(.{9})$/', '\1\,\2', $num);
      $num = preg_replace( '/(.)(.{6})$/', '\1\,\2', $num);
      $num = preg_replace( '/(.)(.{3})$/', '\1\,\2', $num);
  }
  return $num;
}

/**
 * Replace digit in number v2
 *
 * @param array $digits Number digits.
 * @param int $pos Position of digit to replace.
 * @param bool $chose Is the digit chosen?
 * @param int $correct Correct answer.
 * @param string $solution Correct answer.
 * @param text $szam Number to print.
 *
 * @return int $correct Correct answer.
 * @return string $solution Correct answer.
 * @return text $szam Number to print.
 */
function replaceDigit2($digits,$pos,$chosen,$correct,$solution,$szam)
{
  $hossz = count($digits);
  if ($hossz >= $pos) {
      $jegy = $digits[$hossz - $pos];
      if ($chosen) {
          $correct = $correct + $jegy;
          $solution = $jegy.'$$+$$'.$solution;
          $szam = '\textcolor{red}{?}'.$szam;
      } else {
          $szam = $jegy.$szam;
      }
      if ($pos % 3 == 0) {
      $szam = '\,'.$szam;
      }
  }
  return array($correct,$solution,$szam);
}

/**
 * Check if number has digit
 *
 * @param array $digits Digits of number
 * @param int   $digit  Digit to check
 *
 * @return bool TRUE if digit occurs.
 */
function hasDigit($digits,$digit)
{
  $ugyanaze = 0;
  foreach ($digits as $value) {
    if ($value == $digit) {
      $ugyanaze = 1;
    }
  }
  if ($ugyanaze == 0) {
    return TRUE;
  } else {
    return FALSE;
  }
}
  
/**
 * Generate new random number based on given number
 *
 * @param int $num Number
 * @param int $len Length of number
 *
 * @return int $new New number
 */
function newNum($num,$len)
{
  if (rand(1,3) == 1) {
    $new = randGenerate(rand($len-1,$len+1),10);
  } else {
    if (rand(1,2) == 1) {
      $ujhossz = floor($len/2); 
    } else {
      $ujhossz = $len - 1;
    }
    
    $new = $num + randGenerate(rand(1,$ujhossz),10);
  }
  return $new;
}

/**
 * Add suffix 'by' to number (nál/nél)
 *
 * @param int    $num  Number (< 10^600)
 *
 * @return string $suffix Suffix
 */
function addSuffixBy($num)
{
  $abs = abs($num);

  switch ($abs % 10) {
    case 1:
    case 2:
    case 4:
    case 5:
    case 7:
    case 9:
      return 'nél';
    case 3:
    case 6:
    case 8:
      return 'nál';
  }

  switch (($abs / 10) % 10) {
    case 1:
    case 4:
    case 5:
    case 7:
    case 9:
      return 'nél';
    case 2:
    case 3:
    case 6:
    case 8:
      return 'nál';
  }

  if ($abs == 0) {
    return 'nál';
  }
  elseif (1000 <= $abs && $abs < 1000000) {
    return 'nél';
  }
  else {
    return 'nál';
  }
}

/**
 * Add suffix 'times' to number (szor/szer/ször)
 *
 * @param int $num Number (< 10^600)
 *
 * @return string $suffix Suffix
 */
function addSuffixTimes($num)
{
  $abs = abs($num);

  switch ($abs % 10) {
    case 1:
    case 2:
    case 4:
    case 7:
    case 9:
      return 'szer';
    case 3:
    case 6:
    case 8:
      return 'szor';
    case 5:
      return 'ször';
  }

  switch (($abs / 10) % 10) {
    case 1:
    case 4:
    case 5:
    case 7:
    case 9:
      return 'szer';
    case 2:
    case 3:
    case 6:
    case 8:
      return 'szor';
  }

  if ($abs == 0) {
    return 'szor';
  } elseif (100 <= $abs && $abs < 1000) {
    return 'szor';
  } elseif (1000 <= $abs && $abs < 1000000) {
    return 'szer';
  } else {
    return 'szor';
  }
}

/**
 * Add suffix 'with' to number (val/vel)
 *
 * @param int $num  Number (< 10^9)
 *
 * @return string $suffix Suffix
 */
function addSuffixWith($num)
{
  $abs = abs($num);

  switch ($abs % 10) {
    case 1:
    case 4:
      return 'gyel';
    case 2:
      return 'vel';
    case 3:
      return 'mal';
    case 5:
    case 7:
      return 'tel';
    case 6:
      return 'tal';
    case 8:
      return 'cal';
    case 9:
      return 'cel';
  }

  switch (($abs / 10) % 10) {
    case 1:
      return 'zel';
    case 2:
      return 'szal';
    case 3:
      return 'cal';
    case 4:
    case 5:
    case 7:
    case 9:
      return 'nel';
    case 6:
    case 8:
      return 'nal';
  }

  if ($abs == 0) {
    return 'val';
  } elseif (100 <= $abs && $abs < 1000) {
    return 'zal';
  } elseif (1000 <= $abs && $abs < 1000000) {
    return 'rel';
  } else {
    return 'val';
  }
}

/**
 * Add article to number
 *
 * @param int $num Number
 *
 * @return string $article Article
 */
function addArticle($num)
{
  if ($num < 0) {
    $article = 'a';
    return $article;
  }

  $digits = str_split($num);
  $digit = $digits[0];
  $len = count($digits);

  if ($len % 3 == 1) {
    if ($digit == '1' || $digit == '5') {
      $article = 'az';
    } else {
      $article = 'a';
    }
  } elseif ($digit == '5') {
      $article = 'az';
  } else {
    $article = 'a';
  }

  return $article;
}

/**
 * Get member of series defined by recursive formula
 *
 * Formula: $coeff_an_1*a_{n-1} + $coeff_an_2*a_{n-2} + $coeff_n*n^{pow_n} + $coeff_a0
 *
 * @param int $a1         Initial value of a1
 * @param int $a2         Initial value of a2
 * @param int $coeff_an_1 Coefficient of a_{n-1} in recursive formula
 * @param int $coeff_an_2 Coefficient of a_{n-2} in recursive formula
 * @param int $coeff_n    Coefficient of n in recursive formula
 * @param int $pow_n      Power of n in recursive formula
 * @param int $coeff_a0   Constant term in recursive formula
 * @param int $index      Index of member we are looking for
 *
 * @return int $res Result
 */
function recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index)
{
  if ($index == 1) {
    $res = $a1;
  } elseif ($index == 2 && $coeff_an_2 != 0) {
    $res = $a2;
  } else {

    $an_1 = recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index-1);
    if ($coeff_an_2 != 0) {
      $an_2 = recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index-2);
    } else {
      $an_2 = 0;

    }
    $res = $coeff_an_1*$an_1 + $coeff_an_2*$an_2 + $coeff_n*pow($index,$pow_n) + $coeff_a0;

  }
  return $res;
}

/**
 * Get greatest common divisor of two numbers
 *
 * source: http://networking.mydesigntool.com/viewtopic.php?tid=289&id=31
 *
 * @param int $x Number 1.
 * @param int $y Number 2.
 *
 * @return int $z Greatest common divisor
 */
function gcd($x, $y)
{
  $z = 1;
  $x = abs($x);
  $y = abs($y);

  if($x + $y == 0) {
    
    return "0";

  } else {

    while($x > 0) {
      $z = $x;
      $x = $y % $x;
      $y = $z;
    }

    return $z;
  }
}

/**
 * Combinations with repetitions
 *
 * @param array $array Array of elements.
 * @param int   $k     Size of selection.
 *
 * @return array $combos Combinations.
 */
function combos($arr, $k) {
  if ($k == 0) {
    return array(array());
  }

  if (count($arr) == 0) {
    return array();
  }

  $head = $arr[0];

  $combos = array();
  $subcombos = combos($arr, $k-1);
  foreach ($subcombos as $subcombo) {
    array_unshift($subcombo, $head);
    $combos[] = $subcombo;
  }
  array_shift($arr);
  $combos = array_merge($combos, combos($arr, $k));
  return $combos;
}







/*********************************    5. OSZTALY    ********************************/



/********** TERMÉSZETES SZÁMOK **********/




/*** Természetes számok ***/




/*** Tízes számrendszer ***/
function E_5_termeszetes_szamok_tizes_szamrendszer_alaki_ertek()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,10);
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  $jo = $szamjegyek[$hossz-$helyiertek];
  
  $helyiertekek = array("az egyesek","a tízesek","a százasok","az ezresek","a tízezresek","a százezresek","a milliósok","a tízmilliósok","a százmilliósok","a milliárdosok");
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\,');
  }
  
  if (rand(1,2) == 1) {
    $question = 'Melyik számjegy áll '.$helyiertekek[$helyiertek-1].' helyén az alábbi számban?$$'.$szam.'$$';
  } else {
    $question = 'Mi '.$helyiertekek[$helyiertek-1].' helyén álló szám alaki értéke?$$'.$szam.'$$';
  }
  
  if ($level < 3) {
    $options = array_unique($szamjegyek);
    $correct = array_search($jo, $options);
    $options = preg_replace( '/\d+/', '\$$0\$', $options);
    shuffleAssoc($options);
    $solution = '$'.$jo.'$';
  } else {
    $options = '';
    $correct = $jo;
    $solution = '$'.$jo.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_helyiertek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,10);
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  $szamjegy = $szamjegyek[$hossz-$helyiertek];
  
  $szam = modifySameDigits($szam,$hossz-$helyiertek);
  
  if (in_array($szamjegy,array(5,1))) {$nevelo = 'az';} else {$nevelo = 'a';}
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\,');
  }
  
  if (rand(1,2) == 1) {
    
    $question = 'Melyik helyen áll '.$nevelo.' $'.$szamjegy.'$ az alábbi számban?$$'.$szam.'$$';
    $helyiertekek = array("egyesek","tízesek","százasok","ezresek","tízezresek","százezresek","milliósok","tízmilliósok","százmilliósok","milliárdosok");
  
    $options = array_slice($helyiertekek,0,$hossz);
    $options = preg_replace( '/.$/', '$0 helyén.', $options);
    $options = preg_replace( '/^[^e]/', 'A $0', $options);
    $options = preg_replace( '/^e/', 'Az $0', $options);
  
    if ($level == 3) {
      shuffleAssoc($options);
    }
  
    $correct = $helyiertek-1;
    $solution = $options[$helyiertek-1];
  
  } else {
  
    $question = 'Mi '.$nevelo.' $'.$szamjegy.'$ helyiértéke az alábbi számban?$$'.$szam.'$$';
    $helyiertekek = array(1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000);
  
    $options = array_slice($helyiertekek,0,$hossz);
    $options = preg_replace( '/000000000$/', '\\,000000000', $options);
    $options = preg_replace( '/000000$/', '\\,000000', $options);
    $options = preg_replace( '/000$/', '\\,000', $options);
    $options = preg_replace( '/^1/', '\$1', $options);
    $options = preg_replace( '/0$/', '0\$', $options);
    $options = preg_replace( '/1$/', '1\$', $options);
  
    $correct = $helyiertek-1;
    $solution = $options[$helyiertek-1];
    $solution = str_ireplace('\\,','\\\\,',$solution);
  
    shuffleAssoc($options);
  
    if ($level == 3) {
      $correct = $helyiertekek[$helyiertek-1];
      $solution = $options[$helyiertek-1];
      $solution = str_ireplace('\\,','\\\\,',$solution);
      $options = '';
    }
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_valodi_ertek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,10);
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  $szamjegy = $szamjegyek[$hossz-$helyiertek];
  
  $szam = modifySameDigits($szam,$hossz-$helyiertek);
  
  if (in_array($szamjegy,array(5,1))) {$nevelo = 'az';} else {$nevelo = 'a';}
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\\,');
  }
  
  if (rand(1,2) == 1) {
    $question = 'Mennyit ér '.$nevelo.' $'.$szamjegy.'$ az alábbi számban?$$'.$szam.'$$';
  } else {
    $question = 'Mi '.$nevelo.' $'.$szamjegy.'$ valódi értéke az alábbi számban?$$'.$szam.'$$';
  }
  
  $helyiertekek = array(1,10,100,1000,10000,100000,1000000,10000000,100000000,1000000000);
  $correct = $helyiertek-1;
  
  foreach ($helyiertekek as $key => $value) {
    $valodiertekek[$key] = $value*$szamjegy; 
  }
  
  $options = array_slice($valodiertekek,0,$hossz);
  $options = preg_replace( '/000000000$/', '\\,000000000', $options);
  $options = preg_replace( '/000000$/', '\\,000000', $options);
  $options = preg_replace( '/000$/', '\\,000', $options);
  $options = preg_replace( '/^(.*)$/', '\$$0\$', $options);
  
  $solution = $options[$helyiertek-1];
  $solution = str_ireplace('\\,','\\\\,',$solution);
  
  shuffleAssoc($options);
  
  if ($level == 3 || $valodiertekek[$helyiertek-1] == 0) {
    $options = '';
    $correct = $valodiertekek[$helyiertek-1];
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_szamolas_betukkel()
{
  $level = $_SESSION['level'];


  $helyiertekek = array("egyes","tízes","százas","ezres","tízezres","százezres","milliós","tízmilliós","százmilliós");
  
  if ($level == 1) {
    $maxdb = rand(1,2);
    $maxhelyiertek = 4;
    $maxertek = 9;
  } elseif ($level == 2) {
    $maxdb = rand(2,3);
    $maxhelyiertek = 6;
    $maxertek = 15;
  } elseif ($level == 3) {
    $maxdb = rand(3,5);
    $maxhelyiertek = count($helyiertekek);
    $maxertek = 25;
  }
  
  $helyiertekek = array_slice($helyiertekek,0,$maxhelyiertek);
  shuffleAssoc($helyiertekek);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($helyiertekek as $key => $value) {
    if ($db < $maxdb) {
      $alakiertek = rand(1,$maxertek);
      $osszeg = $osszeg + $alakiertek*pow(10,$key);
      $szoveg = $szoveg.' $'.$alakiertek.'$ '.$value;
    }
    $db++;
  }
  
  $szoveg = preg_replace('/(\w)\s\$/', '\1, $', $szoveg);
  $szoveg = preg_replace('/,([^,]*)$/', ' és\1', $szoveg);
  
  $options = '';
  $question = 'Mennyit ér '.$szoveg.'?';
  $correct = $osszeg;
  $solution = '$'.$osszeg.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_szamolas_penzzel()
{
  $level = $_SESSION['level'];


  $penzek = array("tíz","húsz","ötven","száz","kétszáz","ötszáz","ezer","kétezer","ötezer","tízezer","húszezer");
  $ertekek = array(10,   20,    50,     100,   200,      500,     1000,  2000,     5000,    10000,    20000);
  $penzek = preg_replace('/^(.*)$/', '\1forintos', $penzek);
  
  if ($level == 1) {
    $maxfajta = rand(1,2);
    $maxhelyiertek = 4;
    $maxdb = array(9,4,1);
  } elseif ($level == 2) {
    $maxfajta = rand(2,4);
    $maxhelyiertek = 7;
    $maxdb = array(15,6,2);
  } elseif ($level == 3) {
    $maxfajta = rand(4,6);
    $maxhelyiertek = count($penzek);
    $maxdb = array(25,8,3);
  }
  
  $penzek = array_slice($penzek,0,$maxhelyiertek);
  shuffleAssoc($penzek);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($penzek as $key => $value) {
    if ($db < $maxfajta) {
      $alakiertek = rand(1,$maxdb[$key % 3]);   
      $osszeg = $osszeg + $alakiertek*$ertekek[$key];
      $szoveg = $szoveg.' $'.$alakiertek.'$ '.$value;
    }
    $db++;
  }
  
  $szoveg = preg_replace('/(\w)\s\$/', '\1, $', $szoveg);
  $szoveg = preg_replace('/,([^,]*)$/', ' és\1', $szoveg);
  
  $options = '';
  $question = 'Mennyit ér '.$szoveg.'?';
  $correct = $osszeg;
  $solution = '$'.$osszeg.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_szamolas_szammal()
{
  $level = $_SESSION['level'];


  $kitevok = array(0,1,2,3,4,5,6,7,8);
  
  if ($level == 1) {
    $maxdb = rand(2,3);
    $maxkitevo = 4;
    $maxertek = 9;
  } elseif ($level == 2) {
    $maxdb = rand(3,4);
    $maxkitevo = 6;
    $maxertek = 15;
  } elseif ($level == 3) {
    $maxdb = rand(4,5);
    $maxkitevo = count($kitevok);
    $maxertek = 25;
  }
  
  $kitevok = array_slice($kitevok,0,$maxkitevo);
  shuffleAssoc($kitevok);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($kitevok as $key => $value) {
    if ($db < $maxdb) {
      $alakiertek = rand(1,$maxertek);
      $helyiertek = pow(10,$kitevok[$key]);
      $osszeg = $osszeg + $alakiertek*$helyiertek;
      $szoveg = $szoveg.$alakiertek.'\cdot'.$helyiertek.'$$+$$';
    }
    $db++;
  }

  $szoveg = preg_replace('/000000000\$/', '\\,000\\,000\\,000$', $szoveg);
  $szoveg = preg_replace('/000000\$/', '\\,000\\,000$', $szoveg);
  $szoveg = preg_replace('/0000\$/', '0\\,000$', $szoveg);
  $szoveg = preg_replace('/^(.)/', '$\1', $szoveg);
  $szoveg = preg_replace('/\$\+\$\$$/', '', $szoveg);
  
  $options = '';
  $question = 'Mennyivel egyenlő '.$szoveg.'?';
  $correct = $osszeg;
  
  $osszeg = str_ireplace(',','\\\\,',number_format($osszeg));
  $solution = '$'.$osszeg.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_nullak_szamol()
{
  $level = $_SESSION['level'];


  $kitevok = array(0,1,2,3,4,5,6,7,8);
  
  if ($level == 1) {
    $maxdb = rand(2,3);
    $maxkitevo = 4;
    $maxertek = 9;
  } elseif ($level == 2) {
    $maxdb = rand(3,4);
    $maxkitevo = 6;
    $maxertek = 15;
  } elseif ($level == 3) {
    $maxdb = rand(4,5);
    $maxkitevo = count($kitevok);
    $maxertek = 25;
  }
  
  $kitevok = array_slice($kitevok,0,$maxkitevo);
  shuffleAssoc($kitevok);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($kitevok as $key => $value) {
    if ($db < $maxdb) {
      $alakiertek = rand(1,$maxertek);
      $helyiertek = pow(10,$kitevok[$key]);
      $osszeg = $osszeg + $alakiertek*$helyiertek;
      $szoveg = $szoveg.$alakiertek.'\cdot'.$helyiertek.'$$+$$';
    }
    $db++;
  }
  
  $nulla_db = 0;
  foreach(str_split($osszeg) as $value) {
    if ($value == 0) {
      $nulla_db++;
    }
  }

  $szoveg = preg_replace('/000000000\$/', '\\,000\\,000\\,000$', $szoveg);
  $szoveg = preg_replace('/000000\$/', '\\,000\\,000$', $szoveg);
  $szoveg = preg_replace('/0000\$/', '0\\,000$', $szoveg);
  $szoveg = preg_replace('/^(.)/', '$\1', $szoveg);
  $szoveg = preg_replace('/\$\+\$\$$/', '', $szoveg);
  
  $options = '';
  $question = 'Hány nulla szerepel az alábbi műveletsor eredményében: '.$szoveg.'?';
  $correct = $nulla_db;
  
  if ($osszeg > 9999) {
    $osszeg = str_ireplace(',','\\\\,',number_format($osszeg));
  }
  
  $solution = '$'.$nulla_db.'$ (az eredmény: $'.$osszeg.'$)';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_szamvaltas()
{
  $level = $_SESSION['level'];


  $mitvalt = array("százas","ezres","tízezres","százezres","milliós","tízmilliós","százmilliós");
  $mirevalt = array("tízest","százast","ezrest");
  $index = rand(0,2);
  $mirevalt = $mirevalt[$index];
  
  if ($level == 1) {
    $max_db = rand(1,2);
    $max_helyiertek = 2;
    $max_ertek = 9;
  } elseif ($level == 2) {
    $max_db = rand(2,3);
    $max_helyiertek = 4;
    $max_ertek = 15;
  } elseif ($level == 3) {
    $max_db = rand(3,4);
    $max_helyiertek = 6;
    $max_ertek = 25;
  }
  
  $mitvalt = array_slice($mitvalt,$index,$max_helyiertek-$index+1,TRUE);
  shuffleAssoc($mitvalt);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($mitvalt as $key => $value) {
    if ($db < $max_db) {
      $k = rand(1,$max_ertek);
      $osszeg = $osszeg + $k*pow(10,$key+2);
      $szoveg = $szoveg.' $'.$k.'$ '.$value;
    }
    $db++;
  }
  
  $eredmeny = $osszeg/pow(10,$index+1);
  if ($eredmeny > 9999) {
    $eredmeny = str_ireplace(',','\\\\,',number_format($eredmeny));
  }

  $szoveg = preg_replace('/(\w)\s\$/', '\1, $', $szoveg);
  $szoveg = preg_replace('/,([^,]*)$/', ' és\1', $szoveg);
  
  $options = '';
  $question = 'Hány darab  '.$mirevalt.' jelent '.$szoveg.'?';
  $correct = $eredmeny;
  $solution = '$'.$eredmeny.'$ db '.$mirevalt.'.';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_penzvaltas()
{
  $level = $_SESSION['level'];


  $mirevalt = array("tízforintosra","százforintosra");
  $index = rand(0,1);
  $mirevalt = $mirevalt[$index];
  
  if ($level == 1) {
    $dbmax = rand(1,2);
    $mitvalt = array("százast","ezrest");
    $max = array(7,9);
  } elseif ($level == 2) {
    $dbmax = rand(2,3);
    $mitvalt = array("százast","ezrest","ötezrest");
    $max = array(15,13,1);
  } elseif ($level == 3) {
    $dbmax = rand(3,4);
    $mitvalt = array("százast","ezrest","ötezrest","tízezrest","húszezrest");
    $max = array(25,25,2,12,2);
  }
  
  $mitvalt = array_slice($mitvalt,$index);
  $max = array_slice($max,$index);
  shuffleAssoc($mitvalt);
  
  $osszeg = 0;
  $szoveg = '';
  
  $db = 0;
  foreach ($mitvalt as $key => $value) {
    if ($db < $dbmax) {
      $k = rand(1,$max[$key]);
      if ($value == "százast") {
        $osszeg = $osszeg + $k*100;
      } elseif ($value == "ezrest") {
        $osszeg = $osszeg + $k*1000;
      } elseif ($value == "ötezrest") {
        $osszeg = $osszeg + $k*5000;
      } elseif ($value == "tízezrest") {
        $osszeg = $osszeg + $k*10000;
      } elseif ($value == "húszezrest") {
        $osszeg = $osszeg + $k*20000;
      }
      $szoveg = $szoveg.' $'.$k.'$ '.$value;
    }
    $db++;
  }
  
  if ($mirevalt == "tízforintosra") {
    $eredmeny = $osszeg/10;
  } elseif ($mirevalt == "százforintosra") {
    $eredmeny = $osszeg/100;
  }
  
  $szoveg = preg_replace('/(\w)\s\$/', '\1, $', $szoveg);
  $szoveg = preg_replace('/,([^,]*)$/', ' és\1', $szoveg);
  
  $options = '';
  $question = 'Hány darab  '.$mirevalt.' lehet felváltani '.$szoveg.'?';
  $correct = $eredmeny;
  $solution = '$'.$eredmeny.'$ db '.$mirevalt.'.';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_betuk_szammal()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,10);
  
  $szamjegyek = str_split($szam);
  $szamjegyek = array_reverse($szamjegyek);
  
  $szam_hatar = array('','ezer','millió','milliárd');
  $szamok1 = array('','egy','kettő','három','négy','öt','hat','hét','nyolc','kilenc');
  $szamok1b = array('','','két','három','négy','öt','hat','hét','nyolc','kilenc');
  $szamok2 = array('','tizen','huszon','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
  $szamok2b = array('','tíz','húsz','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
  
  $szakasz = 0;
  $szam_betu = '';
  foreach ($szamjegyek as $key => $value) {
    if ($key % 3 == 0) {
      if ($szam > 2000 && $szakasz > 0) {
        $szam_betu = $szam_hatar[$szakasz].'-'.$szam_betu;
      } else {
        $szam_betu = $szam_hatar[$szakasz].$szam_betu;
      }
      $szam_betu = $szamok1[$value].$szam_betu;
      $szakasz++;
    } elseif ($key % 3 == 1) {
      if ($szamjegyek[$key-1] == 0) {
        $szam_betu = $szamok2b[$value].$szam_betu;
      } else {
        $szam_betu = $szamok2[$value].$szam_betu;
      }
    } elseif ($key % 3 == 2) {
      $szam_betu = $szamok1b[$value].'száz'.$szam_betu;
    }
  }
  
  $options = '';
  $szam_betu = str_ireplace('egyezer','ezer', $szam_betu);
  $szam_betu = str_ireplace('kettőezer','kétezer', $szam_betu);
  $szam_betu = str_ireplace('kettőmillió','kétmillió', $szam_betu);
  $szam_betu = str_ireplace('kettőmilliárd','kétmilliárd', $szam_betu);
  
  $question = 'Írjuk le számjegyekkel az alábbi számot: <i>"'.$szam_betu.'"</i> !';
  $correct = $szam;
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\,');
  }
  
  $solution = '$'.$szam.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_tizes_szamrendszer_szam_betukkel()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $szam = rand(1,2500);
  } elseif ($level == 2) {
    $szam = randGenerate(rand(4,7),10);
  } elseif ($level == 3) {
    $szam = randGenerate(rand(7,10),10);
  }
  
  $szamjegyek = str_split($szam);
  $szamjegyek = array_reverse($szamjegyek);
  
  $szam_hatar = array('','ezer','millió','milliárd');
  $szamok1 = array('','egy','kettő','három','négy','öt','hat','hét','nyolc','kilenc');
  $szamok1b = array('','','két','három','négy','öt','hat','hét','nyolc','kilenc');
  $szamok2 = array('','tizen','huszon','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
  $szamok2b = array('','tíz','húsz','harminc','negyven','ötven','hatvan','hetven','nyolcvan','kilencven');
  
  $szakasz = 0;
  $szam_betu = '';
  foreach ($szamjegyek as $key => $value) {
    if ($key % 3 == 0) {
      if ($szam > 2000 && $szakasz > 0) {
        $szam_betu = $szam_hatar[$szakasz].'-'.$szam_betu;
      } else {
        $szam_betu = $szam_hatar[$szakasz].$szam_betu;
      }
      $szam_betu = $szamok1[$value].$szam_betu;
      $szakasz++;
    } elseif ($key % 3 == 1) {
      if ($szamjegyek[$key-1] == 0) {
        $szam_betu = $szamok2b[$value].$szam_betu;
      } else {
        $szam_betu = $szamok2[$value].$szam_betu;
      }
    } elseif ($key % 3 == 2 && $value > 0) {
      $szam_betu = $szamok1b[$value].'száz'.$szam_betu;
    }
  }
  
  $options = '';
  $szam_betu = str_ireplace('egyezer','ezer', $szam_betu);
  $szam_betu = str_ireplace('kettőezer','kétezer', $szam_betu);
  $szam_betu = str_ireplace('kettőmillió','kétmillió', $szam_betu);
  $szam_betu = str_ireplace('kettőmilliárd','kétmilliárd', $szam_betu);
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\,');
  }
  
  $question = 'Írjuk le betűkkel az alábbi számot: $'.$szam.'$!';
  $correct = $szam_betu;
  
  $solution = '<i>'.$szam_betu.'<\i>';


  return array($question, $options, $correct, $solution);
}




/*** Kettes számrendszer ***/
function E_5_termeszetes_szamok_kettes_szamrendszer_alaki_ertek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $szam = randGenerate($hossz,2);
  
  while (count(array_unique(str_split($szam))) == 1) {
    $szam = randGenerate($hossz,2);
  }
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  $jo = $szamjegyek[$hossz-$helyiertek];
  
  $helyiertekek = array("az egyesek","a kettesek","a négyesek","a nyolcasok","a tizenhatosok","a harminckettesek","a hatvannégyesek","a százhuszonnyolcasok","a kétszázötvenhatosok","az ötszáztizenkettesek","az ezerhuszonnégyesek");
  
  if (strlen($szam) > 8) {
    $szam = preg_replace( '/(\d{4})(\d{4})$/', '\\\\,\1\\\\,\2', $szam);
  }
  elseif (strlen($szam) > 4) {
    $szam = preg_replace( '/(\d{4})$/', '\\\\,\1', $szam);
  }
  
  if (rand(1,2) == 1) {
    $question = 'Melyik számjegy áll '.$helyiertekek[$helyiertek-1].' helyén az alábbi számban?$$'.$szam.'_2$$';
  } else {
    $question = 'Mi '.$helyiertekek[$helyiertek-1].' helyén álló szám alaki értéke?$$'.$szam.'_2$$';
  }
  
  if ($level < 3) {
    $options = array_unique($szamjegyek);
    $correct = array_search($jo, $options);
    $options = preg_replace( '/\d+/', '\$$0\$', $options);
    shuffleAssoc($options);
    $solution = '$'.$jo.'$';
  } else {
    $options = '';
    $correct = $jo;
    $solution = '$'.$jo.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kettes_szamrendszer_helyiertek()
{

  $level = $_SESSION['level'];

  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $szam = randGenerate($hossz,2);
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  
  $szam2 = '';
  foreach ($szamjegyek as $key => $value) {
    if ($key == $hossz-$helyiertek) {
      $value = '\textcolor{red}{'.$value.'}';
    }
    if ($hossz-$key == 4 || $hossz-$key == 8) {
      $szam2 = $szam2.'\,';
    }
    $szam2 = $szam2.$value;
  }
  
  if (rand(1,2) == 1) {
    
    $question = 'Melyik helyen áll a piros számjegy az alábbi számban?$$'.$szam2.'_2$$';
    $helyiertekek = array("egyesek","kettesek","négyesek","nyolcasok","tizenhatosok","harminckettesek","hatvannégyesek","százhuszonnyolcasok","kétszázötvenhatosok","ötszáztizenkettesek","ezerhuszonnégyesek");
  
    $options = array_slice($helyiertekek,0,$hossz);
    $options = preg_replace( '/.$/', '$0 helyén.', $options);
    $options = preg_replace( '/^[^{eö}]/', 'A $0', $options);
    $options = preg_replace( '/^(e)/', 'Az $0', $options);
    $options = preg_replace( '/^(ö)/', 'Az $0', $options);
  
    if ($level == 3) {
      shuffleAssoc($options);
    }
  
    $correct = $helyiertek-1;
    $solution = $options[$helyiertek-1];
  
  } else {
  
    $question = 'Mi a piros számjegy helyiértéke az alábbi számban?$$'.$szam2.'_2$$';
    $helyiertekek = array(1,2,4,8,16,32,64,128,256,512,1024);
  
    $options = array_slice($helyiertekek,0,$hossz);
    $options = preg_replace( '/^(\d)/', '\$\1', $options);
    $options = preg_replace( '/(\d)$/', '\1\$', $options);
  
    $correct = $helyiertek-1;
    $solution = $options[$helyiertek-1];
    $solution = str_ireplace('\\,','\\\\,',$solution);
  
    shuffleAssoc($options);
  
    if ($level == 3) {
      $correct = $helyiertekek[$helyiertek-1];
      $solution = $options[$helyiertek-1];
      $solution = str_ireplace('\\,','\\\\,',$solution);
      $options = '';
    }
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kettes_szamrendszer_valodi_ertek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $szam = randGenerate($hossz,2);
  
  $szamjegyek = str_split($szam);
  $helyiertek = rand(round($hossz/2),$hossz);
  $szamjegy = $szamjegyek[$hossz-$helyiertek];
  
  $szam2 = '';
  foreach ($szamjegyek as $key => $value) {
    if ($key == $hossz-$helyiertek) {
      $value = '\textcolor{red}{'.$value.'}';
    }
    if ($hossz-$key == 4 || $hossz-$key == 8) {
      $szam2 = $szam2.'\,';
    }
    $szam2 = $szam2.$value;
  }
  
  if (rand(1,2) == 1) {
    $question = 'Mennyit ér a piros számjegy az alábbi számban?$$'.$szam2.'_2$$';
  } else {
    $question = 'Mi a piros számjegy valódi értéke az alábbi számban?$$'.$szam2.'_2$$';
  }
  
  $helyiertekek = array(0,1,2,4,8,16,32,64,128,256,512,1024);
  
  $options = array_slice($helyiertekek,0,$hossz+1);
  $options = preg_replace( '/^(\d)/', '\$\1', $options);
  $options = preg_replace( '/(\d)$/', '\1\$', $options);
  
  if ($szamjegy == 0) {
    $solution = '$0$';
    $correct = 0;
  } else {
    $solution = $options[$helyiertek];
    $correct = pow(2,$helyiertek-1);
  }
  
  shuffleAssoc($options);
  
  if ($level > 2 || $solution == 0) {
    $options = '';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kettes_szamrendszer_valtas_2_bol_10_be()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $szam = randGenerate($hossz,2);
  $helyiertekek = array(0,1,2,4,8,16,32,64,128,256,512,1024);
  $szamjegyek = str_split($szam);
  
  $correct = 0;
  
  foreach ($szamjegyek as $key => $value) {
    if ($value == 1) {
      $correct = $correct + pow(2,$hossz-$key-1);
    }
  }
  
  if (strlen($szam) > 8) {
    $szam = preg_replace( '/(\d{4})(\d{4})$/', '\\\\,\1\\\\,\2', $szam);
  }
  elseif (strlen($szam) > 4) {
    $szam = preg_replace( '/(\d{4})$/', '\\\\,\1', $szam);
  }
  
  $question = 'Írjuk fel tízes számrendszerben!$$'.$szam.'_2$$';
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kettes_szamrendszer_valtas_lampa()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];
  $utvonal = str_replace('/', '___', $utvonal);

  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $szam = randGenerate($hossz,2);
  $helyiertekek = array(0,1,2,4,8,16,32,64,128,256,512,1024);
  $szamjegyek = str_split($szam);
  
  $correct = 0;
  
  foreach ($szamjegyek as $key => $value) {
    if ($value == 1) {
      $correct = $correct + pow(2,$hossz-$key-1);
    }
  }

  $szelesseg = floor(100/11*strlen($szam));
  $question = 'A lámpák egy kettes számrendszerbeli számot jelölnek.
        A bekapcsolt lámpa $1$-et, a kikapcsolt $0$-t jelent.
        Írjuk fel a szám tízes számrendszerbeli alakját!
        <div class="text-center">
          <img class="img-question" width="'.$szelesseg.'%" src="functions_math.php?generate=P_5_termeszetes_szamok_kettes_szamrendszer_valtas_lampa&path='.$utvonal.'&num='.$szam.'">
        </div>';
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

/* Lamps turned on and turned off */
function P_5_termeszetes_szamok_kettes_szamrendszer_valtas_lampa()
{
  $utvonal = $_REQUEST['path'];
  $utvonal = str_replace('___', '/', $utvonal);
  $szam = $_REQUEST['num'];

  header('Content-Type: image/png');
  $szamjegyek = str_split($szam);
  $terkoz = 5;
  $szelesseg = (80+$terkoz)*strlen($szam)-$terkoz;
  $magassag = 126;

  $image = imagecreatetruecolor($szelesseg, $magassag) or die('Nem sikerült képet létrehozni');
  $hatter = imagecolorallocate($image, 0, 0, 0);
  imagecolortransparent($image, $hatter);
  imagealphablending($image, false);

  foreach ($szamjegyek as $key => $value) {
    if ($value == 1) {
      $fajl = $utvonal.'/fel.png';
    } else {
      $fajl = $utvonal.'/le.png';
    }
    $kep = imagecreatefrompng($fajl);
    imagecopy($image, $kep, $key*(80+$terkoz), 0, 0, 0, 80, 126);
  }

  imagesavealpha($image, false);
  imagepng($image);
  imagedestroy($image);

  return;
}

function E_5_termeszetes_szamok_kettes_szamrendszer_valtas_ujjak()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];
  $utvonal = str_replace('/', '___', $utvonal);

  if ($level == 1) {
    $hossz = rand(2,5); 
  } elseif ($level == 2) {
    $hossz = rand(4,7);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,2);
  $helyiertekek = array(0,1,2,4,8,16,32,64,128,256,512,1024);
  $szamjegyek = str_split($szam);
  
  $correct = 0;
  
  foreach ($szamjegyek as $key => $value) {
    if ($value == 1) {
      $correct = $correct + pow(2,$hossz-$key-1);
    }
  }
  
  if ($hossz > 5) {
    $width = 80;
  } else {
    $width = 40;
  }

  $question = 'A kezünkkel egy kettes számrendszerbeli számot mutatunk.
        Minden ujj egy helyiértéket jelöl, a helyiértékek jobbról balra növekednek.
        Írjuk fel a szám tízes számrendszerbeli alakját!
        <div class="text-center">
          <img class="img-question" width="'.$width.'%" src="functions_math.php?generate=P_5_termeszetes_szamok_kettes_szamrendszer_valtas_ujjak&path='.$utvonal.'&num='.$szam.'">
        </div>';
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

/* Fingers showing number turned on and turned off */
function P_5_termeszetes_szamok_kettes_szamrendszer_valtas_ujjak()
{
  $utvonal = $_REQUEST['path'];
  $utvonal = str_replace('___', '/', $utvonal);
  $szam = $_GET['num'];

  header('Content-Type: image/png');
  $szamjegyek = str_split($szam);

  if (strlen($szam) > 5) {
    $fajl1 = '';
    for ($i=5; $i < 10; $i++) { 
      if ($i < strlen($szam)) {
        $fajl1 = $fajl1.$szamjegyek[strlen($szam)-$i-1];
      } else {
        $fajl1 = $fajl1.'0';
      }
    }
  }

  $fajl2 = '';
  for ($i=0; $i < 5; $i++) { 
    if ($i < strlen($szam)) {
      $fajl2 = $szamjegyek[strlen($szam)-$i-1].$fajl2;
    } else {
      $fajl2 = '0'.$fajl2;
    }
  }

  $terkoz = 10;
  $magassag = 72;
  $szelesseg = 80;
  if (strlen($szam) > 5) {
    $vaszonszelesseg = $szelesseg*2+ $terkoz;
    $kezdopont = $szelesseg+$terkoz;
  } else {
    $vaszonszelesseg = 80;
    $kezdopont = 0;
  }

  $image = imagecreate($vaszonszelesseg, $magassag) or die('Nem sikerült képet létrehozni');
  $hatter = imagecolorallocate($image, 0, 0, 0);
  imagecolortransparent($image, $hatter);
  imagealphablending($image, false);

  if (strlen($szam) > 5) {
    $kep1 = imagecreatefrompng($utvonal.'/'.'png/'.$fajl1.'.png');
    imageflip($kep1, IMG_FLIP_HORIZONTAL);
    imagecopy($image, $kep1, 0, 0, 0, 0, $szelesseg, $magassag);
  }

  $kep2 = imagecreatefrompng($utvonal.'/'.'png/'.$fajl2.'.png');
  imagecopy($image, $kep2, $kezdopont, 0, 0, 0, $szelesseg, $magassag);

  imagepng($image);
  imagedestroy($image);

  return;
}

function E_5_termeszetes_szamok_kettes_szamrendszer_valtas_10_bol_2_be()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,4); 
  } elseif ($level == 2) {
    $hossz = rand(5,8);
  } elseif ($level == 3) {
    $hossz = rand(9,11);
  }
  
  $correct = randGenerate($hossz,2);
  $helyiertekek = array(0,1,2,4,8,16,32,64,128,256,512,1024);
  $szamjegyek = str_split($correct);
  
  $szam = 0;
  
  foreach ($szamjegyek as $key => $value) {
    if ($value == 1) {
      $szam = $szam + pow(2,$hossz-$key-1);
    }
  }
  
  $question = 'A $0$ és $1$ számjegyek segítségével váltsuk át kettes számrendszerbe az alábbi számot!$$'.$szam.'_{10}$$';
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}




/*** Római számrendszer ***/
function E_5_termeszetes_szamok_romai_szamrendszer_romaibol_tizesbe()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $szam = rand(1,399);
  } elseif ($level == 2) {
    $szam = rand(400,3999);
  } elseif ($level == 3) {
    $szam = randGenerate(rand(5,6),10);
    while ($szam >= 4000000) {
      $szam = randGenerate(rand(5,6),10);
    }
  }
  
  $romai_szam = convertRoman($szam);
  
  if ($szam > 9999) {
    $szam = number_format($szam,0,',','\\\,');
  }
  
  $question = 'Írjuk fel az alábbi római számot tízes számrendszerben!$$\mathrm{'.$romai_szam.'}$$';
  $options = '';
  $correct = $szam;
  $solution = '$'.$szam.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_romai_szamrendszer_tizesbol_romaiba()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $szam = rand(1,399);
  } elseif ($level == 2) {
    $szam = rand(400,1999);
  } elseif ($level == 3) {
    $szam = rand(2000,3999);
  }
  
  $romai_szam = convertRoman($szam);
  
  $question = 'Írjuk fel római számokkal!$$'.$szam.'$$';
  $correct = $romai_szam;
  $options = '';
  $solution = '$\\\\mathrm{'.$romai_szam.'}$';


  return array($question, $options, $correct, $solution);
}




/*** Számegyenes ***/
function E_5_termeszetes_szamok_szamegyenes_osztaskoz()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $osztaskoz = pow(10,rand(1,4)-1);
    $kulonbseg = 1;
    $minpontok = 4;
    $kezdoertek = rand(0,2)*$osztaskoz;
  } elseif ($level == 2) {
    $osztaskoz = 5*pow(10,rand(1,4)-1);
    $kulonbseg = rand(2,3);
    $minpontok = 7;
    $kezdoertek = rand(3,5)*$osztaskoz;
  } elseif ($level == 3) {
    $osztaskoz = rand(2,9)*pow(10,rand(1,4)-1);
    $kulonbseg = rand(3,4);
    $minpontok = 9;
    $kezdoertek = rand(6,9)*$osztaskoz;
  }
  
  if (strlen($osztaskoz) == 4) {
    $pontok = rand($minpontok,9);
  } elseif (strlen($osztaskoz) == 3) {
    $pontok = rand($minpontok,11);
  } else {
    $pontok = rand($minpontok,15);
  }
  
  $poz1 = rand(1,$pontok-$kulonbseg);
  $poz2 = $poz1 + $kulonbseg;
  $poz3 = rand(1,$pontok);
  while ($poz3 == $poz1 || $poz3 == $poz2) {
    $poz3 = rand(1,$pontok);
  }

  $ertek1 = $kezdoertek + ($poz1-1)*$osztaskoz;
  $ertek2 = $kezdoertek + ($poz2-1)*$osztaskoz;
  $ertek3 = $kezdoertek + ($poz3-1)*$osztaskoz;
  
  $utvonal = $_SESSION['utvonal'];
  $question = 'Mekkora az alábbi számegyenes osztásköze?
        <div class="text-center">
          <img class="img-question" width="100%" src="functions_math.php?generate=P_5_termeszetes_szamok_szamegyenes_osztaskoz&poz1='.$poz1.'&ertek1='.$ertek1.'&poz2='.$poz2.'&ertek2='.$ertek2.'&pontok='.$pontok.'">
        </div>';
  
  $correct = $osztaskoz;
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

/* Number line - stepsize */
function P_5_termeszetes_szamok_szamegyenes_osztaskoz()
{

  header('Content-Type: image/png');

  $w = 400;
  $h = 50;
  $hspace = 10;
  $vspace = 30;

  $pontok = $_GET['pontok'];
  $poz1 = $_GET['poz1'];
  $poz2 = $_GET['poz2'];
  $ertek1 = $_GET['ertek1'];
  $ertek2 = $_GET['ertek2'];

  $img = imagecreatetruecolor($w, $h) or die('Nem sikerült képet létrehozni');

  $white = imagecolorallocate($img, 255, 255, 255);
  $black = imagecolorallocate($img, 0, 0, 0);

  imagefill($img, 0, 0, $white);
  imagecolortransparent($img, $white);

  // nyíl
  imageline($img, $hspace, $h-$vspace, $w-$hspace, $h-$vspace, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace+5, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace-5, $black);

  // beosztasok
  $osztaskoz = ($w - 2*$hspace - 40)/($pontok-1);
  $kezdoPoz = $hspace+20;
  // imagestring($img, 4, 0, 0, $pontok, $black);
  for ($i=0; $i < $pontok; $i++) { 
    $osztoPoz = $kezdoPoz+$i*$osztaskoz;
    imageline($img, $osztoPoz, $h-$vspace+5, $osztoPoz, $h-$vspace-5, $black);
    if ($poz1 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek1)*4, $h-20, $ertek1, $black);
    }
    if ($poz2 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek2)*4, $h-20, $ertek2, $black);
    }
  }

  imagepng($img);
  imagedestroy($img);

  return;
}

function E_5_termeszetes_szamok_szamegyenes_szamegyenes()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $osztaskoz = pow(10,rand(1,4)-1);
    $kulonbseg = 1;
    $minpontok = 4;
  } elseif ($level == 2) {
    $osztaskoz = 5*pow(10,rand(1,4)-1);
    $kulonbseg = rand(1,2);
    $minpontok = 7;
  } elseif ($level == 3) {
    $osztaskoz = rand(2,9)*pow(10,rand(1,4)-1);
    $kulonbseg = rand(2,4);
    $minpontok = 9;
  }
  
  if (strlen($osztaskoz) == 4) {
    $pontok = rand($minpontok,9);
  } elseif (strlen($osztaskoz) == 3) {
    $pontok = rand($minpontok,11);
  } else {
    $pontok = rand($minpontok,15);
  }
  
  $poz1 = rand(1,$pontok-$kulonbseg);
  $poz2 = $poz1 + $kulonbseg;
  $poz3 = rand(1,$pontok);
  while ($poz3 == $poz1 || $poz3 == $poz2) {
    $poz3 = rand(1,$pontok);
  }
  
  $kezdoertek = rand(0,9)*$osztaskoz;
  $ertek1 = $kezdoertek + ($poz1-1)*$osztaskoz;
  $ertek2 = $kezdoertek + ($poz2-1)*$osztaskoz;
  $ertek3 = $kezdoertek + ($poz3-1)*$osztaskoz;
  
  $utvonal = $_SESSION['utvonal'];
  $question = 'Melyik számot jelöli a kérdőjel?
        <div class="text-center">
          <img class="img-question" width="100%" src="functions_math.php?generate=P_5_termeszetes_szamok_szamegyenes_szamegyenes&poz1='.$poz1.'&ertek1='.$ertek1.'&poz2='.$poz2.'&ertek2='.$ertek2.'&poz3='.$poz3.'&pontok='.$pontok.'">
        </div>';
  
  $correct = $ertek3;
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

/* Number line - position */
function P_5_termeszetes_szamok_szamegyenes_szamegyenes()
{
  header('Content-Type: image/png');

  $w = 400;
  $h = 50;
  $hspace = 10;
  $vspace = 30;

  $pontok = $_GET['pontok'];
  $poz1 = $_GET['poz1'];
  $poz2 = $_GET['poz2'];
  $poz3 = $_GET['poz3'];
  $ertek1 = $_GET['ertek1'];
  $ertek2 = $_GET['ertek2'];

  $img = imagecreatetruecolor($w, $h) or die('Nem sikerült képet létrehozni');

  $white = imagecolorallocate($img, 255, 255, 255);
  $black = imagecolorallocate($img, 0, 0, 0);

  imagefill($img, 0, 0, $white);
  imagecolortransparent($img, $white);

  // nyíl
  imageline($img, $hspace, $h-$vspace, $w-$hspace, $h-$vspace, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace+5, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace-5, $black);

  // beosztasok
  $osztaskoz = ($w - 2*$hspace - 40)/($pontok-1);
  $kezdoPoz = $hspace+20;
  // imagestring($img, 4, 0, 0, $pontok, $black);
  for ($i=0; $i < $pontok; $i++) { 
    $osztoPoz = $kezdoPoz+$i*$osztaskoz;
    imageline($img, $osztoPoz, $h-$vspace+5, $osztoPoz, $h-$vspace-5, $black);
    if ($poz1 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek1)*4, $h-20, $ertek1, $black);
    }
    if ($poz2 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek2)*4, $h-20, $ertek2, $black);
    }
    if ($poz3 == $i+1) {
      imagestring($img, 4, $osztoPoz-4, $h-20, '?', $black);
    }
  }

  imagepng($img);
  imagedestroy($img);

  return;
}

function E_5_termeszetes_szamok_szamegyenes_muveletek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $osztaskoz = pow(10,rand(1,4)-1);
    $pont12kulonbseg = 1;
    $minpontokszama = 4;
    $szinesiveke = 1;
    $muvelethossz = rand(2,2);
    $kezdoertek = rand(0,2)*$osztaskoz;
  } elseif ($level == 2) {
    $osztaskoz = 5*pow(10,rand(1,4)-1);
    $pont12kulonbseg = rand(1,2);
    $minpontokszama = 7;
    $szinesiveke = 1;
    $muvelethossz = rand(3,4);
    $kezdoertek = rand(3,5)*$osztaskoz;
  } elseif ($level == 3) {
    $osztaskoz = rand(2,9)*pow(10,rand(1,4)-1);
    $pont12kulonbseg = rand(2,4);
    $minpontokszama = 9;
    $szinesiveke = 0;
    $muvelethossz = rand(5,6);
    $kezdoertek = rand(6,9)*$osztaskoz;
  }
  
  if (strlen($osztaskoz) == 4) {
    $pontok = rand($minpontokszama,9);
  } elseif (strlen($osztaskoz) == 3) {
    $pontok = rand($minpontokszama,11);
  } else {
    $pontok = rand($minpontokszama,15);
  }
  
  $poz1 = rand(1,$pontok-$pont12kulonbseg);
  $poz2 = $poz1 + $pont12kulonbseg;
  
  $ertek1 = $kezdoertek + ($poz1-1)*$osztaskoz;
  $ertek2 = $kezdoertek + ($poz2-1)*$osztaskoz;
  
  $kevertpontok = range(1,$pontok,1);
  shuffle($kevertpontok);
  
  $muveletHosszu = '';
  $muveletRovid = '';
  for ($i=0; $i < $muvelethossz; $i++) { 
    if ($i == 0) {
      $szam = $kezdoertek + ($kevertpontok[$i]-1)*$osztaskoz;
      $muveletHosszu = $muveletHosszu.$szam;
      $muveletRovid = $muveletRovid.$kevertpontok[$i];
    } else {
      $muveletRovid = $muveletRovid.'_'.$kevertpontok[$i];
      $szam = abs($kevertpontok[$i]-$kevertpontok[$i-1])*$osztaskoz;
      if ($kevertpontok[$i] > $kevertpontok[$i-1]) {
        $muveletHosszu = $muveletHosszu.'+'.$szam;
      } else {
        $muveletHosszu = $muveletHosszu.'-'.$szam;
      }
    }
  }

  $utvonal = $_SESSION['utvonal'];
  $question = 'Melyik műveletsort ábrázoltuk a számegyenesen?
        <div class="text-center">
          <img class="img-question" width="100%" src="functions_math.php?generate=P_5_termeszetes_szamok_szamegyenes_muveletek&poz1='.$poz1.'&ertek1='.$ertek1.'&poz2='.$poz2.'&ertek2='.$ertek2.'&pontok='.$pontok.'&szinese='.$szinesiveke.'&muveletek='.$muveletRovid.'">
        </div>';
  
  $correct = $muveletHosszu;
  $solution = '$'.$correct.'$';
  
  $options = '';


  return array($question, $options, $correct, $solution);
}

/* Number line - operations */
function P_5_termeszetes_szamok_szamegyenes_muveletek()
{
  header('Content-Type: image/png');

  $w = 400;
  $hspace = 10;
  $vspace = 30;

  $pontok = $_GET['pontok'];
  $poz1 = $_GET['poz1'];
  $poz2 = $_GET['poz2'];
  $ertek1 = $_GET['ertek1'];
  $ertek2 = $_GET['ertek2'];
  $szinese = $_GET['szinese'];
  $muveletek = $_GET['muveletek'];

  // milyen nagy legyen a kép?
  $muveletek = explode('_',$muveletek);
  $ivatmero = 0;
  foreach ($muveletek as $key => $value) {
    if ($key < count($muveletek) - 1 && $ivatmero < abs($value - $muveletek[$key+1])) {
      $ivatmero = abs($value - $muveletek[$key+1]);
    }
  }
  $osztaskoz = ($w - 2*$hspace - 40)/($pontok-1);
  $h = 1.5*$vspace + $ivatmero/2*$osztaskoz;

  // kep
  $img = imagecreatetruecolor($w, $h) or die('Nem sikerült képet létrehozni');

  $white = imagecolorallocate($img, 255, 255, 255);
  $black = imagecolorallocate($img, 0, 0, 0);
  $red = imagecolorallocate($img, 255, 0, 0);
  $green = imagecolorallocate($img, 51, 204, 51);
  $blue = imagecolorallocate($img, 0, 41, 163);

  imagefill($img, 0, 0, $white);
  imagecolortransparent($img, $white);

  // nyíl
  imageline($img, $hspace, $h-$vspace, $w-$hspace, $h-$vspace, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace+5, $black);
  imageline($img, $w-$hspace, $h-$vspace, $w-$hspace-5, $h-$vspace-5, $black);

  // beosztasok
  $osztaskoz = ($w - 2*$hspace - 40)/($pontok-1);
  $kezdoPoz = $hspace+20;
  for ($i=0; $i < $pontok; $i++) { 
    $osztoPoz = $kezdoPoz+$i*$osztaskoz;
    imageline($img, $osztoPoz, $h-$vspace+5, $osztoPoz, $h-$vspace-5, $black);
    if ($poz1 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek1)*4, $h-20, $ertek1, $black);
    }
    if ($poz2 == $i+1) {
      imagestring($img, 4, $osztoPoz - strlen($ertek2)*4, $h-20, $ertek2, $black);
    }
  }

  // ivek
  foreach ($muveletek as $key => $value) {
    if ($key < count($muveletek) - 1) {
      $width = abs($value-$muveletek[$key+1])*$osztaskoz;
      $cx = $kezdoPoz+(($value+$muveletek[$key+1])/2-1)*$osztaskoz;
      if ($szinese == 1) {
        if ($value < $muveletek[$key+1]) {
          $szin = $blue;
        } else {
          $szin = $green;
        }
      } else {
        $szin = $black;
      }
      imagearc($img, $cx, $h-$vspace, $width, $width, 180, 0, $szin);
    }
  }

  // kor
  $cx = $kezdoPoz+($muveletek[0]-1)*$osztaskoz;
  imagefilledarc($img, $cx, $h-$vspace, 10, 10, 0, 360, $red, IMG_ARC_PIE);

  imagepng($img);
  imagedestroy($img);

  return;
}

/* Calculate distance between snails */
function E_5_termeszetes_szamok_szamegyenes_csiga()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];

  if ($level == 1) {
    $unit = rand(1, 2);
    $no = rand(2, 3);
  } elseif ($level == 2) {
    $unit = rand(2, 3);
    $no = rand(4, 6);
  } elseif ($level == 3) {
    $unit = rand(3, 5);
    $no = rand(7, 10);
  }

  if ($no * $unit > 10) {
    $length = 'centiméter';
  } else {
    $length = 'deciméter';
  }

  $diff = ($no-1) * $unit;

  $question = 'Csiga Béla és Csiga Boglárka elhatározták, felmásznak két szomszédos, függőlegesen álló, egyenes nádszálra, hogy többet lássanak a világból. Egy idő múlva Boglárka rémülten észlelte, hogy Béla már sokkal magasabbra jutott. Béla a földtől számítva '.$no.'-'.addSuffixTimes($no).' akkora utat tett meg, mint ő, és így éppen '.$diff.' '.$length.'rel előzte meg őt. Hány '.$length.'re volt ekkor a földtől Béla?<div class="text-center"><img class="img-question" width="50%" src="'.$utvonal.'/csiga.jpg"></div><p class="text-center small image-description"> <a target="_blank" href="https://en.wikipedia.org/wiki/File:Snail-WA_wolfied.jpg">Forrás</a></p>';

  $options = '';
  $correct = $no * $unit;
  $solution = '$'.$correct.'$ '.$length.'re';

  return array($question, $options, $correct, $solution);
}



/*** Számok összehasonlítása ***/
function E_5_termeszetes_szamok_osszehasonlitas_relacios_jelek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam1 = randGenerate($hossz,10);
  
  if (rand(1,4) == 1) {
    $szam2 = $szam1;
  } else {
    if (rand(1,4) == 1) {
      if (rand(1,2) == 1) {
        $hossz++;
      } else {
        $hossz--; 
      }
    }
    $szam2 = $szam1 + randGenerate(rand(floor($hossz/2),$hossz),10);
  }
  
  $relacios_jelek_egyenlo = array("<=",">=","=");
  $relacios_jelek_kisebb = array("<","<=","!=");
  $relacios_jelek_nagyobb = array(">",">=","!=");
  
  shuffle($relacios_jelek_egyenlo);
  shuffle($relacios_jelek_kisebb);
  shuffle($relacios_jelek_nagyobb);
  
  if (rand(1,4) == 1) {
    $szam2 = $szam1;
  }
  
  if ($szam1 == $szam2) {
    $jo = $relacios_jelek_egyenlo[0];
    $options = array($jo,"!=","<",">");
  } elseif ($szam1 > $szam2) {
    $jo = $relacios_jelek_nagyobb[0];
    $options = array($jo,"=","<","<=");
  } else {
    $jo = $relacios_jelek_kisebb[0];
    $options = array($jo,"=",">",">=");
  }
  
  if ($szam1 > 9999) {$szam1 = number_format($szam1,0,',','\,');}
  if ($szam2 > 9999) {$szam2 = number_format($szam2,0,',','\,');}
  
  $options = preg_replace( '/^<$/', '$<$', $options);
  $options = preg_replace( '/^>$/', '$>$', $options);
  $options = preg_replace( '/^=$/', '$=$', $options);
  $options = preg_replace( '/^!=$/', '$\\neq$', $options);
  $options = preg_replace( '/^<=$/', '$\\leq$', $options);
  $options = preg_replace( '/^>=$/', '$\\geq$', $options);
  
  $question = 'Melyik relációs jel írható a kérdőjel helyére?$$'.$szam1.'\quad?\quad'.$szam2.'$$';
  
  $correct = 0;
  $solution = '$'.$jo.'$';
  shuffleAssoc($options);


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszehasonlitas_osszehasonlitas()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3); 
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam1 = randGenerate($hossz,10);
  
  if (rand(1,4) == 1) {
    $szam2 = $szam1;
  } else {
    if (rand(1,4) == 1) {
      if (rand(1,2) == 1) {
        $hossz++;
      } else {
        $hossz--; 
      }
    }
    $szam2 = $szam1 + randGenerate(rand(floor($hossz/2),$hossz),10);
  }
  
  $relacios_jelek_egyenlo = array("legalább akkora, mint","nagyobb vagy egyenlő, mint","legfeljebb akkora, mint","kisebb vagy egyenlő, mint","egyenlő");
  $relacios_jelek_kisebb = array("kisebb, mint","legalább akkora, mint","nagyobb vagy egyenlő, mint","nem egyenlő");
  $relacios_jelek_nagyobb = array("nagyobb, mint","legfeljebb akkora, mint","kisebb vagy egyenlő, mint","nem egyenlő");
  
  shuffle($relacios_jelek_egyenlo);
  shuffle($relacios_jelek_kisebb);
  shuffle($relacios_jelek_nagyobb);
  
  if (rand(1,4) == 1) {
    $szam2 = $szam1;
  }
  
  if ($szam1 == $szam2) {
    $jo = $relacios_jelek_egyenlo[0];
    $options = array($jo,"nem egyenlő","kisebb, mint","nagyobb, mint");
  } elseif ($szam1 > $szam2) {
    $jo = $relacios_jelek_nagyobb[0];
    $options = array($jo,"egyenlő","kisebb, mint","legalább akkora, mint","nagyobb vagy egyenlő, mint");
  } else {
    $jo = $relacios_jelek_kisebb[0];
    $options = array($jo,"egyenlő","nagyobb, mint","legfeljebb akkora, mint","kisebb vagy egyenlő, mint");
  }
  
  if ($szam1 > 9999) {
    $szam1 = number_format($szam1,0,',','\,');
  }
  
  if ($szam2 > 9999) {
    $szam2 = number_format($szam2,0,',','\,');
  }
  
  $question = 'Melyik állítás igaz?$$A:'.$szam1.'\qquad B:'.$szam2.'$$';
  
  $correct = 0;
  $solution = '$'.$jo.'$';
  shuffleAssoc($options);
  
  $options = preg_replace( '/^/', '$A$ ', $options);
  $options = preg_replace( '/$/', ' $B.$', $options);
  $options = preg_replace( '/egyenlő \$B.\$$/', 'egyenlő $B$-vel.', $options);


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszehasonlitas_sorrend()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3);
    $darab = rand(2,3);
  } elseif ($level == 2) {
    $hossz = rand(4,6);
    $darab = rand(4,5);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $darab = rand(6,7);
  }
  
  $betuk = array("A","B","C","D","E","F","G");
  $szam = randGenerate($hossz,10);
  
  for ($i=0; $i < $darab; $i++) {
  
    $ujszam = newNum($szam,$hossz);
  
    if ($i == 0) {
      $szamok[$betuk[$i]] = $ujszam;
    } else {
      while (!hasDigit($szamok,$ujszam)) {
        $ujszam = newNum($szam,$hossz);
      }
    }
  
    $szamok[$betuk[$i]] = $ujszam;
  }

  shuffle($betuk);
  
  $felsorolas = '$$\begin{align}';
  foreach ($szamok as $key => $value) {
    if ($value > 9999) {
      $valuenew = number_format($value,0,',','\,');
    } else {
      $valuenew = $value;
    }
    $felsorolas = $felsorolas.$key.'&:&'.$valuenew.'\\\\';
  }
  $felsorolas = $felsorolas.'\end{align}$$';
  
  $options = '';
  
  if (rand(1,2) == 1) {
    $irany = 'csökkenő';
    arsort($szamok);
  } else {
    $irany = 'növekvő';
    asort($szamok);
  }
  
  $question = 'Rendezd a számokat '.$irany.' sorrendbe, és írd le egymás mellé a számokat jelölő betűket!'.$felsorolas;
  
  $correct = '';
  foreach ($szamok as $key => $value) {
    $correct = $correct.$key;
  }
  
  $solution = '$'.$correct.'$';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszehasonlitas_hianyzo_szamjegy()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3);
  } elseif ($level == 2) {
    $hossz = rand(4,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $relacios_jelek = array("<",">","=","!=","<=",">=");
  shuffle($relacios_jelek);
  $rel = $relacios_jelek[0];
  
  $szamjegyek = range(0, 9);
  
  if ($rel == ">") {
    $szamjegy = rand(2,7);
    $jo = $szamjegy-1;
    $rossz = array_slice($szamjegyek, $szamjegy);
  } elseif ($rel == ">=") {
    $szamjegy = rand(0,8);
    $jo = $szamjegy;
    $rossz = array_slice($szamjegyek, $szamjegy+1);
  } elseif ($rel == "<") {
    $szamjegy = rand(3,8);
    $jo = $szamjegy+1;
    $rossz = array_slice($szamjegyek, 0, $szamjegy+1);
  } elseif ($rel == "<=") {
    $szamjegy = rand(2,9);
    $jo = $szamjegy;
    $rossz = array_slice($szamjegyek, 0, $szamjegy);
  } else {
    $szamjegy = rand(0,9);
    $jo = $szamjegy;
    unset($szamjegyek[array_search($jo, $szamjegyek)]);
    $rossz = array_values($szamjegyek);
  }
  
  $szam = randGenerate($hossz,10);
  $helyiertek = rand(1,$hossz-1);
  
  if (rand(1,2) == 1) {
    $szam_bal = replaceDigit($szam,$helyiertek,$szamjegy);
    $szam_jobb = replaceDigit($szam,$helyiertek,'?');
  } else {
    $szam_bal = replaceDigit($szam,$helyiertek,'?');
    $szam_jobb = replaceDigit($szam,$helyiertek,$szamjegy);
    $rel = preg_replace( '/</', '>', $rel);
    $rel = preg_replace( '/>/', '<', $rel);
  }
  
  $options[0] = $jo;
  foreach ($rossz as $key => $value) {
    $options[$key+1] = $value;
  }
  
  shuffleAssoc($options);
  
  $correct = 0;
  $solution = '$'.$szamjegy.'$';
  
  if ($rel == "!=") {
    $negacio = ' nem ';
  } else {
    $negacio = ' ';
  }
  
  $rel = preg_replace( '/^<$/', '<', $rel);
  $rel = preg_replace( '/^>$/', '>', $rel);
  $rel = preg_replace( '/^=$/', '=', $rel);
  $rel = preg_replace( '/^!=$/', '\\neq', $rel);
  $rel = preg_replace( '/^<=$/', '\\leq', $rel);
  $rel = preg_replace( '/^>=$/', '\\geq', $rel);
  
  $question = 'Melyik számjegy'.$negacio.'írható a kérdőjel helyére?$$'.$szam_bal.$rel.$szam_jobb.'$$';

  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszehasonlitas_hany_termeszetes_szam()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $intervallum = rand(1,5);
    $hossz = rand(1,2); 
  } elseif ($level == 2) {
    $intervallum = rand(6,15);
    $hossz = rand(3,6);
  } elseif ($level == 3) {
    $intervallum = rand(16,20);
    $hossz = rand(7,10);
  }
  
  if ($level == 1 && rand(1,3) == 1) {
    $szam_bal = 0;
  } else {
    $szam_bal = randGenerate($hossz,10);
  }
  
  $szam_jobb = $szam_bal + $intervallum;
  $correct = $szam_jobb - $szam_bal - 1;
  
  if (rand(1,2) == 1) {
    $hatarpont_bal = TRUE;
    $correct++;
  } else {
    $hatarpont_bal = FALSE;
  }
  
  if (rand(1,2) == 1) {
    $hatarpont_jobb = TRUE;
    $correct++;
  } else {
    $hatarpont_jobb = FALSE;
  }
  
  $opcio_kivalasztott = rand(0,3);
  
  if ($szam_bal > 9999) {$szam_bal = number_format($szam_bal,0,',','\,');}
  if ($szam_jobb > 9999) {$szam_jobb = number_format($szam_jobb,0,',','\,');}
  
  if ($hatarpont_bal) {
    if ($hatarpont_bal == 0 && rand(1,2) == 1) {
      if (rand(1,2 == 1)) {
        $szoveg_bal = '';
      }
    } else {
      $opcio_bal[0] = 'legalább $'.$szam_bal.'$';
      $opcio_bal[1] = 'nem kisebb, mint $'.$szam_bal.'$';
      $opcio_bal[2] = 'nem kisebb $'.$szam_bal.'$-'.addSuffixBy($szam_bal);
      $opcio_bal[3] = '$'.$szam_bal.'$-'.addSuffixBy($szam_bal).' nem kisebb';
      $szoveg_bal = $opcio_bal[$opcio_kivalasztott];
    }
  } else {
    $opcio_bal[0] = 'nagyobb, mint $'.$szam_bal.'$';
    $opcio_bal[1] = 'nagyobb, mint $'.$szam_bal.'$';
    $opcio_bal[2] = 'nagyobb $'.$szam_bal.'$-'.addSuffixBy($szam_bal);
    $opcio_bal[3] = '$'.$szam_bal.'$-'.addSuffixBy($szam_bal).' nagyobb';
    $szoveg_bal = $opcio_bal[$opcio_kivalasztott];
  }
  
  if ($hatarpont_jobb) {
    $opcio_jobb[0] = 'legfeljebb $'.$szam_jobb.'$';
    $opcio_jobb[1] = 'nem nagyobb, mint $'.$szam_jobb.'$';
    $opcio_jobb[2] = 'nem nagyobb $'.$szam_jobb.'$-'.addSuffixBy($szam_jobb);
    $opcio_jobb[3] = '$'.$szam_jobb.'$-'.addSuffixBy($szam_jobb).' nem nagyobb';
    $szoveg_jobb = $opcio_jobb[$opcio_kivalasztott];
  } else {
    $opcio_jobb[0] = 'kisebb, mint $'.$szam_jobb.'$';
    $opcio_jobb[1] = 'kisebb, mint $'.$szam_jobb.'$';
    $opcio_jobb[2] = 'kisebb $'.$szam_jobb.'$-'.addSuffixBy($szam_jobb);
    $opcio_jobb[3] = '$'.$szam_jobb.'$-'.addSuffixBy($szam_jobb).' kisebb';
    $szoveg_jobb = $opcio_jobb[$opcio_kivalasztott];
  }
  
  $options = '';
  
  $question = 'Hány olyan természetes szám van, amely '.$szoveg_bal.' és '.$szoveg_jobb.'?';
  
  $solution = '$'.$correct.'$';


  return array($question, $options, $correct, $solution);
}




/*** Kerekítés ***/
function E_5_termeszetes_szamok_kerekites_kerekites_le()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $correct = floor($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1);
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Kerekítsük lefelé '.$helyiertekek[$helyiertek-1].' az alábbi számot!$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_kerekites_fel()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $correct = ceil($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1);
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Kerekítsük felfelé '.$helyiertekek[$helyiertek-1].' az alábbi számot!$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_kerekites()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $correct = round($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1);
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Kerekítsük '.$helyiertekek[$helyiertek-1].' az alábbi számot!$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_kerekitesi_hiba_le()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $szamkerek = floor($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1); 
  $correct = abs($szam-$szamkerek);;
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Mekkora lesz a kerekítési hiba, ha az alábbi számot '.$helyiertekek[$helyiertek-1].' kerekítjük lefelé?$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_kerekitesi_hiba_fel()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $szamkerek = ceil($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1); 
  $correct = abs($szam-$szamkerek);
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Mekkora lesz a kerekítési hiba, ha az alábbi számot '.$helyiertekek[$helyiertek-1].' kerekítjük felfelé?$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_kerekitesi_hiba()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $szamkerek = round($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1); 
  $correct = abs($szam-$szamkerek);
  
  $options = '';
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Mekkora lesz a kerekítési hiba, ha az alábbi számot '.$helyiertekek[$helyiertek-1].' kerekítjük?$$'.$szam.'$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_hany_szam()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(1,2);
    $helyiertek = rand(1,$hossz+1);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
    $helyiertek = rand(3,$hossz+1);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
    $helyiertek = rand(6,min($hossz+1,10));
  }
  
  $helyiertekek = array("az egyesekre","a tízesekre","a százasokra","az ezresekre","a tízezresekre","a százezresekre","a milliósokra","a tízmilliósokra","a százmilliósokra","a milliárdosokra");
  
  $szam = randGenerate($hossz,10);
  $szamkerek = round($szam/pow(10,$helyiertek-1))*pow(10,$helyiertek-1); 
  
  $options = '';
  
  if ($szamkerek > 9999) { $szamkerek = number_format($szamkerek,0,',','\,');}
  
  $question = 'Hány olyan természetes szám van, aminek '.$helyiertekek[$helyiertek-1].' kerekített értéke $'.$szamkerek.'$?';
  
  if ($szamkerek == 0) {
    if ($helyiertek == 1) {
      $correct = 1;
    } else {
      $correct = 5*pow(10,$helyiertek-2);
    }
  } else {
    $correct = pow(10,$helyiertek-1);
  }
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kerekites_nem_kerekitett()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $hossz = rand(2,3);
  } elseif ($level == 2) {
    $hossz = rand(3,6);
  } elseif ($level == 3) {
    $hossz = rand(7,10);
  }
  
  $szam = randGenerate($hossz,10);
  
  for ($i=0; $i < $hossz+1; $i++) {
    $options_jo[$i] = round($szam/pow(10,$i))*pow(10,$i);
    $options_fel[$i] = ceil($szam/pow(10,$i))*pow(10,$i);
    $options_le[$i] = floor($szam/pow(10,$i))*pow(10,$i);
    $options_rossz[$i] = round($szam/pow(10,$i)+1)*pow(10,$i);
    $options_rossz_fel[$i] = ceil($szam/pow(10,$i)+1)*pow(10,$i);
    $options_rossz_le[$i] = floor($szam/pow(10,$i)+1)*pow(10,$i);
  }
  
  $sikerult = FALSE;
  $helyiertekek = range(0,$hossz);
  shuffle($helyiertekek);
  
  foreach ($helyiertekek as $value) {
    if (!$sikerult && !in_array($options_fel[$value],$options_jo) && $options_le[$value] != 0) {
      $rossz = $options_fel[$value];
      $options_jo[$value] = $rossz;
      $sikerult = TRUE;
    } elseif (!$sikerult && !in_array($options_le[$value],$options_jo) && $options_le[$value] != 0) {
      $rossz = $options_le[$value];
      $options_jo[$value] = $rossz;
      $sikerult = TRUE;
    }
  }
  
  if (!$sikerult) {
    foreach ($helyiertekek as $value) {
      if (!$sikerult && !in_array($options_rossz[$value],$options_jo) && $options_rossz[$value] != 0) {
        $rossz = $options_rossz[$value];
        $options_jo[$value] = $rossz;
        $sikerult = TRUE;
      } elseif (!$sikerult && !in_array($options_rossz_le[$value],$options_jo) && $options_rossz_le[$value] != 0) {
        $rossz = $options_rossz_le[$value];
        $options_jo[$value] = $rossz;
        $sikerult = TRUE;
      } elseif (!$sikerult && !in_array($options_rossz_fel[$value],$options_jo) && $options_rossz_fel[$value] != 0) {
        $rossz = $options_rossz_fel[$value];
        $options_jo[$value] = $rossz;
        $sikerult = TRUE;
      }
    }
  }
  
  if (!$sikerult) {
    print_r('Feladatgenerálás sikertelen. Kérlek, frissítsd a honlapot!');
  }
  
  $options = array_unique($options_jo);
  $correct = array_search($rossz, $options_jo);
  
  if ($options[$correct] > 9999) {
    $solution = '$'.number_format($options[$correct],0,',','\\\\,').'$';
  } else {
    $solution = '$'.$options[$correct].'$';
  }
  
  foreach ($options as $key => $value) {
    if ($value > 9999) {
      $options[$key] = '$'.number_format($value,0,',','\,').'$';
    } else {
      $options[$key] = '$'.$value.'$';
    }
  }
  
  if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}
  
  $question = 'Melyik nem lehet az alábbi szám kerekített értéke?$$'.$szam.'$$';


  return array($question, $options, $correct, $solution);
}




/*** Összeadás ***/
function E_5_termeszetes_szamok_osszeadas_osszeadas()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  
  if ($szam2 < $szam1) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }
  
  $options = '';
  $correct = $szam1+$szam2;
  if ($szam1 > 999) { $szam1 = number_format($szam1,0,',','\,');}
  if ($szam2 > 999) { $szam2 = number_format($szam2,0,',','\,');}
  $question = 'Adjuk össze az alábbi számokat!$$\begin{align}'.$szam1.'\\\\ +\,'.$szam2.'\\\\ \hline?\end{align}$$';

  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszeadas_modosit()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
    $modosit1 = rand(1,2);
    $modosit2 = rand(1,2);
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
    $modosit1 = rand(3,4);
    $modosit2 = rand(3,4);
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
    $modosit1 = rand(5,9);
    $modosit2 = rand(5,9);
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam3 = $szam1+$szam2;
  
  if (rand(1,2) == 1) {
    $szam1b = $szam1 + $modosit1;
  } else {
    $szam1b = max(0,$szam1 - $modosit1);
  }
  
  if ($level > 1) {
    if (rand(1,2) == 1) {
      $szam2b = $szam2 + $modosit2;
    } else {
      $szam2b = max(0,$szam2 - $modosit2);
    }
  } else {
    $szam2b = $szam2;
  }
  
  if ($level > 1) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }
  
  $options = '';
  $correct = $szam1b+$szam2b;
  if ($szam1 > 9999) { $szam1 = number_format($szam1,0,',','\,');}
  if ($szam1b > 9999) { $szam1b = number_format($szam1b,0,',','\,');}
  if ($szam2 > 9999) { $szam2 = number_format($szam2,0,',','\,');}
  if ($szam2b > 9999) { $szam2b = number_format($szam2b,0,',','\,');}
  if ($szam3 > 9999) { $szam3 = number_format($szam3,0,',','\,');}
  $question = 'Ha tudjuk, hogy $$'.$szam1.'+'.$szam2.'='.$szam3.',$$akkor mennyivel egyenlő $$'.$szam1b.'+'.$szam2b.'?$$';

  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszeadas_osszeadas_tobb()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
    $darab = 3;
  } elseif ($level == 2) {
    $minhossz = 2;
    $maxhossz = 6;
    $darab = 4;
  } elseif ($level == 3) {
    $minhossz = 4;
    $maxhossz = 10;
    $darab = rand(5,6);
  }
  
  for ($i=0; $i < $darab; $i++) { 
    $szamok[$i] = randGenerate(rand($minhossz,$maxhossz),10);
  }
  
  sort($szamok);
  $correct = array_sum($szamok);
  
  $options = '';
  $osszeg = '';
  
  foreach ($szamok as $value) {
    if ($value == end($szamok)) {
      $plusz = '+\,';
    } else {
      $plusz = '';
    }
    if ($value > 999) {
      $osszeg = $osszeg.'\\\\ '.$plusz.number_format($value,0,',','\,');
    } else {
      $osszeg = $osszeg.'\\\\ '.$plusz.$value;
    }
  }
  $osszeg = ltrim($osszeg,'\\\\ ');
  
  $question = 'Adjuk össze az alábbi számokat!$$\begin{align}'.$osszeg.'\\\\ \hline?\end{align}$$';

  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_osszeadas_kerdojelek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam3 = $szam1 + $szam2;
  
  if ($szam2 < $szam1) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }

  $szamjegyek1 = str_split($szam1);
  $szamjegyek2 = str_split($szam2);
  $szamjegyek3 = str_split($szam3);
  
  $szam1b = '';
  $szam2b = '';
  $szam3b = '';
  
  $correct = 0;
  $solution = '';
  
  for ($helyiertek=1; $helyiertek <= $maxhossz+1; $helyiertek++) {
    $melyik = rand(1,3);
    if ($melyik == 1) {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,TRUE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,FALSE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,FALSE,$correct,$solution,$szam3b); 
    } elseif ($melyik == 2) {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,FALSE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,TRUE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,FALSE,$correct,$solution,$szam3b); 
    } else {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,FALSE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,FALSE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,TRUE,$correct,$solution,$szam3b); 
    }
  }
  
  $solution = rtrim($solution,'$+$$');
  $solution = '$'.$correct.'$ ($'.$solution.'$)';

  $options = '';
  $question = 'Mennyi a kérdőjelek helyén álló számjegyek összege?$$\begin{align}'.$szam1b.'\\\\ +\,'.$szam2b.'\\\\ \hline '.$szam3b.'\end{align}$$';


  return array($question, $options, $correct, $solution);
}




/*** Kivonás ***/
function E_5_termeszetes_szamok_kivonas_kivonas()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  
  if ($szam1 < $szam2) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }
  
  $kulonbseg = strlen($szam1)-strlen($szam2);
  $szokoz = '';
  
  for ($i=0; $i < $kulonbseg; $i++) { 
    $szokoz = '\,\,\,'.$szokoz;
  }
  
  $options = '';
  $correct = $szam1-$szam2;
  if ($szam1 > 999) { $szam1 = number_format($szam1,0,',','\,');}
  if ($szam2 > 999) { $szam2 = number_format($szam2,0,',','\,');}
  $question = 'Mi az alábbi kivonás eredménye?$$\begin{align}'.$szam1.'\\\\ -'.$szokoz.'\,'.$szam2.'\\\\ \hline?\end{align}$$';

  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kivonas_modosit()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
    $modosit1 = rand(1,2);
    $modosit2 = rand(1,2);
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
    $modosit1 = rand(3,4);
    $modosit2 = rand(3,4);
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
    $modosit1 = rand(5,9);
    $modosit2 = rand(5,9);
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  
  if ($szam1 < $szam2) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }
  
  $szam3 = $szam1-$szam2;
  
  if (rand(1,2) == 1) {
    $szam1b = $szam1 + $modosit1;
  } else {
    $szam1b = max(1,$szam1 - $modosit1);
  }
  
  if ($level > 1) {
    if (rand(1,2) == 1) {
      $szam2b = min($szam2 + $modosit2,$szam1b);
    } else {
      $szam2b = min(max(1,$szam2 - $modosit2),$szam1b);
    }
  } else {
    $szam2b = min($szam2,$szam1b);
  }
  
  $options = '';
  $correct = $szam1b-$szam2b;
  if ($szam1 > 9999) { $szam1 = number_format($szam1,0,',','\,');}
  if ($szam1b > 9999) { $szam1b = number_format($szam1b,0,',','\,');}
  if ($szam2 > 9999) { $szam2 = number_format($szam2,0,',','\,');}
  if ($szam2b > 9999) { $szam2b = number_format($szam2b,0,',','\,');}
  if ($szam3 > 9999) { $szam3 = number_format($szam3,0,',','\,');}
  $question = 'Ha tudjuk, hogy $$'.$szam1.'-'.$szam2.'='.$szam3.',$$akkor mennyivel egyenlő $$'.$szam1b.'-'.$szam2b.'?$$';

  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kivonas_zarojel()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  if ($level == 1) {
    $szam2 = rand(1,floor($szam1/2));
    $szam3 = rand(1,floor($szam1/2));
  } else {
    $szam2 = randGenerate(strlen($szam1)-1,10);
    $szam3 = randGenerate(strlen($szam1)-1,10);
  }
  
  if ($szam2 < $szam3) {
    list($szam3, $szam2) = array($szam2, $szam3);
  }
  
  $kulonbseg1 = ($szam1-$szam2)-$szam3;
  $kulonbseg2 = $szam1-($szam2-$szam3);
  $betu1 = 'A';
  $betu2 = 'B';
  
  $correct = abs($kulonbseg1-$kulonbseg2);
  
  $options = '';
  
  if ($szam1 > 9999) { $szam1 = number_format($szam1,0,',','\,');}
  if ($szam2 > 9999) { $szam2 = number_format($szam2,0,',','\,');}
  if ($szam3 > 9999) { $szam3 = number_format($szam3,0,',','\,');}
  if ($kulonbseg1 > 9999) { $kulonbseg1 = number_format($kulonbseg1,0,',','\\\\,');}
  if ($kulonbseg2 > 9999) { $kulonbseg2 = number_format($kulonbseg2,0,',','\\\\,');}
  $question = 'Mekkora az eltérés a két kifejezés eredménye között?$$\textrm{A)}\quad('.$szam1.'-'.$szam2.')-'.$szam3.'$$ $$\textrm{B)}\quad'.$szam1.'-('.$szam2.'-'.$szam3.')$$';
  
  if ($correct > 9999) {
    $solution = '$'.number_format($correct,0,',','\\\\,').'$';
  } else {
    $solution = '$'.$correct.'$';
  }
  
  $solution = $solution.' ($\\\\textrm{'.$betu1.'}='.$kulonbseg1.'$, $\\\\textrm{'.$betu2.'}='.$kulonbseg2.'$)';


  return array($question, $options, $correct, $solution);
}

function E_5_termeszetes_szamok_kivonas_kerdojelek()
{
  $level = $_SESSION['level'];


  if ($level == 1) {
    $minhossz = 1;
    $maxhossz = 2;
  } elseif ($level == 2) {
    $minhossz = 3;
    $maxhossz = 6;
  } elseif ($level == 3) {
    $minhossz = 7;
    $maxhossz = 10;
  }
  
  $szam1 = randGenerate(rand($minhossz,$maxhossz),10);
  $szam2 = randGenerate(rand($minhossz,$maxhossz),10);
  
  if ($szam1 < $szam2) {
    list($szam1, $szam2) = array($szam2, $szam1);
  }
  
  $kulonbseg = strlen($szam1)-strlen($szam2);
  $szokoz = '';
  
  for ($i=0; $i < $kulonbseg; $i++) { 
    $szokoz = '\,\,\,'.$szokoz;
  }
  
  $szam3 = $szam1-$szam2;

  $szamjegyek1 = str_split($szam1);
  $szamjegyek2 = str_split($szam2);
  $szamjegyek3 = str_split($szam3);
  
  $szam1b = '';
  $szam2b = '';
  $szam3b = '';
  
  $correct = 0;
  $solution = '';
  
  for ($helyiertek=1; $helyiertek <= $maxhossz+1; $helyiertek++) {
    $melyik = rand(1,3);
    if ($melyik == 1) {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,TRUE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,FALSE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,FALSE,$correct,$solution,$szam3b); 
    } elseif ($melyik == 2) {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,FALSE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,TRUE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,FALSE,$correct,$solution,$szam3b); 
    } else {
      list($correct, $solution, $szam1b) = replaceDigit2($szamjegyek1,$helyiertek,FALSE,$correct,$solution,$szam1b); 
      list($correct, $solution, $szam2b) = replaceDigit2($szamjegyek2,$helyiertek,FALSE,$correct,$solution,$szam2b); 
      list($correct, $solution, $szam3b) = replaceDigit2($szamjegyek3,$helyiertek,TRUE,$correct,$solution,$szam3b); 
    }
  }
  
  $solution = rtrim($solution,'$+$$');
  $solution = '$'.$correct.'$ ($'.$solution.'$)';
  
  $options = '';
  $question = 'Mennyi a kérdőjelek helyén álló számjegyek összege?$$\begin{align}'.$szam1b.'\\\\ -'.$szokoz.'\,'.$szam2b.'\\\\ \hline '.$szam3b.'\end{align}$$';

  return array($question, $options, $correct, $solution);
}





/********** TÖRTSZÁMOK **********/




/*** Törtszámok ***/

/* Define numerator */
function E_5_tortszamok_tortszamok_szamlalo()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
  }

  $question = 'Mekkora a számláló az alábbi törtben?$$\\frac{'.$num.'}{'.$denom.'}$$';
  $options = '';
  $correct = $num;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define denominator */
function E_5_tortszamok_tortszamok_nevezo()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
  }

  $question = 'Mekkora a nevező az alábbi törtben?$$\\frac{'.$num.'}{'.$denom.'}$$';
  $options = '';
  $correct = $denom;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Compare fraction with 1 */
function E_5_tortszamok_tortszamok_osszehasonlitas_1_gyel()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
  }

  $rand = rand(1,3);

  if ($rand == 1) {
    list($num, $denom) = array($denom, $num);
  } elseif ($rand == 2) {
    $num = $denom;
  }

  $frac = $num/$denom;

  $question = 'Melyik relációs jel kerül a kérdőjel helyére?$$\\frac{'.$num.'}{'.$denom.'}\\qquad?\\qquad1$$';
  $options = array(0 => '>', 1 => '<', 2 => '=');

  if ($frac > 1) {
    $correct = 0;
  } elseif ($frac < 1) {
    $correct = 1;
  } else {
    $correct = 2;
  }

  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define ratio in rectangle */
function E_5_tortszamok_tortszamok_teglalap()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $row = 1;
    $col = rand(5,8);
  } elseif ($level == 2) {
    $row = rand(2,3);
    $col = rand(5,8);
  } elseif ($level == 3) {
    $row = rand(5,10);
    $col = rand(5,10);
  }

  $width = floor(100/11*$col);
  $total = $row*$col;
  $num = rand(1, $total);

  $question = 'Az alábbi téglalap hányad része kék?
        <div class="text-center">
          <img class="img-question" width="'.$width.'%" src="functions_math.php?generate=P_5_tortszamok_tortszamok_teglalap&row='.$row.'&col='.$col.'&num='.$num.'">
        </div>';
  $options = 'fraction';
  $correct = $num/$total;
  $solution = '$\\\\frac{'.$num.'}{'.$total.'}$';


  return array($question, $options, $correct, $solution);
}

/* Create rectangle */
function P_5_tortszamok_tortszamok_teglalap()
{
  header('Content-Type: image/png');

  $row = $_REQUEST['row'];
  $col = $_REQUEST['col'];
  $num = $_REQUEST['num'];

  $a = 20;

  $w = $col * $a+1;
  $h = $row * $a+1;

  $img = imagecreatetruecolor($w, $h) or die('Nem sikerült képet létrehozni');

  $white = imagecolorallocate($img, 255, 255, 255);
  $black = imagecolorallocate($img, 0, 0, 0);
  $blue = imagecolorallocate($img, 0, 0, 255);

  imagefill($img, 0, 0, $white);
  // imagecolortransparent($img, $white);

  // colour squares
  $n = 0;
  for ($i=0; $i < $col; $i++) { 
    for ($j=0; $j < $row; $j++) { 
      if ($n < $num) {
        imagefilledrectangle($img, $i*$a, $j*$a, ($i+1)*$a, ($j+1)*$a, $blue);
        $n++;
      } else {
        break;
      }
    }
  }

  // grid
  for ($i=0; $i <= $row; $i++) { 
    imageline($img, 0, $i*$a, $w, $i*$a, $black);
  }
  for ($i=0; $i <= $col; $i++) { 
    imageline($img, $i*$a, 0, $i*$a, $h, $black);
  }

 

  imagepng($img);
  imagedestroy($img);

  return;
}



/*** Átalakítás ***/


/* Calculate reciprocal of fraction */
function E_5_tortszamok_atalakitas_reciprok()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
  }

  $question = 'Számítsd ki a reciprokát!$$\\frac{'.$num.'}{'.$denom.'}$$';
  $options = 'fraction';
  $correct = $denom/$num;
  $solution = '$\\\\frac{'.$denom.'}{'.$num.'}$';

  return array($question, $options, $correct, $solution);
}

/* Convert fraction to integer */
function E_5_tortszamok_atalakitas_tort_egessze()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $denom = rand(1,3);
    $integer = rand(1,5);
  } elseif ($level == 2) {
    $denom = rand(3,10);
    $integer = rand(5,10);
  } elseif ($level == 3) {
    $denom = rand(5,20);
    $integer = rand(10,20);
  }

  $num = $denom * $integer;

  $question = 'Alakítsd egésszé!$$\\frac{'.$num.'}{'.$denom.'}$$';
  $options = '';
  $correct = $integer;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Convert integer to fraction */
function E_5_tortszamok_atalakitas_egesz_tortte()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $denom = 1;
    $integer = rand(1,5);
  } elseif ($level == 2) {
    $denom = rand(3,5);
    $integer = rand(5,10);
  } elseif ($level == 3) {
    $denom = rand(10,20);
    $integer = rand(10,20);
  }

  $num = $denom * $integer;

  $question = 'Melyik szám áll a kérdőjel helyén?$$'.$integer.'=\\frac{?}{'.$denom.'}$$';
  $options = '';
  $correct = $num;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Expand fraction */
function E_5_tortszamok_atalakitas_bovites()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
    $expand = 2;
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
    $expand = rand(3,5);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
    $expand = rand(5,10);
  }

  $question = 'Melyik szám áll a kérdőjel helyén?$$\\frac{'.$num.'}{'.$denom.'}=\\frac{?}{'.$denom*$expand.'}$$';
  $options = '';
  $correct = $num*$expand;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Simplify fraction */
function E_5_tortszamok_atalakitas_egyszerusites()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num = rand(1,3);
    $denom = rand(3,5);
    $expand = 2;
  } elseif ($level == 2) {
    $num = rand(3,10);
    $denom = rand(10,20);
    $expand = rand(3,5);
  } elseif ($level == 3) {
    $num = rand(5,20);
    $denom = rand(30,100);
    $expand = rand(5,10);
  }

  $question = 'Melyik szám áll a kérdőjel helyén?$$\\frac{'.$num*$expand.'}{'.$denom*$expand.'}=\\frac{?}{'.$denom.'}$$';
  $options = '';
  $correct = $num;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}


/*** Műveletek ***/


/* Compare fractions */
function E_5_tortszamok_muveletek_osszehasonlitas()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num1 = rand(1,2);
    $num2 = rand(1,2);
    $denom1 = rand(1,3);
    $denom2 = rand(1,3);
  } elseif ($level == 2) {
    $num1 = rand(3,5);
    $num2 = rand(3,5);
    $denom1 = rand(5,10);
    $denom2 = rand(5,10);
  } elseif ($level == 3) {
    $num1 = rand(5,10);
    $num2 = rand(5,10);
    $denom1 = rand(10,20);
    $denom2 = rand(10,20);
  }

  $frac1 = $num1/$denom1;
  $frac2 = $num2/$denom2;

  $question = 'Melyik relációs jel kerül a kérdőjel helyére? $$\\frac{'.$num1.'}{'.$denom1.'}\\qquad?\\qquad\\frac{'.$num2.'}{'.$denom2.'}$$';
  $options = array(0 => '>', 1 => '<', 2 => '=');
  if ($frac1 > $frac2) {
    $correct = 0;
  } elseif ($frac1 < $frac2) {
    $correct = 1;
  } else {
    $correct = 2;
  }
  $solution = '$'.$options[$correct].'$';

  return array($question, $options, $correct, $solution);
}

/* Add fractions */
function E_5_tortszamok_muveletek_osszeadas()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num1 = rand(1,2);
    $num2 = rand(1,2);
    $denom1 = rand(1,3);
    $denom2 = rand(1,3);
  } elseif ($level == 2) {
    $num1 = rand(3,5);
    $num2 = rand(3,5);
    $denom1 = rand(5,10);
    $denom2 = rand(5,10);
  } elseif ($level == 3) {
    $num1 = rand(5,10);
    $num2 = rand(5,10);
    $denom1 = rand(10,20);
    $denom2 = rand(10,20);
  }

  $frac1 = $num1/$denom1;
  $frac2 = $num2/$denom2;

  $num = $num1*$denom2 + $num2*$denom1;
  $denom = $denom1*$denom2;
  $gcd = gcd($num, $denom);
  
  if ($gcd) {
    $num /= $gcd;
    $denom /= $gcd;    
  }

  $question = 'Mennyi lesz az alábbi művelet eredménye? $$\\frac{'.$num1.'}{'.$denom1.'}+\\frac{'.$num2.'}{'.$denom2.'}$$';
  $options = 'fraction';
  $correct = $frac1 + $frac2;
  $solution = '$\\\\frac{'.$num.'}{'.$denom.'}$';

  return array($question, $options, $correct, $solution);
}

/* Calculate mixed fraction */
function E_5_tortszamok_muveletek_vegyes_tort()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $int = rand(1,3);
    $num1 = rand(1,2);
    $denom1 = rand(1,3);
  } elseif ($level == 2) {
    $int = rand(5,10);
    $num1 = rand(3,5);
    $denom1 = rand(5,10);
  } elseif ($level == 3) {
    $int = rand(10,20);
    $num1 = rand(5,10);
    $denom1 = rand(10,20);
  }

  $num2 = $int*$denom1 + $num1;
  $denom2 = $denom1;
  $gcd = gcd($num2, $denom2);
  
  if ($gcd) {
    $num2 /= $gcd;
    $denom2 /= $gcd;    
  }

  $question = 'Alakítsd át közönséges törtté!$$'.$int.'\frac{'.$num1.'}{'.$denom1.'}$$';
  $options = 'fraction';
  $correct = $num2/$denom2;
  $solution = '$\\\\frac{'.$num2.'}{'.$denom2.'}$';

  return array($question, $options, $correct, $solution);
}

/* Subtract fractions */
function E_5_tortszamok_muveletek_kivonas()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num1 = rand(1,2);
    $num2 = rand(1,2);
    $denom1 = rand(1,3);
    $denom2 = rand(1,3);
  } elseif ($level == 2) {
    $num1 = rand(3,5);
    $num2 = rand(3,5);
    $denom1 = rand(5,10);
    $denom2 = rand(5,10);
  } elseif ($level == 3) {
    $num1 = rand(5,10);
    $num2 = rand(5,10);
    $denom1 = rand(10,20);
    $denom2 = rand(10,20);
  }

  $frac1 = $num1/$denom1;
  $frac2 = $num2/$denom2;

  $num = $num1*$denom2 - $num2*$denom1;
  $denom = $denom1*$denom2;
  $gcd = gcd($num, $denom);
  
  if ($gcd) {
    $num /= $gcd;
    $denom /= $gcd;    
  }

  $question = 'Mennyi lesz az alábbi művelet eredménye? $$\\frac{'.$num1.'}{'.$denom1.'}-\\frac{'.$num2.'}{'.$denom2.'}$$';
  $options = 'fraction';
  $correct = $frac1 - $frac2;
  $solution = '$\\\\frac{'.$num.'}{'.$denom.'}$';

  return array($question, $options, $correct, $solution);
}

/* Multiply fractions */
function E_5_tortszamok_muveletek_szorzas()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num1 = rand(1,2);
    $num2 = rand(1,2);
    $denom1 = rand(1,3);
    $denom2 = rand(1,3);
  } elseif ($level == 2) {
    $num1 = rand(3,5);
    $num2 = rand(3,5);
    $denom1 = rand(5,10);
    $denom2 = rand(5,10);
  } elseif ($level == 3) {
    $num1 = rand(5,10);
    $num2 = rand(5,10);
    $denom1 = rand(10,20);
    $denom2 = rand(10,20);
  }

  $frac1 = $num1/$denom1;
  $frac2 = $num2/$denom2;

  $num = $num1*$num2;
  $denom = $denom1*$denom2;
  $gcd = gcd($num, $denom);
  
  if ($gcd) {
    $num /= $gcd;
    $denom /= $gcd;    
  }

  $question = 'Mennyi lesz az alábbi művelet eredménye? $$\\frac{'.$num1.'}{'.$denom1.'}\\cdot\\frac{'.$num2.'}{'.$denom2.'}$$';
  $options = 'fraction';
  $correct = $frac1 * $frac2;
  $solution = '$\\\\frac{'.$num.'}{'.$denom.'}$';

  return array($question, $options, $correct, $solution);
}

/* Divide fractions */
function E_5_tortszamok_muveletek_osztas()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $num1 = rand(1,2);
    $num2 = rand(1,2);
    $denom1 = rand(1,3);
    $denom2 = rand(1,3);
  } elseif ($level == 2) {
    $num1 = rand(3,5);
    $num2 = rand(3,5);
    $denom1 = rand(5,10);
    $denom2 = rand(5,10);
  } elseif ($level == 3) {
    $num1 = rand(5,10);
    $num2 = rand(5,10);
    $denom1 = rand(10,20);
    $denom2 = rand(10,20);
  }

  $frac1 = $num1/$denom1;
  $frac2 = $num2/$denom2;

  $num = $num1*$denom2;
  $denom = $denom1*$num2;
  $gcd = gcd($num, $denom);
  
  if ($gcd) {
    $num /= $gcd;
    $denom /= $gcd;    
  }

  $question = 'Mennyi lesz az alábbi művelet eredménye? $$\\frac{'.$num1.'}{'.$denom1.'}:\\frac{'.$num2.'}{'.$denom2.'}$$';
  $options = 'fraction';
  $correct = $frac1 / $frac2;
  $solution = '$\\\\frac{'.$num.'}{'.$denom.'}$';

  return array($question, $options, $correct, $solution);
}

/* Calculate weight of iceberg */
function E_5_tortszamok_muveletek_jeghegy()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];

  $part = rand(5,15); // weight of 1/9 iceberg;
  $over = $part;
  $under = 8*$part;
  $difference = $under - $over;
  $whole = 9*$part;

  $question = 'A jéghegyeknek csak $1/9$ része van a vízfelszín felett. ';
  if ($level == 1) {
    $question .= 'Hány tonnás az a jéghegy, amelynek víz feletti része $'.$over.'$ tonna tömegű? ';
  } elseif ($level == 2) {
    $question .= 'Hány tonnás az a jéghegy, amelynek víz alatti része $'.$under.'$ tonna tömegű? ';
  } elseif ($level == 3) {
    $question .= 'Hány tonnás az a jéghegy, amelynek víz alatti része $'.$difference.'$ tonnával nehezebb, mint a víz feletti része? ';
  }

  $question .= '<i>(</i><a href="#popover_jeghegy" data-toggle="popover" data-trigger="focus" data-content="6 osztályos gimnáziumi felvételi feladatsor (2001. október 8.) 1. feladat"><i>Forrás</i></a><i>)</i><div class="text-center"><img class="img-question" width="90%" src="'.$utvonal.'/jeghegy.jpg"></div><p class="text-center small image-description"> <a target="_blank" href="https://www.flickr.com/photos/pere/523019984">Forrás</a></p>';

  $options = '';
  $correct = $whole;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}


























/*********************************    6. OSZTALY    ********************************/







/********** LOGIKA **********/





/*** logika ***/

/* Define winner of race */
function E_6_logika_logika_verseny()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];

  if ($level == 1) {
    $no_of_people = 3;
  } elseif ($level == 2) {
    $no_of_people = 4;
  } elseif ($level == 3) {
    $no_of_people = 5;
  }

  $names_all = array('Aladár', 'Béla', 'Cili', 'Dénes', 'Endre');
  $wrong = array('ketten', 'hárman', 'négyen');

  $names = array_slice($names_all, 0, $no_of_people);

  $names_text1 = implode(', ', array_slice($names, 0, $no_of_people-1));
  $names_text2 = $names[count($names) - 1];

  $solution = E_6_logika_logika_verseny_generateSolution($no_of_people);

  $question = 'Egy versenyről '.$names_text1.' és '.$names_text2.' így számolt be:';

  $statements = E_6_logika_logika_verseny_generateQuestion($names, $solution);
  $question .= $statements;
  $question .= 'Tudjuk, hogy csak egyikük mondott igazat, '.$wrong[$no_of_people-3].' hamisat állítottak. Ki lett a győztes ezen a versenyen, ha tudjuk, hogy nem volt holtverseny?<div class="text-center"><img class="img-question" width="40%" src="'.$utvonal.'/kupa.png"></div>';

  $options = $names;
  $correct = $solution['winner']-1;
  $solution = $names[$correct];

  return array($question, $options, $correct, $solution);
}

/**
 * Generate solution for race exercise
 *
 * Number of people (n) is having a race. After the race n statements are given 
 * regarding the winner, among which 1 is true and the rest is false. Generate a
 * possible solution which is possible to solve.
 *
 * @param int $n Number of people.
 *
 * @return array $solution Solution for race (statements of participants).
 */
function E_6_logika_logika_verseny_generateSolution($n)
{
  $people = range(1, $n);
  $options = array(TRUE, FALSE);

  $names_all = combos($people, $n); // List of people appearing in statements.
  $statements_all = combos($options, $n); // Whether people won or not.

  shuffle($names_all);
  shuffle($statements_all);

  foreach ($names_all as $names) {
    foreach ($statements_all as $statements) {

      if (E_6_logika_logika_verseny_checkStatements($names, $statements)) {

        $hasSolution = FALSE;
        foreach ($people as $true) { // Who was right?

          $winner = E_6_logika_logika_verseny_checkSolution($names, $statements, $true);
          if ($winner) {

            if ($hasSolution) { // multiple solutions!

              break;

            } else {

              $solution['names'] = $names; 
              $solution['statements'] = $statements; 
              $solution['true'] = $true;
              $solution['winner'] = $winner;

              $hasSolution = TRUE;


            }
          }
        }

        if ($hasSolution) {

          return $solution; 

        }
      }
    }
  }

  return NULL;
}

/**
 * Check statements for race exercise
 *
 * @param array $names      List of people appearing in statements.
 * @param array $statements Whether people won or not.
 *
 * @return bool $unique Whether statements are unique (has no duplication).
 */
function E_6_logika_logika_verseny_checkStatements($names, $statements)
{

  foreach ($names as $key => $value) {
    if (isset($target[$value][(int)$statements[$key]])) { // multiple statement!!!
      return FALSE;
    } else {
      $target[$value][(int)$statements[$key]] = 1;
    }
  }

  return TRUE;
}

/**
 * Check solution for race exercise
 *
 * @param array $names      List of people appearing in statements.
 * @param array $statements Whether people won or not.
 * @param int   $true       Index of true statement.
 *
 * @return bool $winner Index of winner (NULL if there is no unique solution)
 */
function E_6_logika_logika_verseny_checkSolution($names, $statements, $true)
{

  foreach ($statements as $key => $value) { // Invert wrong statements
    if ($key+1 != $true) {
      $statements[$key] = !$value;
    }
  }

  $winner = FALSE;
  $solution = array();

  foreach ($names as $key => $value) {
    if (!isset($solution[$value])) { // person not checked yet

      if ($statements[$key]) { // person won
        if ($winner) { // there is already a winner
          return FALSE;
        }
        $winner = $value;
      }
      $solution[$value] = $statements[$key];

    } else { // person already checked


      if ($solution[$value] != $statements[$key]) { // contradiction!
        return FALSE;
      }
    }
  }

  if (!$winner && count($solution) == (count($names)-1)) { // missing person is the winner
    $people = range(1, count($names));
    foreach (array_keys($solution) as $person) {
      unset($people[$person-1]);
    }
    if (count($people) == 1) {
      $winner = array_values($people)[0]; 
    } else {
      die('No winner found!!!');
    }
  }

  return $winner;
}

/**
 * Check solution for race exercise
 *
 * @param array $names    List of names
 * @param array $solution Solution
 *
 * @return string $statements Statements of people.
 */
function E_6_logika_logika_verseny_generateQuestion($names, $solution)
{

  $statements = '';
  $statements .= '<table style="margin:15px">';

  for ($i=0; $i<count($names); $i++) {
    
    $statements .= '<tr><td><b>'.$names[$i].'</b>:&nbsp;&nbsp;</td>';
    $name = $names[$solution['names'][$i]-1];

    if ($i == $solution['names'][$i]-1) { // statement about self
      if ($solution['statements'][$i]) {
        $rand = rand(1, 3);
        if ($rand == 1) {
          $statements .= '<td>Én győztem.</td>';
        } elseif ($rand == 2) {
          $statements .= '<td>Én lettem a győztes.</td>';
        } elseif ($rand == 3) {
          $statements .= '<td>Én nyertem meg a versenyt.</td>';
        }
      } else {
        $rand = rand(1, 3);
        if ($rand == 1) {
          $statements .= '<td>Nem én győztem.</td>';
        } elseif ($rand == 2) {
          $statements .= '<td>Sajnos nem én lettem a győztes.</td>';
        } elseif ($rand == 3) {
          $statements .= '<td>Nem én nyertem meg a versenyt.</td>';
        }
      }
    } else {
      if ($solution['statements'][$i]) {
        $rand = rand(1, 3);
        if ($rand == 1) {
          $statements .= '<td>A győztes '.$name.' lett.</td>';
        } elseif ($rand == 2) {
          $statements .= '<td>A versenyt '.$name.' nyerte meg.</td>';
        } elseif ($rand == 3) {
          $statements .= '<td>'.$name.' lett az első.</td>';
        }
      } else {
        $rand = rand(1, 3);
        if ($rand == 1) {
          $statements .= '<td>Nem '.$name.' győzött.</td>';
        } elseif ($rand == 2) {
          $statements .= '<td>A nyertes nem '.$name.' lett.</td>';
        } elseif ($rand == 3) {
          $statements .= '<td>A versenyt nem '.$name.' nyerte meg.</td>';
        }
      }
    }
    $statements .= '</tr>';
  }

  $statements .= '</table>';

  return $statements;
}
















/********************************    12. OSZTALY    ********************************/





/********** SOROZATOK **********/






/*** Sorozatok ***/

/* Define member of number sequence by explicit formula */
function E_12_sorozatok_sorozatok_explicit()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $a0 = rand(1,10);
    $a1 = rand(1,5);
    $a2 = 0;
    $a3 = 0;
    $formula =  ' '.$a1.'\\cdot n+ '.$a0;
    $index = rand(6,8);
  } elseif ($level == 2) {
    $a0 = pow(-1,rand(1,2))*rand(1,15);
    $a1 = pow(-1,rand(1,2))*rand(1,10);
    $a2 = pow(-1,rand(1,2))*rand(1,5);
    $a3 = 0;
    $formula =  ' '.$a2.'\\cdot n^2+ '.$a1.'\\cdot n+ '.$a0;
    $index = rand(12,20);
  } elseif ($level == 3) {
    $a0 = pow(-1,rand(1,2))*rand(1,20);
    $a1 = pow(-1,rand(1,2))*rand(1,15);
    $a2 = pow(-1,rand(1,2))*rand(1,10);
    $a3 = pow(-1,rand(1,2))*rand(1,5);
    $formula =  ' '.$a3.'\\cdot n^3+ '.$a2.'\\cdot n^2+ '.$a1.'\\cdot n+ '.$a0;
    $index = rand(21,30);
  }

  $formula = str_replace(' 1\\cdot ', ' ', $formula);
  $formula = str_replace(' -1\\cdot ', ' -', $formula);
  $formula = str_replace('+ -', '-', $formula);
  $formula2 = str_replace('n', $index, $formula);
  $formula2 = str_replace('\\', '\\\\', $formula2);

  $correct = $a0 + $a1*$index + $a2*pow($index,2) + $a3*pow($index,3);
  if ($correct > 9999) {
    $correct = number_format($correct,0,',','\,');
  }

  $solution = '$'.$correct.'$ (mert $'.$formula2.'='.$correct.'$)';
  $question = 'Egy sorozat képzési szabálya a következő:$$a_n='.$formula.'$$'
    .'Mekkora $a_{'.$index.'}$ értéke?';
  $options = '';

  return array($question, $options, $correct, $solution);
}

/* Define member of number sequence by recursive formula */
function E_12_sorozatok_sorozatok_rekurziv()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $a1 = pow(-1,rand(1,2))*rand(1,4); // initial value of a1
    $a2 = 0; // initial value of a2
    $coeff_an_1 = pow(-1,rand(1,2))*rand(1,3); // coefficient of a_{n-1} in recursive formula
    $coeff_an_2 = 0; // coefficient of a_{n-1} in recursive formula
    $coeff_n = 0; // coefficient of n in recursive formula
    $pow_n = 0; // power of n in recursive formula
    $coeff_a0 = pow(-1,rand(1,2))*rand(1,5); // constant term in recursive formula
    $index = 2; // index of member we are looking for
    $formula =  ' '.$coeff_an_1.'\\cdot a_{n-1}'
                .'+ '.$coeff_a0; // recursive formula
    $init = '$a_1='.$a1.'$'; // initial values
  } elseif ($level == 2) {
    $a1 = pow(-1,rand(1,2))*rand(5,10);
    $a2 = 0;
    $coeff_an_1 = pow(-1,rand(1,2))*rand(4,7);
    $coeff_an_2 = 0;
    $coeff_n = pow(-1,rand(1,2))*rand(0,2);
    $pow_n = rand(1,3);
    $coeff_a0 = pow(-1,rand(1,2))*rand(5,10);
    $index = rand(2,3);
    $formula =  ' '.$coeff_an_1.'\\cdot a_{n-1}'
                .'+ '.$coeff_n.'\\cdot n^'.$pow_n
                .'+ '.$coeff_a0;
    $init = '$a_1='.$a1.'$';
  } elseif ($level == 3) {
    $a1 = pow(-1,rand(1,2))*rand(2,4);
    $a2 = pow(-1,rand(1,2))*rand(1,3);
    $coeff_an_1 = pow(-1,rand(1,2))*rand(4,7);
    $coeff_an_2 = pow(-1,rand(1,2))*rand(2,5);
    $coeff_n = pow(-1,rand(1,2))*rand(2,5);
    $pow_n = rand(1,5);
    $coeff_a0 = pow(-1,rand(1,2))*rand(5,10);
    $index = rand(3,5);
    $formula =  ' '.$coeff_an_1.'\\cdot a_{n-1}'
                .'+ '.$coeff_an_2.'\\cdot a_{n-2}'
                .'+ '.$coeff_n.'\\cdot n^'.$pow_n
                .'+ '.$coeff_a0;
    $init = '$a_1='.$a1.'$ és $a_2='.$a2.'$';
  }

  $formula = str_replace(' 1\\cdot ', '', $formula);
  $formula = str_replace(' -1\\cdot ', ' -', $formula);
  $formula = str_replace('+ -', '-', $formula);
  $formula = str_replace('^1+', '+', $formula);
  $formula2 = str_replace('\\', '\\\\', $formula);

  $correct = recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index);
  if ($correct > 9999) {
    $correct = number_format($correct,0,',','\,');
  }

  $an_1 = recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index-1);
  if ($index > 2 && $coeff_an_2 != 0) {
    $an_2 = recursiveSeries($a1, $a2, $coeff_an_1, $coeff_an_2, $coeff_n, $pow_n, $coeff_a0, $index-2);
    if ($an_2 > 0) {
      $formula2 = str_replace('a_{n-2}', $an_2, $formula2);
    } else {
      $formula2 = str_replace('a_{n-2}', '('.$an_2.')', $formula2);
    }
  }

  if ($an_1 > 0) {
    $formula2 = str_replace('a_{n-1}', $an_1, $formula2);
  } else {
    $formula2 = str_replace('a_{n-1}', '('.$an_1.')', $formula2);
  }
  $formula2 = str_replace('n', $index, $formula2);

  $solution = '$'.$correct.'$ (mert $'.$formula2.'='.$correct.'$)';
  $question = 'Egy sorozat képzési szabálya a következő:$$a_n='.$formula.'$$'
    .'Ha tudjuk, hogy '.$init.', akkor mekkora $a_{'.$index.'}$ értéke?';

  $options = '';

  return array($question, $options, $correct, $solution);
}

/* Define difference of number sequence */
function E_12_sorozatok_szamtani_sorozatok_differencia()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(-3,3);
    $ind1 = 1;
    $ind2 = 2;
    $a1 = rand(1,5);
  } elseif ($level == 2) {
    $d = rand(-10, 10);
    $ind1 = rand(1,5);
    $ind2 = $ind1 + rand(1,5);
    $a1 = rand(-10, 10);
  } elseif ($level == 3) {
    $d = rand(-20, 20);
    $ind1 = rand(1,10);
    $ind2 = $ind1 + rand(5,20);
    $a1 = rand(-20, 20);
  }

  $a2 = $a1 + ($ind2 - $ind1) * $d;

  $question = 'Egy sorozatban $a_{'.$ind1.'}='.$a1.'$, és $a_{'.$ind2.'}='.$a2.'$. Mekkora a $d$ értéke?';
  $options = '';
  $correct = $d;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}
/* Define member of number sequence by member and difference */
function E_12_sorozatok_szamtani_sorozatok_tag_1()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(-3,3);
    $ind1 = 1;
    $ind2 = 2;
    $a1 = rand(1,5);
  } elseif ($level == 2) {
    $d = rand(-10, 10);
    $ind1 = rand(1,5);
    $ind2 = $ind1 + rand(2,5);
    $a1 = rand(-10, 10);
  } elseif ($level == 3) {
    $d = rand(-20, 20);
    $ind1 = rand(1,10);
    $ind2 = $ind1 + rand(5,20);
    $a1 = rand(-20, 20);
  }

  $a2 = $a1 + ($ind2 - $ind1) * $d;

  $question = 'Egy sorozatban $a_{'.$ind1.'}='.$a1.'$, és $d='.$d.'$. Mekkora az $a_{'.$ind2.'}$ értéke?';
  $options = '';
  $correct = $a2;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define member of number sequence by two members */
function E_12_sorozatok_szamtani_sorozatok_tag_2()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(-3, 3);
    $a1 = rand(1, 5);
    $ind1 = 1;
    $ind2 = 2;
    $ind3 = 3;
  } elseif ($level == 2) {
    $d = rand(-10, 10);
    $a1 = rand(-10, 10);
    $indexes = range(1, 10);
  } elseif ($level == 3) {
    $d = rand(-20, 20);
    $a1 = rand(-20, 20);
    $indexes = range(1, 20);
  }

  if ($level > 1) {
    shuffle($indexes);

    $ind1 = $indexes[0];
    $ind2 = $indexes[1];
    $ind3 = $indexes[2];
  }

  $a2 = $a1 + ($ind2 - $ind1) * $d;
  $a3 = $a1 + ($ind3 - $ind1) * $d;

  $question = 'Egy sorozatban $a_{'.$ind1.'}='.$a1.'$, és $a_{'.$ind2.'}='.$a2.'$. Mekkora az $a_{'.$ind3.'}$ értéke?';
  $options = '';
  $correct = $a3;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define sum of number sequence by member and difference */
function E_12_sorozatok_szamtani_sorozatok_osszeg_1()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(-3, 3);
    $a_i = rand(1, 5);
    $i = 1;
    $j = rand(3,5);
  } elseif ($level == 2) {
    $d = rand(-10, 10);
    $a_i = rand(-10, 10);
    $indexes = range(1, 10);
  } elseif ($level == 3) {
    $d = rand(-20, 20);
    $a_i = rand(-20, 20);
    $indexes = range(1, 10);
  }

  if ($level > 1) {
    shuffle($indexes);

    $i = $indexes[0];
    $j = $indexes[1];
  }

  $a1 = $a_i - ($i-1) * $d;
  $S = (( 2*$a1 + ($j-1) * $d ) * $j) / 2;

  $question = 'Egy sorozatban $a_{'.$i.'}='.$a_i.'$, és $d='.$d.'$. Mekkora az $S_{'.$j.'}$ értéke?';
  $options = '';
  $correct = $S;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define sum of number sequence by 2 members */
function E_12_sorozatok_szamtani_sorozatok_osszeg_2()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(-3, 3);
    $a_i = rand(1, 5);
    $i = 1;
    $j = rand(3,5);
    $k = $j;
  } elseif ($level == 2) {
    $d = rand(-10, 10);
    $a_i = rand(-10, 10);
    $indexes = range(1, 10);
  } elseif ($level == 3) {
    $d = rand(-20, 20);
    $a_i = rand(-20, 20);
    $indexes = range(1, 20);
  }

  if ($level > 1) {
    shuffle($indexes);

    $i = $indexes[0];
    $j = $indexes[1];
    $k = $indexes[2];
  }

  $a_j = $a_i + ($j-$i) * $d;
  $a_1 = $a_i - ($i-1) * $d;
  $S_k = (( 2*$a_1 + ($k-1) * $d ) * $k) / 2;

  $question = 'Egy sorozatban $a_{'.$i.'}='.$a_i.'$, és $a_{'.$j.'}='.$a_j.'$. Mekkora az $S_{'.$k.'}$ értéke?';
  $options = '';
  $correct = $S_k;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define order of member of number sequence by some members */
function E_12_sorozatok_szamtani_sorozatok_hanyadik()
{
  $level = $_SESSION['level'];

  $sgn = pow(-1, rand(1, 2));

  if ($level == 1) {
    $d = $sgn * rand(1, 3);
    $a1 = rand(1, 5);
    $i = rand(5,7);
  } elseif ($level == 2) {
    $d = $sgn * rand(1, 10);
    $a1 = rand(-10, 10);
    $i = rand(7, 10);
  } elseif ($level == 3) {
    $d = $sgn * rand(1, 20);
    $a1 = rand(-20, 20);
    $i = rand(10, 20);
  }

  $a2 = $a1 + $d;
  $a3 = $a2 + $d;
  $ai = $a1 + ($i-1) * $d;

  $article = addArticle($ai);
  $question = 'Tekintsük az alábbi számtani sorozatot:$$'.$a1.','.$a2.','.$a3.',\ldots$$Hányadik tagja ennek a sorozatnak '.$article.' $'.$ai.'$?';
  $options = '';
  $correct = $i;
  $solution = '$'.$correct.'.$';

  return array($question, $options, $correct, $solution);
}

/* Define sum by member, length and difference */
function E_12_sorozatok_szamtani_sorozatok_repa()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];

  if ($level == 1) {
    $d = rand(1, 3);
    $a1 = rand(1, 5);
    $n = rand(3,6);
    $i = 1;
  } elseif ($level == 2) {
    $d = rand(1, 10);
    $a1 = rand(1, 10);
    $n = rand(7, 10);
    $i = rand(1,$n);
  } elseif ($level == 3) {
    $d = rand(1, 20);
    $a1 = rand(1, 20);
    $n = rand(10, 20);
    $i = rand(1,$n);
  }

  $ai = $a1 + ($i-1) * $d;
  $Sn = (( 2*$a1 + ($n-1) * $d ) * $n) / 2;

  $suffix = addSuffixWith($d);
  $article = addArticle($ai);

  $question = 'Nagymama kertjében $'.$n.'$ sor répa van. Minden sorban $'.$d.'$-'.$suffix.' több répa van, mint az előzőben. Hány répa van összesen nagymama kertjében, ha tudjuk, hogy '.$article.' $'.$i.'$. sorban $'.$ai.'$ darab répa van?<div class="text-center"><img class="img-question" width="70%" src="'.$utvonal.'/repa.jpg"></div>';
  $options = '';
  $correct = $Sn;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define number of members by sum, member and difference */
function E_12_sorozatok_szamtani_sorozatok_hany_tag()
{
  $level = $_SESSION['level'];

  if ($level == 1) {
    $d = rand(1, 3);
    $a1 = rand(1, 5);
    $n = rand(3,6);
    $i = 1;
  } elseif ($level == 2) {
    $d = rand(1, 10);
    $a1 = rand(1, 10);
    $n = rand(7, 10);
    $i = rand(1,$n);
  } elseif ($level == 3) {
    $d = rand(1, 20);
    $a1 = rand(1, 20);
    $n = rand(10, 20);
    $i = rand(1,$n);
  }

  $ai = $a1 + ($i-1) * $d;
  $Sn = (( 2*$a1 + ($n-1) * $d ) * $n) / 2;

  $question = 'Egy számtani sorozatban $a_{'.$i.'}='.$ai.'$, és $d='.$d.'$. Az első néhány tagot összeadtuk, az eredmény $'.$Sn.'$ lett. Hány tagot adtunk össze?';
  $options = '';
  $correct = $n;
  $solution = '$'.$correct.'$';

  return array($question, $options, $correct, $solution);
}

/* Define last member of sum by sum, member and difference */
function E_12_sorozatok_szamtani_sorozatok_drazse()
{
  $level = $_SESSION['level'];
  $utvonal = $_SESSION['utvonal'];

  if ($level == 1) {
    $d = rand(1, 3);
    $a1 = rand(1, 5);
    $n = rand(3,6);
    $i = 1;
  } elseif ($level == 2) {
    $d = rand(1, 10);
    $a1 = rand(1, 10);
    $n = rand(7, 10);
    $i = rand(1,$n);
  } elseif ($level == 3) {
    $d = rand(1, 20);
    $a1 = rand(1, 20);
    $n = rand(10, 20);
    $i = rand(1,$n);
  }

  $ai = $a1 + ($i-1) * $d;
  $an = $a1 + ($n-1) * $d;
  $Sn = (( 2*$a1 + ($n-1) * $d ) * $n) / 2;

  $question = 'Harry Potter egy nap észrevette, hogy $'.$Sn.'$ db Bogoly Berti-féle mindenízű drazséjának nyoma veszett. Némi nyomozás után rájött, hogy legjobb barátja, Ron Weasley volt a tettes, aki végül beismerte, hogy egy ideje minden nap elcsent néhány szem drazsét. Arra nem emlékezett pontosan, hogy mikor kezdte, de azt tudta, hogy a kezdéstől számított $'.$i.'$. napon $'.$ai.'$ db-ot lopott, és minden nap $'.$d.'$ darabbal többet lopott, mint az előző nap. Hány drazsét lopott Ron Weasley az utolsó napon?<div class="text-center"><img class="img-question" width="50%" src="'.$utvonal.'/drazse.png"></div>';
  $options = '';
  $correct = $an;
  $solution = '$'.$correct.'$';


  return array($question, $options, $correct, $solution);
}

?>