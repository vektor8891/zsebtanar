<?php

// $nehezseg = 3;
// require('d:\xampp\htdocs\zsebtanar\fuggvenyek.php');

if ($nehezseg == 1) {
	$hossz = rand(1,2);
	$helyiertek = rand(1,$hossz+1);
} elseif ($nehezseg == 2) {
	$hossz = rand(3,6);
	$helyiertek = rand(3,$hossz+1);
} elseif ($nehezseg == 3) {
	$hossz = rand(7,10);
	$helyiertek = rand(6,min($hossz+1,10));
}

$helyiertekek = array("egyesekre","tízesekre","százasokra","ezresekre","tízezresekre","százezresekre","milliósokra","tízmilliósokra","százmilliósokra","milliárdosokra");

$szam = randGenerate($hossz,10);
$helyes = floordecerr($szam,$helyiertek);

function floordecerr($szam,$decimals=2){
	$szamkerek = floor($szam/pow(10,$decimals-1))*pow(10,$decimals-1); 
    return abs($szam-$szamkerek);
}

$opciok = '';

if ($szam > 9999) { $szam = number_format($szam,0,',','\,');}

$question = 'Mekkora lesz a kerekítési hiba, ha az alábbi számot '.$helyiertekek[$helyiertek-1].' kerekítjük lefelé?$$'.$szam.'$$';

if ($helyes > 9999) {
	$megoldas = '$'.number_format($helyes,0,',','\\\\,').'$';
} else {
	$megoldas = '$'.$helyes.'$';
}

// print_r($question);
// print_r($megoldas);
// print_r($opciok);
// print_r($helyes);

?>