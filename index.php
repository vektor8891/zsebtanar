<?php
/**
 * Index page
 *
 * PHP Version 5
 *
 * @category PHP
 * @package  Zsebtanár
 * @author   Viktor Szabó <zsebtanar@gmail.com>
 * @license  http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
 * @link	 http://zsebtanar.hu/
 */

session_start();

require 'functions_math.php';
require 'functions_main.php';
require_once 'eszkozok/php/PHPExcel/PHPExcel.php';
require_once 'eszkozok/php/captcha/simple-php-captcha.php';

connectDatabase('../database_old.txt');

// Search
if (isset($_REQUEST['search'])) {
	Search();
}

// Log in user
if (!isset($_SESSION['logged_in']) || !$_SESSION['logged_in']) {
	if (isset($_REQUEST['logged_in']) && $_REQUEST['logged_in'] == TRUE) {
		$_SESSION['logged_in'] = TRUE;
	} else {
		$_SESSION['logged_in'] = FALSE;
	}   
}

// Write statistics
if (isset($_REQUEST['write']) && $_REQUEST['write'] == TRUE) {
	writeStatistics('statistics.xlsx');
	$_REQUEST['write'] = FALSE;
}


// ****************************** Generate HTML ***********************************//

require 'header.html';

include_once('analyticstracking.php');

printNavBar();

if (isset($_REQUEST['send'])) {

	// Send
	printSendPage();

} elseif (isset($_REQUEST['p'])) {

	$id = $_REQUEST['p'];
	$subtopic = getSubtopics('id = '.$id);

	if (count($subtopic) == 1) {

		printJumbotron($id);

		echo '<div class="container">';

		printPageNavButtonsTop($subtopic[0]);

		printSendNewButton($id);

		$questions = getQuestions('subtopicID = '.$id);

		if (!isset($_REQUEST["f"]) && count($questions) > 0) {

			// Print questions
			printQuestions($subtopic[0]);

		} else {

			// Print exercises
			printExercises($subtopic[0]);

		}

		echo '</div>';

	} else {
		printMainPage();
	}
} else {
	printMainPage();
}

require 'footer.html';
































/*********************************  FUNCTIONS *********************************/

















/*******************   GET  - get data (e.g. from database)   **********************/

/**
 * Get classes from database
 *
 * @param string $cond Condition.
 *
 * @return array $classes Class data.
 */
function getClasses($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = "SELECT * FROM classes WHERE ".$cond;
	} else {
		$sql = "SELECT * FROM classes";
	}

	$res = $conn->query($sql) or die ('Failed to select classes.');

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$classes[$i] = $row;
			$i++;
		}
	}

	return $classes;
}

/**
 * Get exercises
 *
 * @param string $cond Condition.
 *
 * @return array $exercises Exercises data.
 */
function getExercises($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = "SELECT * FROM exercises WHERE ".$cond;
	} else {
		$sql = "SELECT * FROM exercises";
	}

	$res = $conn->query($sql) or die ('Failed to select exercises.');

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$exercises[$i] = $row;
			$i++;
		}
	} else {
		return NULL;
	}

	return $exercises;
}

/**
 * Print linked data to exercise.
 *
 * @param array  $exercise Exercise data.
 * @param string $type   Link type ('to' or 'from')
 *
 * @return array $links Link data
 */
function getLinksToExercise($exercise, $type)
{

	if ($type == 'to') {

		$class = getClasses('id = "'.$exercise["classID"].'"');
		$topic = getTopics('id = "'.$exercise["topicID"].'"');
		$subtopic = getSubtopics('id = "'.$exercise["subtopicID"].'"');

		$links = getLinks(
			'link_class = \''.$class[0]["name"].'\' AND '
			.'link_topic = \''.$topic[0]["name"].'\' AND '
			.'link_subtopic = \''.$subtopic[0]["name"].'\' AND '
			.'link_label = \''.$exercise["label"].'\'');

	} elseif ($type == 'from') {

		$links = getLinks('id = '.$exercise["id"]);

	}

	return $links;
}

/**
 * Print linked data to exercise.
 *
 * @param array  $link Link data.
 * @param string $type Link type ('to' or 'from').
 *
 * @return array $exercise Exercise data.
 */
function getExerciseFromLink($link, $type)
{

	if ($type == 'from') {

		$class = getClasses('name = "'.$link["link_class"].'"');
		$topic = getTopics('name = "'.$link["link_topic"].'" AND classID = "'.$class[0]["id"].'"');
		$subtopic = getSubtopics('name = "'.$link["link_subtopic"].'" AND topicID = "'.$topic[0]["id"].'"');
		$exercise = getExercises('subtopicID = "'.$subtopic[0]["id"].'" AND label = "'.$link["link_label"].'"');

	}

	if ($type == 'to') {

		$exercise = getExercises('id = "'.$link["id"].'"');

	}

	return $exercise;
}

/**
 * Get links
 *
 * @param string $cond Condition.
 *
 * @return array $links Link data.
 */
function getLinks($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = 'SELECT * FROM links WHERE '.$cond;
	} else {
		$sql = 'SELECT * FROM links';
	}
	
	$res = $conn->query($sql) or die ('Failed to select links.'.$sql);

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$links[$i] = $row;
			$i++;
		}
	} else {
		return NULL;
	}

	return $links;
}

/**
 * Get questions
 *
 * @param string $cond Condition.
 *
 * @return array $questions Question data.
 */
function getQuestions($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = 'SELECT * FROM questions WHERE '.$cond;
	} else {
		$sql = 'SELECT * FROM questions';
	}
	
	$res = $conn->query($sql) or die ('Failed to select questions.'.$sql);

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$questions[$i] = $row;
			$i++;
		}
	} else {
		return NULL;
	}

	return $questions;
}

/**
 * Get quiz
 *
 * @param int $id Subtopic id.
 *
 * @return object $res Result of query.
 */
function getQuiz($id=NULL)
{
	$conn = $_SESSION['conn'];
	if ($id) {
		$sql = "SELECT * FROM quiz WHERE subtopicID = ".$id;
	} else {
		$sql = "SELECT * FROM quiz";
	}
	$res = $conn->query($sql) or die ('Failed to select quiz questions.');
	return $res;
}

/**
 * Get topics
 *
 * @param string $cond Condition.
 *
 * @return array $topics Topic data.
 */
function getTopics($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = 'SELECT * FROM topics WHERE '.$cond;
	} else {
		$sql = 'SELECT * FROM topics';
	}
	
	$res = $conn->query($sql) or die ('Failed to select topics.'.$sql);

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$topics[$i] = $row;
			$i++;
		}
	} else {
		return NULL;
	}

	return $topics;
}

/**
 * Get subtopics
 *
 * @param string $cond Condition.
 *
 * @return array $subtopics Topic data.
 */
function getSubtopics($cond=NULL)
{
	$conn = $_SESSION['conn'];

	if ($cond) {
		$sql = 'SELECT * FROM subtopics WHERE '.$cond;
	} else {
		$sql = 'SELECT * FROM subtopics';
	}
	
	$res = $conn->query($sql) or die ('Failed to select subtopics.'.$sql);

	$i = 0;
	if ($res->num_rows > 0) {
		while($row = $res->fetch_assoc()) {
			$subtopics[$i] = $row;
			$i++;
		}
	} else {
		return NULL;
	}

	return $subtopics;
}

/**
 * Get videos
 *
 * @return object $res Result of query.
 */
function getVideos()
{
	$conn = $_SESSION['conn'];
	$sql = "SELECT * FROM exercises WHERE youtube <> ''";
	$res = $conn->query($sql) or die ('Failed to select exercises.');
	return $res;
}



























/************************** PRINT - create html data *************************/

/**
 * Print exercises menu and text
 *
 * @param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printExercises($subtopic)
{
	$id = $subtopic["id"];
	$exercises = getExercises('id = "'.$id.'"');

	if (count($exercises) > 0) {
		if ($subtopic["type"] != 'video') {?>

			<div class="row">
				<div class="col-md-1"></div>
				<nav class="col-md-4 navbar" id="leftCol"><?php

					printExercisesMenu($id);

				?></nav>
				<div class="col-md-6"><?php

					printExercisesText($subtopic);

				?></div>
				<div class="col-md-1"></div>
			</div><?php

			printQuiz($id);

		} else {?>

			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<div class="anchor"></div><?php

						printExercisesText($subtopic);

					?></div>
				<div class="col-md-4"></div>
			</div><?php

		}

	} else {?>

		<div class="row">
			<div class="col-md-12 text-center">
				<p>Ehhez a témakörhöz még nincsenek feladatok. :(</p>
				<a href="index.php?p=<?php echo $id; ?>&amp;send=exercise" class="btn btn-success">
					<span class="glyphicon glyphicon-plus"></span> Új feladat beküldése
				</a>
			</div>
		</div><?php

		printQuiz($id);

	}
	return;
}

/**
 * Print exercises
 *
 * @param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printExercisesText($subtopic)
{
	$id = $subtopic["id"];
	$type = $subtopic["type"];

	$exercises = getExercises('subtopicID = "'.$id.'"');
	$topics = getTopics('id = "'.$subtopic["topicID"].'"');
	$classes = getClasses('id = "'.$subtopic["classID"].'"');

	if (count($exercises) == 0) {

		echo '<p>Ehhez a témakörhöz még nincsenek feladatok.<p>';
		return;

	}

	echo '<h1>Feladatok</h1>';

	if ($type != 'video' && $type != 'video_full') {
		printExerciseButtonsTop($id);
	} else {

		if ($subtopic["source"] != '') {
			printExerciseSource($subtopic["source"]);
		}

		printExerciseModal();

	}

	$order = count($exercises);

	foreach ($exercises as $exercise) {

		// Print exercise
		printExercise($exercise);

		if ($order > 1 && $subtopic["type"] != 'video') {
			echo '<div class="bottom text-center">
					<img src="eszkozok/images/divider.png" width="80%">
				</div>';
		}

		$order--;

	}

	echo '<div class="bottom-last text-center"></div>';

	return;
}

/**
 * Print exercise
 *
 * @param array $exercise Exercise data.
 *
 * @return NULL
 */
function printExercise($exercise)
{

	$subtopic = getSubtopics('id = "'.$exercise["subtopicID"].'"');
	$topic = getTopics('id = "'.$exercise["topicID"].'"');
	$class = getClasses('id = "'.$exercise["classID"].'"');

	$ex_name = $exercise["label"];
	$ex_no = $exercise["ex_no"];
	$ex_label = toAscii($ex_name); 
	$type = $subtopic[0]["type"];

	if (isset($_REQUEST["f"])) {
		$_SESSION['level'] = $_REQUEST["f"];
	} else {
		$_SESSION['level'] = 1;
	}

	echo '<div id="smooth_'.$ex_label.'" class="anchor"></div>';
	echo '<div id="'.$ex_label.'" class="anchor"></div>';

	if ($type == 'video') {

		echo '<p><a href="http://www.youtube.com/watch?v='.$exercise["youtube"].'">'.$ex_no.') '.$ex_name.'</a></p>';

	} else {

		echo '<div action="" class="panel panel-default" >';
		
		$_SESSION['utvonal'] = 
			'tananyag/'.$class[0]["alt"].'/'.$topic[0]["alt"].'/'
			.$subtopic[0]["alt"].'/feladatok'.'/'.$ex_label;

		if ($type == 'video_full') {

			$question = $exercise["ex_question"];
			$answer = $exercise["ex_answer"];
			printExerciseHeader($ex_no, $question);
			printExerciseAnswer($answer);
			printExerciseLink($exercise, 'to');
			printExerciseLink($exercise, 'from');
			echo '</div>';
			printExerciseButtonsBottom($subtopic[0], $ex_no, $ex_label);

		} else {

			$function = 'E_'.$class[0]["alt"].'_'.$topic[0]["alt"].'_'.$subtopic[0]["alt"].'_'.$ex_label;
			list($question, $opciok, $helyes, $megoldas) = $function();
			printExerciseHeader($ex_no, $question);
			printExerciseBody($subtopic[0], $ex_no, $opciok, $helyes, $megoldas, $ex_label);
			printExerciseLink($exercise, 'to');
			printExerciseLink($exercise, 'from');
			echo '</div>';
			printExerciseButtonsBottom($subtopic[0], $ex_no, $ex_label);
			printExerciseHint($ex_no, $exercise);

		}
	}
}

/**
 * Print exercise body
 *
 * @param array  $subtopic Subtopic data.
 * @param int   $order Exercise order.
 * @param array  $options  Options.
 * @param int   $correct  Correct option.
 * @param string $solution Solution.
 * @param string $ex_label Exercise label.
 *
 * @return NULL
 */
function printExerciseBody($subtopic, $order, $options, $correct, $solution, $ex_label)
{
	$id = $subtopic['id'];
	$type = '';?>

	<div class="panel-body">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<form name="feladat<?php echo $order; ?>" autocomplete="off"><?php

				if (is_array($options)) {

					if (count($options) > 3) {?>

						<select name="answer" class="form-control" id="sel<?php echo $order; ?>"><?php

						foreach ($options as $key => $value) {?>
							
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option><?php

						}?>

						</select><?php

					} else {

						foreach ($options as $key => $value) {?>
							
							<div class="radio">
								<label>
									<input type="radio" name="answer" value="<?php echo $key; ?>"><?php echo $value; ?>
								</label>
							</div><?php

						}
					}

				} elseif ($options=='fraction') {?>

					<table align="center" class="answer_fraction">
						<tbody>
							<tr>
								<td align="center">
									<input class="smallInput" name="numerator" data-autosize-input='{ "space": 20 }' />
								</td>
							</tr>
							<tr style="background-color: black; height: 2px;">
								<td align="center">

								</td>
							</tr>
							<tr>
								<td align="center">
									<input class="smallInput" name="denominator" data-autosize-input='{ "space": 20 }' />
								</td>
							</tr>
						</tbody>
					</table><?php

					$type = $options;

				} else {?>

					<input type="text" class="form-control" name="answer"><?php

				}?>
				
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="text-center">
			<p class="hibauzenet" id="error<?php echo $order; ?>"></p>
		</div>
		<div id="button<?php echo $order; ?>" class="text-center">
			<button class="btn btn-primary" onclick="ellenoriz(<?php
				echo '\''
					.$order.'\',\''
					.$correct.'\',\''
					.$solution.'\',\''
					.$type.'\''?>)">Mehet</button><br />
		</div>

	</div><?php

	return;
}

/**
 * Print exercise buttons for exercise
 *
 * @param int   $subtopic Subtopic data.
 * @param int   $order Exercise order.
 * @param string $ex_label Exercise label.
 *
 * @return NULL
 */
function printExerciseButtonsBottom($subtopic, $order, $ex_label)
{
	$subtopicID = $subtopic["id"];
	?>

	<div class="text-center">
		<p>
			<a href="index.php?p=<?php echo $subtopicID; ?>&amp;send=error&amp;label=<?php echo $ex_label; ?>" class="text-right error gray">
				<span class="glyphicon glyphicon-remove"></span>  Hibát találtam!
			</a>&nbsp;<?php

			if ($subtopic["type"] != 'video_full') {?>

				<a id="utmutato-gomb<?php echo $order; ?>" data-toggle="modal"  data-target="#utmutato-szoveg<?php echo $order; ?>" class="text-center utmutato">
				<span class="glyphicon glyphicon-question-sign"></span> Útmutató</a><?php

			}?>

			<a href="#smooth_top" class="text-right jump gray">
				<span class="glyphicon glyphicon-arrow-up"></span>  Ugrás a tetejére
			</a>
		</p>
	</div><?php

	return;
}

/**
 * Print exercise answer
 *
 * @param string $answer Exercise answer.
 *
 * @return NULL
 */
function printExerciseAnswer($answer)
{
	?><div class="panel-body">
		<div class="row">
			<div class="col-md-12"><?php

				echo $answer;

			?></div>
		</div>
	</div><?php

	return;
}

/**
 * Print linked data to exercise.
 *
 * @param array  $exercise Exercise data.
 * @param string $type   Link type ('to' or 'from')
 *
 * @return NULL
 */
function printExerciseLink($exercise, $type)
{

	$links = getLinksToExercise($exercise, $type);

	if (count($links) > 0) {?>

		<div class="panel-footer">
			<div class="text-center"><?php

				if (count($links) > 0) {
					if ($type == 'to') {
						if (count($links) > 1) {
							echo '<p>Videók:</p>';
						} else {
							echo '<p>Videó:</p>';
						}
					} else {
						if (count($links) > 1) {
							echo '<p>Gyakorlófeladatok:</p>';
						} else {
							echo '<p>Gyakorlófeladat:</p>';
						} 
					}
				}

				foreach ($links as $link) {

					$exercise = getExerciseFromLink($link, $type);

					if ($exercise) {

						printExerciseLinkButton($exercise[0], $type);

					}
				}?>

				</p>
			</div>
		</div><?php

	}

	return;
}

/**
 * Print button to linked exercise.
 *
 * @param array  $exercise Exercise data.
 * @param string $type   Link type ('to' or 'from')
 *
 * @return NULL
 */
function printExerciseLinkButton($exercise, $type)
{
	if ($type == 'from') {
		$icon = 'pencil';
	} else {
		$icon = 'play';
	}

	$subtopicID = $exercise["subtopicID"];
	$label = $exercise["label"];
	$label_ascii = toAscii($label);?>

	<p><a class="btn btn-info" href="index.php?p=<?php echo $subtopicID; ?>&amp;f=2#<?php echo $label_ascii; ?>">
		<span class="glyphicon glyphicon-<?php echo $icon; ?>"></span>&nbsp;&nbsp;<?php echo $label; ?>
	</a></p><?php

	return;
}

/**
 * Print model for youtube videos
 *
 * @return NULL
 */
function printExerciseModal()
{?>

	<!-- Video / Generic Modal -->
	<div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	  <div class="modal-content">
		<div class="modal-body">
		  <!-- content dynamically inserted -->
		</div>
	  </div>
	</div>
	</div><?php

}

/**
 * Print exercise link
 *
 * @param string $source Exercise source
 *
 * @return NULL
 */
function printExerciseSource($source)
{
	?><br /><div class="text-center">
		<a class="btn btn-default" href="<?php echo $source; ?>">
			<span class="glyphicon glyphicon-download-alt"></span>&nbsp;
			Eredeti feladatsor letöltése
		</a>
	</div><br /><?php

	return;
}

/**
 * Print exercise buttons on top
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printExerciseButtonsTop($id)
{
	if (isset($_REQUEST['f'])) {
		$level = $_REQUEST['f'];
	} else {
		$level = 1;
	}

	if ($level == 1) {
		$class1 = 'active';
		$class2 = '';
		$class3 = '';
	} elseif ($level == 2) {
		$class1 = '';
		$class2 = 'active';
		$class3 = '';
	} elseif ($level == 3) {
		$class1 = '';
		$class2 = '';
		$class3 = 'active';
	}

	?><div class="row row-buttons">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="btn-group-justified">
				<a href="index.php?p=<?php echo $id; ?>&amp;f=1" class="btn btn-warning <?php echo $class1; ?>">
					Kezdő<br />
					<span class="glyphicon glyphicon-star"></span>
				</a>
				<a href="index.php?p=<?php echo $id; ?>&amp;f=2" class="btn btn-warning <?php echo $class2; ?>">
					Haladó<br />
					<span class="glyphicon glyphicon-star"></span>
					<span class="glyphicon glyphicon-star"></span>
				</a>
				<a href="index.php?p=<?php echo $id; ?>&amp;f=3" class="btn btn-warning <?php echo $class3; ?>">
					Mester<br />
					<span class="glyphicon glyphicon-star"></span>
					<span class="glyphicon glyphicon-star"></span>
					<span class="glyphicon glyphicon-star"></span>
				</a>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div><?php

	return;
}

/**
 * Print exercise header
 *
 * @param int   $id	   Exercise id.
 * @param string $exercise Exercise text.
 *
 * @return NULL
 */
function printExerciseHeader($id, $exercise)
{
	?><div class="panel-heading" id="feladat-'.$id.'">
		<div class="row">
			<div id="feladatszam" class="col-md-12 text-left"><?php

			echo $id;

			?>.</div>
		</div>
		<div class="row">
			<div id="feladatszoveg" class="col-md-12">
				<h4 class="panel-title"><?php echo $exercise; ?></h4>
			</div>
		</div>
	</div><?php

	return;
}

/**
 * Print exercise hint
 *
 * @param int   $id Subtopic id.
 * @param array $data  Exercise data.
 *
 * @return NULL
 */
function printExerciseHint($id, $data)
{

	$level = $_SESSION['level'];

	echo '<div id="utmutato-szoveg'.$id.'" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" '
					.'data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Feladat</h4>
				</div>';

	if ($level == 1 && $data["guide1"] != '') {
		$guide = $data["guide1"];
	} elseif ($level == 2 && $data["guide2"] != '') {
		$guide = $data["guide2"];
	} elseif ($level == 3 && $data["guide3"] != '') {
		$guide = $data["guide3"];
	} else {
		$guide = $data["guide"];
	}
	
	echo '	<div class="modal-body">'.$guide.'</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" '
					.'data-dismiss="modal">Bezár</button>
				</div>
			</div>
		</div>
	</div>';
	return;
}

/**
 * Print sidebar for exercises
 *
 * @param int $subtopicID Subtopic id.
 *
 * @return NULL
 */
function printExercisesMenu($subtopicID)
{
	echo '<ul class="nav nav-pills nav-stacked" '
			.'data-spy="affix" data-offset-top="205" id="sidebar">';
	$exercises = getExercises('subtopicID = "'.$subtopicID.'"');
	
	if (count($exercises) > 0) {

		$active = true;

		foreach ($exercises as $exercise) {

			$links_to = getLinksToExercise($exercise, 'to');

			if (count($links_to) > 0) {
				$span = '<span class="glyphicon glyphicon-play"></span>';
			} else {
				$span = '';
			}

			if ($active) {
				$class = ' class="active"';
				$active = false;
			} else {
				$class = '';
			}
			echo '<li'.$class.'>
					<a href="#smooth_'.toAscii($exercise["label"]).'">'
					.$span.'&nbsp;'.$exercise["label"].'</a>
				<li>';
		}

	}
	echo '</ul>';
	return;
}

/**
 * Print icons on main page
 *
 * @return NULL
 */
function printIcons()
{
	$questions = count(getQuestions());
	$ex_total = count(getExercises());
	$videos = getVideos()->num_rows;
	$quiz = getQuiz()->num_rows;
	?>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<div class="row">
				<div class="col-md-4">
					<a href="#TOC"><img class="img-responsive center-block" src="eszkozok/images/learn.png" alt="tanulj" width="150"></a>
					<h1 class="text-center">TANULJ</h1>
					<p>Ez a honlap Neked készült. A célja, hogy érthetővé és szerethetővé tegye a matekot. Jelenleg
						<?php echo $questions; ?> elméleti kérdés, <?php echo $quiz; ?> tesztkérdés, <?php echo $ex_total-$videos; ?> feladat és <?php echo $videos;?> videó található rajta. A tartalom lassan, de folyamatosan bővül.</p>
				   <div class="text-center main-button"><a href="index.php#smooth_toc" class="btn btn-primary">Tartalom</a></div>
				</div>
				<div class="col-md-4">
					<a href="index.php"><img class="img-responsive center-block" src="eszkozok/images/question.png" alt="tanulj" width="95"></a>
					<h1 class="text-center">KÉRDEZZ</h1>
					<p>Te döntheted el, hogy mi legyen a honlapon. Nem értesz valamit órán? Nem tudod megoldani az egyik feladatot? Szólj, és ahogy időnk engedi, igyekszünk segíteni! (Azért a házit nem fogjuk helyetted megcsinálni! ;)</p>
				   <div class="text-center main-button"><a href="index.php?send=question" class="btn btn-primary">Kérdezek</a></div>
				</div>
				<div class="col-md-4">
					<a href="index.php"><img class="img-responsive center-block" src="eszkozok/images/comment.png" alt="tanulj" width="143"></a>
					<h1 class="text-center">SZÓLJ HOZZÁ</h1>
					<p>A honlap teljes anyaga ingyenes, és az is marad. A pénzedre nem, de a véleményedre annál inkább kíváncsiak vagyunk! Furcsán működik valami? Tudod, hogy lehetne még jobbá tenni a honlapot? Írj bátran!</p>
				   <div class="text-center main-button"><a href="index.php?send=comment" class="btn btn-primary">Üzenetet küldök</a></div>
			   </div>
			</div>
		</div>
		<div class="col-md-1"></div>
	</div>';

		<?php
		return;
}

/**
 * Print info page
 *
 * @return NULL
 */
function printInfoPageHeader()
{?>

	<p>Ez a honlap elsősorban azért jött létre, hogy segítsen azoknak, akiknek nehezen megy a matek, és nincs pénzük különtanárra. A honlap szerkezete az iskolákban használt tankönyvek felépítését követi. Az esetleges hibákért semmilyen jogi felelősséget nem vállalok, de ha kapok róla visszajelzést, igyekszem minél hamarabb kijavítani őket. Egy témakör kidolgozása hosszabb ideig is eltarthat, ezért a honlap lassan bővül.</p>
	<p>A honlap használatához <a href="https://www.google.com/intl/hu/chrome/browser/desktop" target="_blank">Google Chrome</a> böngésző használata javasolt.</p>
	<p>A honlap teljes tartalma a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.hu" target="_blank">Creative Commons Nevezd meg! - Ne add el! - Így add tovább! 4.0 Nemzetközi Licenc</a> feltételeinek megfelelően ingyenesen felhasználható.</p>
	<p>Jó tanulást kívánok!</p>
	<p class="text-right"><i>Szabó Viktor</i></p>
	<p class="text-center"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/deed.hu" target="_blank"><img alt="Creative Commons Licenc" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a></p>
	<h1>Kapcsolat</h1><?php
   
	return;
}

/**
 * Print jumbotron
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printJumbotron($id=FALSE)
{
	if ($id) {
		$subtopic = getSubtopics('id = '.$id);
		$classes = getClasses('id = "'.$subtopic[0]["classID"].'"');
		$title = $subtopic[0]["name"];
		$subtitle = $classes[0]["name"];
		$img = '';
	} else {
		$title = 'zsebtanár';
		$subtitle = 'matek | másként';
		$img = '<a href="index.php"><img class="img-responsive center-block" '
			.'src="eszkozok/images/logo.png" alt="logo" width="150"></a>';
	}
	echo '<div class="jumbotron" id="smooth_top">'
			.$img.'<h1 class="text-center">'.$title.'</h1>	
			<p class="text-center">'.$subtitle.'</p>
		</div>';

	return;
}

/**
 * Print main page
 *
 * @return NULL
 */
function printMainPage()
{
	printJumbotron();
	printIcons();
	printTOC();
	return;
}


/**
 * Print navbar
 *
 * @return NULL
 */
function printNavBar()
{
	$href = 'beolvas.php';;
	if (isset($_REQUEST["p"])) {
		$href .= '?p='.$_REQUEST["p"];
		if (isset($_REQUEST["f"])) {
			$href .= '&f='.$_REQUEST["f"];
		}
	}
	?>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="banner">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="index.php">
					<img src="eszkozok/images/logo_small.png" alt="logo" width="20">
				</a>
				<a class="navbar-brand" href="index.php">Zsebtanár</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav"><?php

					printNavBarMenu();

					?></ul>
				<ul class="nav navbar-nav navbar-right"><?php

					if ($_SESSION['logged_in']) {?>

					<li>
						<a href="<?php echo $href; ?>">
							<span class="glyphicon glyphicon-refresh"></span> Frissítés
						</a>
					</li><?php		  

					}

					?><li>
						<a href="#" data-toggle="modal" data-target="#kereses">
							<span class="glyphicon glyphicon-search"></span> Keresés
						</a>
					</li>
					<li>
						<a href="index.php?send=info">
							<span class="glyphicon glyphicon-info-sign"></span> Mi ez?
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<?php
	printNavBarSearch();
	printNavBarTypeahead();
	return;
}

/**
 * Print navbar search
 *
 * @return NULL
 */
function printNavBarSearch()
{?>
	<div id="kereses" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Keresés</h4>
				</div>
				<div class="modal-body">
					<form action="index.php" method="post">
						<p id="result-container"></p>
						<div class="typeahead-container">
							<div class="typeahead-field">
								<span class="typeahead-query">
									<input id="q" name="q" type="search" autofocus autocomplete="off">
								</span>
								<span class="typeahead-button">
									<button type="submit">
										<span class="typeahead-search-icon"></span>
									</button>
								</span>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Bezár</button>
				</div>
			</div>
		</div>
	</div><?php

	return;
}

/**
 * Print navbar typeahead
 *
 * @return NULL
 */
function printNavBarTypeahead()
{?>
	<script>

		var data_typeahead = {<?php printTypeaheadData(); ?>};

		$('#q').typeahead({
		minLength: 1,
		order: "asc",
		group: true,
		groupMaxItem: 6,
		hint: true,
		dropdownFilter: "Összes",
		href: "index.php?search={{display}}",
		template: "{{display}}",
		source: {<?php printTypeaheadSource(); ?>},
		callback: {
		onClickAfter: function (node, a, item, event) {

		setTimeout(function () {
		  location.replace(item.href);
		}, 0);

		$('#result-container').text('');

		},
		onResult: function (node, query, obj, objCount) {

		var text = "";
		if (query !== "") {
		  text = objCount + ' találat erre: "' + query + '"';
		}
		$('#result-container').text(text);

		}
		},
		debug: true
		});

	</script><?php

	return;
}

/**
 * Print navbar menu
 *
 * @return NULL
 */
function printNavBarMenu()
{
	$classes = getClasses();

	if ($classes) {
		foreach ($classes as $class) {?>

		<li>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php

			echo $class["name"];

			$topics = getTopics('classID = "'.$class["id"].'"');
		
			if (count($topics) > 0) {?>
		
				<b class="caret"></b>
			</a>
			
			<ul class="dropdown-menu multi-level"><?php

				foreach ($topics as $topic) {
					
					printNavBarMenuTopic($topic);

				}?>

			</ul><?php

			}?>

		</li><?php
			
		}
	}

	return;
}

/**
 * Print navbar menu for topic
 *
 * @param array $topic Topic data
 *
 * @return NULL
 */
function printNavBarMenuTopic($topic)
{
	$subtopics = getSubtopics('topicID = "'.$topic["id"].'"');?>
	
	<li class="dropdown-submenu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php

			echo $topic["name"];?>

		</a>

		<ul class="dropdown-menu"><?php

		foreach ($subtopics as $subtopic) {?>

			<li>
				<a href="index.php?p=<?php echo $subtopic["id"]; ?>"><?php

					echo $subtopic["name"];?>

				</a>
			</li><?php
		
		}?>

		</ul>
	</li><?php

	return;
}


/**
 * Print page navigation buttons on top
 *
 * @param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printPageNavButtonsTop($subtopic)
{
	$id = $subtopic["id"];

	echo '<div class="row">
		<div class="col-md-12 text-center">
		<div class="btn-group navbuttonstop btn-group-justified btn-group-justified-big">';

	printPageNavButtonPrevious($id);
	printPageNavButtonQuestions($subtopic);
	printPageNavButtonExercises($subtopic);
	printPageNavButtonQuiz($id);
	printPageNavButtonNext($id);

	echo '</div></div></div>';
	return;
}

/**
 * Print page nav button - previous
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printPageNavButtonPrevious($id)
{
	$prev = $id-1;
	$subtopic = getSubtopics('id = '.$prev);
	if (count($subtopic) > 0) {
		$title = $subtopic[0]["name"];
		$glyphicon = 'glyphicon-chevron-left';
	} else {
		$title = 'Kezdőlap';
		$glyphicon = 'glyphicon-home';
	}
	
	echo '<a href="index.php?p='.$prev.'" class="btn btn-default">
			<span class="glyphicon '.$glyphicon.'"></span><br />
			<div class="hiddentext">'.$title.'</div></a>';
	return;
}

/**
 * Print page nav button - questions
 *
 * param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printPageNavButtonQuestions($subtopic)
{
	if (!isset($_REQUEST["f"])) {
		$class = 'active';
	} else {
		$class = '';
	}

	$questions = getQuestions('subtopicID = '.$subtopic["id"]);

	if (count($questions) > 0) {

		if ($subtopic["type"] != 'video' && $subtopic["type"] != 'video_full') {?>

		<a href="index.php?p=<?php echo $subtopic["id"]; ?>" class="btn btn-primary <?php echo $class; ?>">
			<span class="glyphicon glyphicon-book"></span><br />
			<div class="hiddentext">Elmélet</div>
		</a><?php

		}
	}
	return;
}

/**
 * Print page nav button - exercises
 *
 * @param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printPageNavButtonExercises($subtopic)
{
	$id = $subtopic["id"];
	if ($subtopic["type"] != 'video' && $subtopic["type"] != 'video_full') {
		$glyphicon = 'pencil';
	} else {
		$glyphicon = 'play';
	}
	if (isset($_REQUEST["f"])) {
		$class = 'active';
	} else {
		$class = '';
	}?>

	<a href="index.php?p=<?php echo $id; ?>&amp;f=1" class="btn btn-primary <?php echo $class; ?>">
		<span class="glyphicon glyphicon-<?php echo $glyphicon; ?>"></span><br />
		<div class="hiddentext">Feladatok</div>
	</a><?php

	return;
}

/**
 * Print page nav button - quiz
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printPageNavButtonQuiz($id)
{
	$quizdata = getQuiz($id);

	if ($quizdata->num_rows > 0) {
		echo '<a href="#" data-toggle="modal" data-target="#kviz" class="btn btn-primary">
			<span class="glyphicon glyphicon-ok"></span><br />
			<div class="hiddentext">Teszt</div></a>';
	}
	return;
}

/**
 * Print page nav button - next
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printPageNavButtonNext($id)
{
	$next = $id+1;
	$subtopic = getSubtopics('id = '.$next);
	if (count($subtopic) > 0) {
		$title = $subtopic[0]["name"];
		$glyphicon = 'glyphicon-chevron-right';
	} else {
		$title = 'Kezdőlap';
		$glyphicon = 'glyphicon-home';
	}
	echo '<a href="index.php?p='.$next.'" class="btn btn-default">
		<span class="glyphicon '.$glyphicon.'"></span><br />
		<div class="hiddentext">'.$title.'</div></a>';
	return;
}

/**
 * Print questions menu and text
 *
 * @param int $subtopic Subtopic data.
 *
 * @return NULL
 */
function printQuestions($subtopic)
{
	$id = $subtopic["id"];
	$questions = getQuestions('subtopicID = '.$id);
	if (count($questions) > 0) {?>
		<div class="row">
			<div class="col-md-1"></div>
			<nav class="col-md-4 navbar" id="leftCol"><?php
	
			   printQuestionsMenu($id);

			?></nav>
			<div class="col-md-6"><?php

				printQuestionsText($subtopic);
				printQuiz($id);

			?></div>
			<div class="col-md-1"></div>
		</div><?php

	} else {?>

		<div class="row">
			<div class="col-md-12 text-center">
				<p>Ehhez a témakörhöz még nincsenek kérdések. :(</p>
				<a href="index.php?p=<?php echo $id; ?>&amp;send=question" class="btn btn-success">
					<span class="glyphicon glyphicon-plus"></span> Új kérdés beküldése
				</a>
			</div>
		</div><?php

	}
	return;
}

/**
 * Print sidebar for questions
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printQuestionsMenu($id)
{

	echo '<ul class="nav nav-pills nav-stacked" '
			.'data-spy="affix" data-offset-top="205" id="sidebar">';
	$questions = getQuestions('subtopicID = '.$id);
	if (count($questions) > 0) {
		$active = true;
		foreach ($questions as $question) {
			if ($active) {
				$class = ' class="active"';
				$active = false;
			} else {
				$class = '';
			}
			echo '<li'.$class.'>
					<a href="#smooth_'.toAscii($question["label"]).'">'
					.$question["label"].'</a>
				<li>';
		}
		
	}
	echo '</ul>';
	return;
}

/**
 * Print question and answer
 *
 * @param array $question Question data.
 *
 * @return NULL
 */
function printQuestion($question)
{
	$id = $question["subtopicID"];
	$label = toAscii($question["label"]);?>

	<h1><?php echo $question["question"]; ?></h1><?php echo $question["answer"]; ?>
	<div class="text-center">
		<p>
			<a href="index.php?p=<?php echo $id; ?>&amp;label=<?php echo $label; ?>&amp;send=error" class="text-right error gray">
				<span class="glyphicon glyphicon-remove"></span>  Hibát találtam!
			</a>&nbsp;
			<a href="index.php?p=<?php echo $id; ?>&amp;label=<?php echo $label; ?>&amp;send=comment" class="text-right comment gray">
				<span class="glyphicon glyphicon-comment"></span>  Hozzászólás
			</a>&nbsp;
			<a href="#smooth_top" class="text-right jump gray">
				<span class="glyphicon glyphicon-arrow-up"></span>  Ugrás a tetejére
			</a>
		</p>
	</div><?php

	return;
}

/**
 * Print questions and answers
 *
 * @param array $subtopic Subtopic data.
 *
 * @return NULL
 */
function printQuestionsText($subtopic)
{
	$id = $subtopic["id"];
	$questions = getQuestions('subtopicID = '.$id);
	$counter = count($questions);

	if ($counter > 0) {
		echo '<div id="header_message"></div>';
		foreach ($questions as $question) {

			$label = toAscii($question["label"]);

			echo '<div id="smooth_'.$label.'" class="anchor"></div>';

			printQuestion($question, $id);

			if ($counter > 1) {

				echo '<div class="bottom text-center">
					<img src="eszkozok/images/divider.png" width="80%">
				</div>';

			} else {

				echo '<div class="bottom-last text-center"></div>';

			}

			$counter--;
		}
	}
	return;
}

/**
 * Print quiz
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printQuiz($id)
{
	$quizdata = getQuiz($id);

	if ($quizdata->num_rows > 0) {
		printQuizHtml();
		printQuizJs($quizdata);
	}

	return;
}

/**
 * Print quiz html
 *
 * @return NULL
 */
function printQuizHtml()
{?>

	<div id="kviz" class="modal fade" role="dialog">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><div id="question"></h4>
		  </div>
		  <div class="modal-body">
			<div id="options"></div>
			<div id="error" class="text-center"></div>
			<div id="result"></div>
		  </div>
		  <div class="modal-footer">
			<div id="button"></div>
		  </div>
		</div>
	  </div>
	</div><?php

	return;
}

/**
 * Print quiz js
 *
 * @param array $quizdata Quizdata.
 *
 * @return NULL
 */
function printQuizJs($quizdata)
{?>

	<script type="text/javascript">
		var questions = [<?php

		while ($quiz = $quizdata->fetch_assoc()) {
			echo '{label: \''.$quiz["question"]
				.'\',options: [\''.$quiz["correct"].'\',\''
				.$quiz["wrong1"].'\',\''
				.$quiz["wrong2"].'\'],answer : [\''
				.$quiz["correct"].'\'],forceAnswer : true},';
		}?>

		];
		function ujratolt() {
			location.reload();
		}

		var quizMaker = new DG.QuizMaker({
			questions : questions,
			questions : shuffle(questions),
				el : 'options',
				el2 : 'question',
				el3 : 'button',
			forceCorrectAnswer:false,
			listeners : {
				'finish' : showScore,
				'missinganswer' : showAnswerAlert,
				'sendanswer' : clearErrorBox,
				'wrongAnswer' : showWrongAnswer
			  }
			});
			quizMaker.start();

	</script><?php

	return;

}

/**
 * Print send page
 *
 * @return NULL
 */
function printSendPage()
{
	if (!isset($_REQUEST['action'])) {
		$_SESSION['captcha'] = simple_php_captcha();
		$action=""; 
	} else {
		$action=$_REQUEST['action'];
	}

	?><div class="container">
		<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6"><?php

			if ($action == "") {
				printSendPageForm();				 
			} else { 
				printSendPageCheck();
			}   

		?></div>
		<div class="col-md-3"></div>
	</div>
	</div><?php

	return;
}

/**
 * Print Send New button
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printSendNewButton($id)
{

	?>
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="text-center alert alert-info">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				Kérdésed van? Nem értesz valamit?
				<b><a href="index.php?p=<?php echo $id; ?>&amp;send=message">Küldj üzenetet!</a></b>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<?php

	return;
}

/**
 * Print send page buttons
 *
 * @param int $id Subtopic id.
 *
 * @return NULL
 */
function printSendPageButtons($id)
{?>

	<div class="btn-group">
		<a href="index.php?p=<?php echo $id; ?>" class="btn btn-primary" role="button">
			<span class="glyphicon glyphicon-chevron-left"></span> Vissza
		</a>
		<a href="index.php?p=<?php echo $id; ?>&amp;send=<?php echo $_REQUEST['send']; ?>" class="btn btn-success" role="button">
			<span class="glyphicon glyphicon-repeat"></span> Újra
		</a>
	</div><?php

	return;
}

/**
 * Print send form
 *
 * @return NULL
 */
function printSendPageForm()
{
	$send = $_REQUEST['send'];
	$warning = '';

	if ($send == "comment") {
		$title = 'Megjegyzés küldése';
		$text = 'Megjegyzés';
	} elseif ($send == "error") {
		$title = 'Hibabejelentés';
		$text = 'Hiba';
	} elseif ($send == "exercise") {
		$title = 'Új feladat beküldése';
		$text = 'Feladat';
	} elseif ($send == "question") {
		$title = 'Új kérdés beküldése';
		$text = 'Kérdés';
	} elseif ($send == "info") {
		$title = 'A honlapról';
		$text = 'Üzenet';
	} elseif ($send == "message") {
		$title = 'Új üzenet beküldése';
		$text = 'Üzenet szövege';
	}

	echo '<h1>'.$title.'</h1>'.$warning;

	if ($send == "info") {

		printInfoPageHeader();

	}

	if (isset($_REQUEST["p"])) {
		$id = $_REQUEST["p"];
		$subtopic = getSubtopics('id = '.$id);
		$classes = getClasses('id = "'.$subtopic[0]["classID"].'"');
		$class = $classes[0]["name"];
		$subtopic_name = $subtopic[0]["name"];
		$back = '?p='.$id;
		if (isset($_REQUEST["label"])) {
			$label = $_REQUEST["label"];
		} else {
			$label = '';
		}
	} else {
		$class = '';
		$subtopic_name = '';
		$back = '';
		$label = '';
	}?>

	<form class="form-horizontal" role="form" method="post">
		<input type="hidden" name="action" value="submit"><?php

		if ($send!= "info") {?>

			<div class="form-group">
				<label class="control-label col-sm-3" for="osztaly">Osztály:</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="osztaly" value="<?php echo $class; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-3" for="topic">Témakör:</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" name="subtopic" value="<?php echo $subtopic_name; ?>">
				</div>
			</div><?php

		}?>

		<div class="form-group">
			<label class="control-label col-sm-3" for="email">Email:</label>
			<div class="col-sm-9">
				<input type="email" class="form-control" name="email">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-3" for="message"><?php echo $text; ?>:</label>
			<div class="col-sm-9">	  
				<textarea class="form-control" rows="5" name="message"></textarea>
			</div>
		</div>
		<div class="form-group text-center">
			<label class="control-label" for="kod">Ellenőrző kód:</label><br />
			<input class="form-control-small" name="kod"><br />
			<img src="<?php echo $_SESSION['captcha']['image_src']; ?>" alt="kod">
		</div>
		<div class="form-group text-center">   
			<div class="btn-group">
				<a class="btn btn-default" href="index.php<?php echo $back; ?>">
				<span class="glyphicon glyphicon-chevron-left"></span> Vissza</a>
				<button type="submit" class="btn btn-primary">
				<span class="glyphicon glyphicon-send"></span> Elküld</button>
			</div>
		</div>
		<input type="hidden" name="label" value="<?php echo $label; ?>">
	</form><?php
   
	return;
}

/**
 * Print send check
 *
 * @return NULL
 */
function printSendPageCheck()
{
	$send = $_REQUEST['send'];
	if (isset($_REQUEST['p'])) {
		$id = $_REQUEST['p'];
	} else {
		$id = 0;
	}

	$kod_jo = $_SESSION['captcha']['code'];
	$kod = $_REQUEST['kod'];
	if (isset($_REQUEST['osztaly']) & isset($_REQUEST['subtopic'])) {
		$classID = $_REQUEST['osztaly'];
		$subtopic = $_REQUEST['subtopic'];
	} else {
		$classID = '';
		$subtopic = '';
	}
	$email=$_REQUEST['email']; 
	$message=$_REQUEST['message'];
	$label=$_REQUEST['label']; 

	if ($message=="") {?>
		
		<div class="text-center">
			<h1>Hiba!</h1>
			<p>Az üzenet szövege hiányzik.</p><?php

			printSendPageButtons($id);

		?><div><?php

	} elseif ($kod != $kod_jo) {?>

		<div class="text-center">
			<h1>Hiba!</h1>
			<p>Az ellenőrző kód nem stimmel.</p><?php

			printSendPageButtons($id);

		?><div><?php

	} else {	  

		$header = 'From: '.$email; 
		$subject = '!!!'.$send.'!!! - '.date('Y.m.d - h:i:s');;
		$message_body = 'Feladó: '.$email
			."\r\n".'Osztály: '.$classID
			."\r\n".'Altémakör: '.$subtopic
			."\r\n".'Címke: '.$label
			."\r\n\r\n".'Üzenet: '.$message;
		if(mail('postmaster@zsebtanar.hu', $subject, $message_body, $header)) {?>
			
			<div class="text-center">
				<h1>Köszönjük!</h1>
				<p>Az üzenetet megkaptuk.</p>
				<a href="index.php?p=<?php echo $id; ?>" class="btn btn-primary" role="button">
					<span class="glyphicon glyphicon-chevron-left"></span> Vissza
				</a>
			<div><?php

		} else {?>
			
			<div class="text-center">
				<h1>Hiba!</h1>
				<p>A feladat elküldése sajnos nem sikerült. :(</p><?php

				printSendPageButtons($id);

			?><div><?php

		}
	} 

	return;

}

/**
 * Print subtopics
 *
 * @param array  $subtopic Subtopic data.
 *
 * @return NULL
 */
function printSubtopics($subtopic)
{
	$conn = $_SESSION['conn'];
	echo '<div id="sidebar"><div class="list-group panel">';
	$sql2 = 'SELECT *
			 FROM subtopics
			 WHERE topicID = "'.$subtopic["topicID"].'"';
	$res2 = $conn->query($sql2) or die ('Failed to select subtopic.');
	if ($res2->num_rows > 0) {
		while ($row2 = $res2->fetch_assoc()) {
			$id = $row2["id"];
			echo '<a id="menu'.$id.'" href="#almenu'.$id
				.'" class="list-group-item list-group-item-default" '
				.'data-toggle="collapse" data-parent="#siderbar">'
				.$row2["name"].'</a>';
			$sql3 = "SELECT *
					 FROM questions
					 WHERE subtopicID = ".$id;
			$res3 = $conn->query($sql3) or die ('Failed to select questions.');
			if ($res3->num_rows > 0) {
				echo '<div class="collapse" id="almenu'.$id.'">';
				while ($row3 = $res3->fetch_assoc()) {
					echo '<a href="index.php?p='.$id.'#'
						.$id.'_'.toAscii($row3["label"]).'" '
						.'data-parent="#almenu'.$id.'" '
						.'class="list-group-item list-group-sub-item">'
						.$row3["question"].'</a>';
				}
				$sql3b = "SELECT *
						  FROM exercises
						  WHERE subtopicID = ".$id;
				$res3b = $conn->query($sql3b) or die ('Failed to select exercises.');
				if ($res3b->num_rows > 0) {
					echo '<a href="index.php?p='.$id.'&f=1" '
						.'data-parent="#almenu'.$id.'" '
						.'class="list-group-item list-group-sub-item">Feladatok</a>';
				}
				echo '</div>';
			}
		}
	}
	echo '</div></div>';
	return;
}

/**
 * Print table of contents
 *
 * @return NULL
 */
function printTOC()
{
	$classes = getClasses();
	echo '<div class="anchor" id="smooth_toc"></div>';
	foreach ($classes as $class) {
		echo '<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-3">
					<div class="anchor" id="'.$class["alt"].'"></div>
					<div class="text-right jumbotron2">
						<h1>'.$class["alt"].'</h1>
						<p>'.$class["name"].'</p>
					</div>
				</div>
				<div class="col-md-4">';
		$topics = getTopics('classID = "'.$class["id"].'"');
		foreach ($topics as $topic) {
			echo '<div class="anchor" id="'.$class["alt"].'_'.$topic["alt"].'"></div>
				<h2>'.$topic["name"].'</h2><ul class="list-group">';
			$subtopics = getSubtopics('topicID = "'.$topic["id"].'"');
			foreach ($subtopics as $subtopic) {
				echo '<li class="list-group-item2">
						<a href="index.php?p='.$subtopic["id"].'">'
							.$subtopic["name"]
						.'</a>
					</li>';
			}
			echo '</ul>';
		}
		echo '  </div>
				<div class="col-md-3"></div>
			</div>';
	}
	return;
}

/**
 * Print typeahead data
 *
 * @return NULL
 */
function printTypeaheadData()
{
	$classes = getClasses();
	$data = '';
	if (count($classes) > 0) {
		foreach ($classes as $class) {

			$data = $data.'"osztaly'.$class["alt"].'": [';
			$questions = getQuestions('classID = '.$class["id"]);

			if (count($questions) > 0) {

				foreach ($questions as $question) {

					if (isset($question["label"])) {

						$subtopic = getSubtopics('id = '.$question["subtopicID"]);
						$title = $subtopic[0]["name"];

						mb_internal_encoding('UTF-8');
						if(!mb_check_encoding($title, 'UTF-8')
							OR !($title === mb_convert_encoding(mb_convert_encoding($title, 'UTF-32', 'UTF-8' ), 'UTF-8', 'UTF-32'))) {
							$title = mb_convert_encoding($title, 'UTF-8'); 
						}

						$TITLE = mb_convert_case($title, MB_CASE_UPPER, "UTF-8"); 

						$question["label"] = str_replace('"', '\\"', $question["label"]);
						$data = $data.'"[E] '.$TITLE.' - '.$question["label"].'",';
					}
				}
			}

			$exercises = getExercises('classID = '.$class["id"]);

			if (count($exercises) > 0) {

				foreach ($exercises as $exercise) {

					if (isset($exercise["label"])) {
						$subtopic = getSubtopics('id = '.$exercise["subtopicID"]);

						$exercise["label"] = str_replace('"', '\\"', $exercise["label"]);
						$data = $data.'"[F] '.mb_strtoupper($subtopic[0]["name"]).' - '.$exercise["label"].'",';
					}
				}
				$data = rtrim($data, ',');
			}

			$data = rtrim($data, ',');

			// if ($class["alt"] == '12') {
			//   $data = $data.'"laksjdf asdf","asdfalskdf jaskldfj "';
			// }
			$data = $data.'],';
		}
		$data = rtrim($data, ',');
		print_r($data);
	}
	return;
}

/**
 * Print typeahead source
 *
 * @return NULL
 */
function printTypeaheadSource()
{
	$conn = $_SESSION['conn'];
	$sql1 = "SELECT alt,name FROM classes";
	$res1 = $conn->query($sql1) or die ('Failed to select classes.');
	$data = '';
	if ($res1->num_rows > 0) {
		while ($row1 = $res1->fetch_assoc()) {
			$data = $data.'"'.$row1["name"].'"'
					.': { data: data_typeahead.osztaly'.$row1["alt"].'},';
		}
		$data = rtrim($data, ',');
	}
	print_r($data);
	return;
}

















/************************************   SEARCH *********************************/

/**
 * Slugify text (only for search)
 *
 * @param string $text Original string.
 *
 * @return string $slug Slugified string.
 */
function searchSlugify($text)
{ 
  // replace non letter or digits by -
  $slug = preg_replace('~[^\\pL\d]+~u', '-', $text);
  $mit  = array('á','Á','é','É','í','Í','ó','Ó','ö','Ö','ő','Ő','ú','Ú','ü','Ü','ű','Ű');
  $mire = array('a','a','e','e','i','i','o','o','o','o','o','o','u','u','u','u','u','u');
  $slug = str_replace($mit, $mire, $slug);
  // trim
  $slug = trim($slug, '-');
  // transliterate
  $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
  // lowercase
  $slug = strtolower($slug);
  // remove unwanted characters
  $slug = preg_replace('~[^-\w]+~', '', $slug);
  $slug = rtrim($slug, "-");
  return $slug;
}

function Search()
{
	$talalat = 0;

	$keyword = searchSlugify($_REQUEST["search"]);

	$questions = getQuestions();
	if (count($questions) > 0) {
		foreach ($questions as $question) {
			$subtopic = getSubtopics('id = '.$question["subtopicID"]);
			$slug = searchSlugify('[E] '.$subtopic[0]["name"].' - '.$question["label"]);
			if ($slug == $keyword) {
				$talalat = 1;
				header('Location: index.php?p='.$question["subtopicID"].'#smooth_'.toAscii($question["label"]));
			}
		}
	}

	$exercises = getExercises();
	if (count($exercises) > 0) {
		foreach ($exercises as $exercise) {
			$subtopic = getSubtopics('id = '.$exercise["subtopicID"]);
			$slug = searchSlugify('[F] '.$subtopic[0]["name"].' - '.$exercise["label"]);
			if ($slug == $keyword) {
				$talalat = 1;
				header('Location: index.php?p='.$exercise["subtopicID"].'&f=1#smooth_'.toAscii($exercise["label"]));
			}
		}
	}

	if ($talalat == 0) {
		// header('Location: index.php');
	}
	die;
}















/*******************************   STATISTICS   ************************************/

/**
 * Write statistics to Excel table
 *
 * @param string $file File name.
 *
 * @return NULL
 */
function writeStatistics($file)
{
	// Open file
	$fileType = 'Excel2007';
	$objReader = PHPExcel_IOFactory::createReader($fileType);
	$objPHPExcel = $objReader->load($file);

	$objPHPExcel->setActiveSheetIndex(0);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$highestRow = $objWorksheet->getHighestRow(); 

	$dateFound = FALSE;
	$total_old = 0;
	$currentDate = date('Y.m.d');
	for ($row = 1; $row <= $highestRow; ++$row) {
		$value = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
		$total_old = max($total_old, $objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
		if ($value == $currentDate) {
			$dateFound = TRUE;
			$objPHPExcel = updateExcel($objPHPExcel, $row, $total_old, $currentDate);
		}
	}

	if (!$dateFound) {
		$objPHPExcel = updateExcel($objPHPExcel, $highestRow+1, $total_old, $currentDate);
	}

	// Write the file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
	$objWriter->save($file);

	return;
}

/**
 * Update data in Excel table
 *
 * @param obj   $objPHPExcel PHP Excel object.
 * @param int   $row	   Row id.
 * @param int   $total_old   Total no. of items.
 * @param string $currentDate Current date.
 *
 * @return obj $objPHPExcel PHP Excel object.
 */
function updateExcel($objPHPExcel, $row, $total_old, $currentDate)
{
	$topics = count(getTopics());
	$subtopics = count(getSubtopics());

	$questions = count(getQuestions());
	$quiz = getQuiz()->num_rows;
	$videos = getVideos()->num_rows;
	$exercises = count(getExercises()) - $videos;
	
	$total_new = $questions + $exercises + $videos + $quiz;

	if ($total_new > $total_old) {
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$row, $currentDate)
					->setCellValue('B'.$row, $topics)
					->setCellValue('C'.$row, $subtopics)
					->setCellValue('D'.$row, $questions)
					->setCellValue('E'.$row, $quiz)
					->setCellValue('F'.$row, $exercises)
					->setCellValue('G'.$row, $videos)
					->setCellValue('H'.$row, $total_new);
	}

	return $objPHPExcel;
}

?>