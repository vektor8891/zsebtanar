<?php
/**
 * Main functions
 *
 * PHP Version 5
 *
 * @category PHP
 * @package  Zsebtanár
 * @author   Viktor Szabó <zsebtanar@gmail.com>
 * @license  http://creativecommons.org/licenses/by-nc-sa/4.0/ CC BY-NC-SA 4.0
 * @link     http://zsebtanar.hu/
 */

/**
 * Connects to database
 *
 * @param string $file Configuration file.
 *
 * @return NULL
 */
function connectDatabase($file)
{
    $configdata = parse_ini_file($file);

    $servername = $configdata["servername"];
    $username = $configdata["username"];
    $password = $configdata["password"];
    $dbname = $configdata["dbname"];

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        $conn = new mysqli($servername, $username, $password)
            or die('Connection failed: ' . $conn->connect_error);
    }

    $_SESSION['conn'] = $conn;
    $_SESSION['dbname'] = $dbname;

    return;
}

/**
 * Convert string to Ascii
 *
 * @param string $str Original string.
 *
 * @return string $clean Modified string.
 */
function toAscii($str)
{
    $change = array('Á'=>'A', 'É'=>'E', 'Í'=>'I', 'Ó'=>'O', 'Ö'=>'O',
                    'Ő'=>'O', 'Ú'=>'U', 'Ü'=>'U', 'Ű'=>'U',
                    'á'=>'a', 'é'=>'e', 'í'=>'i', 'ó'=>'o', 'ö'=>'o',
                    'ő'=>'o', 'ú'=>'u', 'ü'=>'u', 'ű'=>'u');
    $clean = strtr($str, $change);
    $clean = preg_replace("/[\/|+ -]+/", '_', $clean);
    $clean = preg_replace("/[^a-zA-Z0-9_-]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));

    return $clean;
}


?>